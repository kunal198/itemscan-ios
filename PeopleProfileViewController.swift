//
//  PeopleProfileViewController.swift
//  itemScan
//
//  Created by brst on 6/9/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit



class PeopleProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    
    var differentiateLocationType = String()
    var differentiateMeType = String()
    
    var peopleSeclectedForScannedBarcode = String()
    
     var peopleString = String()
    
    var ItemName = NSString()
    var ItemNumber = NSString()
    
    var transitListingArray:NSMutableArray = NSMutableArray()
    
    var transitListingArray2:NSMutableArray = NSMutableArray()
    
    var strItemName:NSString = NSString()
    var strAssignToName:NSString = NSString()
     var peopleName = String()
    
    
    var recentListingArray:NSMutableArray = NSMutableArray()
    var recentItemName:NSString = NSString()
    
     var RecentListingCopy:NSMutableArray = NSMutableArray()
    
    @IBOutlet var BarButton: UIButton!
    @IBOutlet var PeopleNameLbl: UILabel!
    
    
    @IBOutlet var ScanAnotherButtonLbl: UIButton!
    @IBOutlet var PeopleNameLbl2: UILabel!
  
    
    @IBOutlet var DoneBtnLbl: UIButton!
    @IBOutlet var ScanAnotherItemView: UIView!
    @IBOutlet var ScanLbl: UILabel!
    @IBOutlet var PeopleCheckInTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //PeopleCheckInTableView.tableFooterView = UIView()
        self.PeopleNameLbl2.text = self.peopleSeclectedForScannedBarcode
        
        
        print("people name selected is \(self.PeopleNameLbl2.text )!")
        
        
        
       print("location selected by didselect value = \(locationSelectedByScanning)")
        
    }

    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        
        x = -myMenuView.frame.size.width
        
        
        if peopleString == "PeopleValue"
        {
            self.ScanAnotherItemView.hidden = true
            self.DoneBtnLbl.hidden = true
            self.PeopleNameLbl.text = "People Profile"
            
            
        }
        else
        {
            self.ScanAnotherItemView.hidden = false
            self.DoneBtnLbl.hidden = false
            
            
        }
    
        
        if differentiateLocationType == "PeopleValue"
        {
            
            if differentiateMeType == "MeOrNoBody"
            {
                 fetchInRecentDataBase_Recent()
            }
            else
            {
                //isOutOrIn = true
                fetchInTransitDataBase()

            }
        }
        else
        {
//            isOutOrIn = false
//            peopleSeclectedForScannedBarcode = " "
            
           fetchInTransitDataBase_ItemLocation()
            
        }
        
       

     
    }
    
    
    
    func fetchInRecentDataBase_Recent()
    {
        //        // print(recentListingArray.count)
        let str = "5451511"
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Recent", selectColumns: ["*"], whereString: "RecentName != '\(str)'ORDER BY dateCreated DESC LIMIT 0,10", whereFields: [])
        
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("RecentName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("RecentStatus"), forKey: "STATUS")
                
                recentItemName = (resultSet.stringForColumn("RecentName"))
                
                recentListingArray.addObject(fetchFields)
            }
            
            
            print("recentListingArray = \(recentListingArray)")
            
        //    cell.recentItemName.text = recentListingArray[indexPath.row].valueForKey("NAME") as? String
            
            
            
            for  var i = 0; i < recentListingArray.count; i++
            {
                let plzCheck = recentListingArray[i].valueForKey("STATUS") as? String
                //        // print("plzCheck is \(plzCheck)")
//                if(plzCheck == "Checked Out")
//                {
//                   print("checkout = \(recentListingArray[i].valueForKey("STATUS") as? String)")
//                }
                 if(plzCheck == "Checked In")
                {
                      print("checkout = \(recentListingArray[i].valueForKey("STATUS") as? String)")
                    
                    
                 
                    
                    RecentListingCopy.addObject(recentListingArray[i])
                    
                    
                    
                }
//                else
//                {
//                    
//                }

            }
            print("array for check in = \(RecentListingCopy.count)")
            
        }
        resultSet.close()
    }


    
    
    @IBAction func ScanAnotherItemAction(sender: AnyObject)
    {
        let ISTwoTabz = self.storyboard!.instantiateViewControllerWithIdentifier("ISTwoTab") as! ISTwoTab
        
        self.navigationController?.pushViewController(ISTwoTabz, animated: true)
    }
    
    
    
    @IBAction func DoneButtonAction(sender: AnyObject)
    {
        print("done btn clicked")
//        ScanAnotherButtonLbl.hidden = true
//        ScanLbl.hidden = true
        
        let ISTwoTabz = self.storyboard!.instantiateViewControllerWithIdentifier("ISHomeScreen") as! ISHomeScreen
        
        self.navigationController?.pushViewController(ISTwoTabz, animated: true)
    }

    
    
    
    @IBAction func BarButtonAction(sender: AnyObject)
    {
        print("barbutton clicked")
        
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })

    }
    
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("transitListingArray.count is \(transitListingArray2.count)")
        
        if differentiateLocationType == "PeopleValue"
        {
            //isOutOrIn = true
            if differentiateMeType == "MeOrNoBody"
            {
                return RecentListingCopy.count
            }
            else
            {
                   return transitListingArray.count;
            }
          
        }
        else
        {
            
             return transitListingArray.count;
        }

      
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
     
        
        if differentiateLocationType == "PeopleValue"
        {
            
               let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
          

            
            if differentiateMeType == "MeOrNoBody"
            {
                cell.transitItemNAme.text = RecentListingCopy[indexPath.row].valueForKey("NAME") as? String
                
                cell.transitItemAssignTo.text = "Me"
            
//                let plzCheck = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
//        
//                if(plzCheck == "Checked In")
//                {
//                    print("checkout = \(recentListingArray[indexPath.row].valueForKey("STATUS") as? String)")
//                }
                
             
                

            }
            else
            {
                cell.transitItemAssignTo.hidden = false
                
                cell.transitItemNAme.text = transitListingArray[indexPath.row].valueForKey("NAME") as? String
                cell.transitItemAssignTo.text = transitListingArray[indexPath.row].valueForKey("ItemAssignTo") as? String
            }

            
            
            cell.layoutMargins = UIEdgeInsetsZero;
            cell.preservesSuperviewLayoutMargins = false;
            // Cell.separatorInset = UIEdgeInsetsZero;
            tableView.separatorInset = UIEdgeInsetsZero;
            tableView.separatorColor = UIColor.clearColor()
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell

        }
        else
        {
            
            print("recent listing array = \(transitListingArray)")
            
           let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
            
            cell.transitItemAssignTo.hidden = true
            cell.transitItemNAme.frame.size.height = cell.contentView.frame.size.height - 15
            cell.StatusLbl.frame.size.height = cell.contentView.frame.size.height - 15
            
            cell.transitItemNAme.text = transitListingArray[indexPath.row].valueForKey("NAME") as? String
            
            let plzCheck = transitListingArray[indexPath.row].valueForKey("STATUS") as? String
            //        // print("plzCheck is \(plzCheck)")
//            if(plzCheck == "Checked Out")
//            {
//                cell.StatusLbl.text = transitListingArray[indexPath.row].valueForKey("STATUS") as? String
//                cell.StatusLbl.textColor = UIColor.redColor()
//            }
             if(plzCheck == "Checked In")
            {
                cell.StatusLbl.text = transitListingArray[indexPath.row].valueForKey("STATUS") as? String
                cell.StatusLbl.textColor = UIColor.greenColor()
            }
            else
            {
                
            }
            
            
            //        cell.transitItemAssignTo.text = transitListingArray[indexPath.row].valueForKey("ItemAssignTo") as? String
            cell.layoutMargins = UIEdgeInsetsZero;
            cell.preservesSuperviewLayoutMargins = false;
            // Cell.separatorInset = UIEdgeInsetsZero;
            tableView.separatorInset = UIEdgeInsetsZero;
            tableView.separatorColor = UIColor.clearColor()
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell

        }

        
        //   return cell
    }
    
    func fetchInTransitDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        var str = " "
        
        
        if differentiateLocationType == "PeopleValue"
        {
             str = peopleSeclectedForScannedBarcode
        }
       
//        else
//        {
//           str = locationSelectedByScanning as String
//        }

        
        
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemAssignTo == '\(str)'", whereFields: [])
        
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                //                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                //                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                //                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                //                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                //                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ItemAssignTo")
                strAssignToName = (resultSet.stringForColumn("ItemAssignTo"))
                strItemName = (resultSet.stringForColumn("ItemName"))
                
                arr.addObject(fetchFields)
                transitListingArray.addObject(fetchFields)
                
                
//                if self.peopleName ==  transitListingArray.valueForKey("ItemAssignTo") as? String
//                {
//                    fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
//                    fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ItemAssignTo")
//                    
//                     transitListingArray2.addObject(fetchFields)
//                }
                

            }
            
        }
        
        resultSet.close()
        print("arr is \(arr)")
    }
    
    
    
    
    func fetchInTransitDataBase_ItemLocation()
    {
        var arr:NSMutableArray = NSMutableArray()
        
      //  var str = " "
        
        
//        if peopleString == "PeopleProfile"
//        {
//            str = self.PeopleNameLbl2.text! as String
//        }
//                else
//                {
              //     str = locationSelectedByScanning as String
 //               }
        
        
        
        
     /*   let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemLocation == '\(str)'", whereFields: [])
        
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                //                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                //                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                //                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                //                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                //                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ItemAssignTo")
                strAssignToName = (resultSet.stringForColumn("ItemAssignTo"))
                strItemName = (resultSet.stringForColumn("ItemName"))
                
                arr.addObject(fetchFields)
                transitListingArray.addObject(fetchFields)
                
                
                //                if self.peopleName ==  transitListingArray.valueForKey("ItemAssignTo") as? String
                //                {
                //                    fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                //                    fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ItemAssignTo")
                //
                //                     transitListingArray2.addObject(fetchFields)
                //                }
                
                
            }
        }
        resultSet.close()
        //        // print("arr is \(arr)") */
        
        
        let str = "5451511"
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Recent", selectColumns: ["*"], whereString: "RecentName != '\(str)'ORDER BY dateCreated DESC LIMIT 0,10", whereFields: [])
        
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("RecentName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("RecentStatus"), forKey: "STATUS")
                
               // recentItemName = (resultSet.stringForColumn("RecentName"))
                
                transitListingArray.addObject(fetchFields)
            }
        }
        resultSet.close()

    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
