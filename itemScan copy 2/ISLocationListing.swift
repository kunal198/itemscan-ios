
//
//  ISLocationListing.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/11/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

var locationSelectedByScanning:NSString = NSString()

class ISLocationListing: UIViewController,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    
    
    var didSelectValue = String()
     var TypeCode = String()
    
    var locationListingArray = NSMutableArray()
    var globalLocationListingArray = NSMutableArray()
    var locationNumberStr:String = NSString() as String
    var locationNumber:String = NSString() as String


    @IBOutlet weak var locationListingTableView: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        myMenuView.hidden = true
        locationListingTableView.tableFooterView = UIView()
        
        
    
        
       // fillArrayForLocationListing()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        //+++++++++++++++++++++++++++++//
        
        fetchLocationData()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillArrayForLocationListing()
    {
        locationListingArray .addObject(["Location":"Location1"])
        locationListingArray .addObject(["Location":"Location2"])
        locationListingArray .addObject(["Location":"Location3"])
        locationListingArray .addObject(["Location":"Location4"])
        locationListingArray .addObject(["Location":"Location5"])
        locationListingArray .addObject(["Location":"Location6"])
        locationListingArray .addObject(["Location":"Location7"])
        // print("locationListingArray is\(locationListingArray)")
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return globalLocationListingArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        cell.Loc_name.text = globalLocationListingArray[indexPath.row].valueForKey("LOCNAME") as? String
        
         locationNumber  = globalLocationListingArray[indexPath.row].valueForKey("LOCNUMBER") as! String
        
        
        
        let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        let imagePath = "\(filePath)/\(locationNumber).jpeg"
        
   //      print("imagePath in \(imagePath)")
        
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
           cell.Loc_listingImgView.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
        }
        else
        {
            cell.Loc_listingImgView.image =  UIImage(named: "goldenImage.png")
        }
        
        
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if(isCheckInForLocationSelected == true)
        {
            let createAccountErrorAlert: UIAlertView = UIAlertView()
            createAccountErrorAlert.delegate = self
            createAccountErrorAlert.title = "Alert"
            createAccountErrorAlert.message = "Please Choose one"
            createAccountErrorAlert.addButtonWithTitle("Scan QR code")
            createAccountErrorAlert.addButtonWithTitle(" Scan Bar Code")
            createAccountErrorAlert.addButtonWithTitle("Cancel")
            createAccountErrorAlert.show()
            
            locationSelectedByScanning = (globalLocationListingArray[indexPath.row].valueForKey("LOCNAME") as? String)!
            
         //   isCheckInForLocationSelected = false
        }
        else
        {
            let ISLocationProfileViewz = self.storyboard!.instantiateViewControllerWithIdentifier("ISLocationProfileView") as! ISLocationProfileView
            ISLocationProfileViewz.descriptionLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCDESCRIPTION") as? String)!
            ISLocationProfileViewz.locLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCADDRESS") as? String)!
            ISLocationProfileViewz.LocationName = (globalLocationListingArray[indexPath.row].valueForKey("LOCNAME") as? String)!
            ISLocationProfileViewz.numberOfLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCNUMBER") as? String)!
            
            
            ISLocationProfileViewz.didSelectValue = (globalLocationListingArray[indexPath.row].valueForKey("LOCNAME") as? String)!
            
            self.navigationController?.pushViewController(ISLocationProfileViewz, animated: true)
            
         //  isCheckInForLocationSelected = true
        }
    }
    
    @IBAction func backfromLocationListing(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    
    func fetchLocationData()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        //        var itemDesc = ""
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Location", selectColumns: ["*"], whereString: "", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("LocationName"), forKey: "LOCNAME")
                fetchFields.setObject(resultSet.stringForColumn("LocationDescription"), forKey: "LOCDESCRIPTION")
                fetchFields.setObject(resultSet.stringForColumn("LocationAddress"), forKey: "LOCADDRESS")
                fetchFields.setObject(resultSet.stringForColumn("LocationNumber"), forKey: "LOCNUMBER")
                
                //                itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
                locationNumberStr = resultSet.stringForColumn("LocationNumber")
                globalLocationListingArray.addObject(fetchFields)
          //    print("globalLocationListingArray is \(globalLocationListingArray)")
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }
    
    //MARK: Alertview Delegate
    
    func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
            
        case 0:
//            let ISSearchQRCodez = self.storyboard!.instantiateViewControllerWithIdentifier("ISSearchQRCode") as! ISSearchQRCode
//            self.navigationController?.pushViewController(ISSearchQRCodez, animated: true)
            
            let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
            
            print("location selected at didselect = \(locationSelectedByScanning)")
            
            ISBarcodescannerz.TypeCode = "QRCode"
            
            self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
            
        case 1:
            
            let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
            
           ISBarcodescannerz.TypeCode = "BarCode"
            
            self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
        case 2:
            
            print ("Retry")
            
             isCheckInForLocationSelected = true
        default:
            print ("Default Retry")
            // isCheckInForLocationSelected = true
        }
    }

}
