//
//  ISTwoTab.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/22/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation

var isCheckInForLocationSelected:Bool = Bool()
var isCheckOutForPeopleSelected:Bool = Bool()

var isCheckInQRForLcoation:Bool = Bool()
var isCheckOutQRForPeople:Bool = Bool()

class ISTwoTab: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    //++++++++++++++++++++++++++++++//
    
    @IBOutlet weak var takePhotoOutlet: UIButton!

    @IBOutlet weak var scanQRCodeOutlet: UIButton!
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        takePhotoOutlet.backgroundColor = UIColor.clearColor()
        takePhotoOutlet.layer.cornerRadius = 6
        takePhotoOutlet.layer.borderWidth = 2
        takePhotoOutlet.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
        
        
        ///////////+++++++++++++++++++++////////////
        
        scanQRCodeOutlet.backgroundColor = UIColor.clearColor()
        scanQRCodeOutlet.layer.cornerRadius = 6
        scanQRCodeOutlet.layer.borderWidth = 2
        scanQRCodeOutlet.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }
    
     //MARK: Check In
    
    @IBAction func takePhoto(sender: AnyObject)
    {
        isCheckInForLocationSelected = true
        isCheckInQRForLcoation = true
        
        let ISLocationListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISLocationListing") as! ISLocationListing
        self.navigationController?.pushViewController(ISLocationListingz, animated: true)
    }
    
    @IBAction func scanQRCode(sender: AnyObject)
    {
         isCheckOutForPeopleSelected = true
        isCheckOutQRForPeople = true
        
        let ISPeopleListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISPeopleListing") as! ISPeopleListing
        self.navigationController?.pushViewController(ISPeopleListingz, animated: true)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex)
        {
        case 0:
             print("Cancel")
        case 1:
            Camera()
        case 2:
            Gallery()
        default:
             print("Default")
        }
    }
    
    func Camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            // print("Camera capture")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
            
            
        }
    }
    
    func Gallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        // print("delegate method two tab")
        
        let ISItemTabz = self.storyboard!.instantiateViewControllerWithIdentifier("ISItemTab") as! ISItemTab
        
        ISItemTabz.saveImgFromTwoTab = image
        self.dismissViewControllerAnimated(false, completion: nil)
        self.navigationController?.pushViewController(ISItemTabz, animated: false)
        
        
    }

  
    
    @IBAction func menuBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }

}
