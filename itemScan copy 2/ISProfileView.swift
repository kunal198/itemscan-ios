//
//  ISProfileView.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/8/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

var emailIdName:String = NSString() as String

class ISProfileView: UIViewController
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var username = NSString()
    var fullName = NSString()
    var usernameProfile = NSString()
    var NameProfile = NSString()
    var menuBarArray = NSMutableArray()
    //+++++++++++++++++++++++++++++++++++++++//
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var fullNameProfile: UILabel!
    @IBOutlet weak var profileUserName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileEmailId: UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        myMenuView.hidden = true
        
        fillArraymenuBarArray()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        fullNameProfile.text = username as String
        
        let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
        let imagePath = "\(filePath)/profilePic.png"
        
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
            profileImage.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
        }
        else
        {
            profileImage.image =  UIImage(named: "img.png")
        }
        
        if(isEditTrue == true)
        {
            usernameProfile = NSUserDefaults.standardUserDefaults().objectForKey("user") as! String
            NameProfile = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
            profileUserName.text = usernameProfile as String
            profileEmailId.text = NameProfile as String
//            fullNameProfile.text = NSUserDefaults.standardUserDefaults().objectForKey("user") as? String
        }
        else
        {
            username = NSUserDefaults.standardUserDefaults().objectForKey("USER") as! String
            emailIdName = NSUserDefaults.standardUserDefaults().objectForKey("EMAILID") as! String
            profileUserName.text = username as String
            profileEmailId.text = emailIdName as String
//            fullNameProfile.text = NSUserDefaults.standardUserDefaults().objectForKey("name") as? String
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillArraymenuBarArray()
    {
        menuBarArray .addObject(["menu":"Home","imageIcon":"home.png"])
        menuBarArray .addObject(["menu":"In Transit","imageIcon":"IntransitMenuBAr.png"])
        menuBarArray .addObject(["menu":"Recent","imageIcon":"recentMenuBar.png"])
        menuBarArray .addObject(["menu":"Location","imageIcon":"locationBlack.png"])
        menuBarArray .addObject(["menu":"Items","imageIcon":"itemMenuBar.png"])
        menuBarArray .addObject(["menu":"Setting","imageIcon":"settings.png"])
        // print("menuBarArray is\(menuBarArray)")
    }
    

    @IBAction func editBtn(sender: AnyObject)
    {
        let ISEditProfilez = self.storyboard!.instantiateViewControllerWithIdentifier("ISEditProfile") as! ISEditProfile
        self.navigationController?.pushViewController(ISEditProfilez, animated: true)
    }

    @IBAction func menuBarBtn(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }

        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
        })

    }
}
