//
//  ISAppSetting.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/13/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISAppSetting: UIViewController,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var settingArray = NSMutableArray()
    
    @IBOutlet weak var settingTable: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
        myMenuView.hidden = true
        
        settingArray = ["Profile","Privacy Policy","Terms and Conditions","Logout"]

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func backFromAppSetting(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return settingArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        // print("settingArray is\(settingArray)")
        cell.settingLbl.text = settingArray[indexPath.row] as? String
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if indexPath.row == 0
        {
            let ISProfileViewz = self.storyboard!.instantiateViewControllerWithIdentifier("ISProfileView") as! ISProfileView
            
            self.navigationController?.pushViewController(ISProfileViewz, animated: true)
        }

        else if indexPath.row == 1
        {
            let ISPrivacyPolicyz = self.storyboard!.instantiateViewControllerWithIdentifier("ISPrivacyPolicy") as! ISPrivacyPolicy
            
            self.navigationController?.pushViewController(ISPrivacyPolicyz, animated: true)
        }
        else if (indexPath.row == 2)
        {
            let ISTermsAndConditionsz = self.storyboard!.instantiateViewControllerWithIdentifier("ISTermsAndConditions") as! ISTermsAndConditions
            
            self.navigationController?.pushViewController(ISTermsAndConditionsz, animated: true)
        }
        else
        {
            
            let refreshAlert = UIAlertController(title: "Alert", message: "Do you want to logout?", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
                 NSUserDefaults.standardUserDefaults().setObject(Bool(false), forKey:"Login")
                self.navigationController?.popToRootViewControllerAnimated(false)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in

            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
}
