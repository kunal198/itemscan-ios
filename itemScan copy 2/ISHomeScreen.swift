//
//  ISHomeScreen.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/8/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MobileCoreServices

class ISHomeScreen: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    var username = NSString()
    var clickedImage = UIImage()
    
    @IBOutlet var MenuBarButton: UIButton!
    @IBOutlet weak var usernameHome: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        myMenuView.hidden = true
        
    
        self.navigationController?.navigationBar.hidden = true
        UIApplication.sharedApplication().statusBarStyle = .LightContent

//        let emailTransfer = username!.valueForKey("email") as! String
//        let passwordTransfer = username!.valueForKey("password") as! String
//        let usernameTransfer = username!.valueForKey("username") as! String
       
        username = NSUserDefaults.standardUserDefaults().valueForKey("USER") as! String
        
        if username != ""
        {
            // print("username is \(username)")
            usernameHome.text = "Welcome" + "," + " " + (username as String)
        }
        else
        {
           usernameHome.text = "Welcome"
        }
        // Do any additional setup after loading the view.
    }

    
    
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width

    }
    
    @IBAction func MenuBarButton(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
        })
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func manualBtn(sender: AnyObject)
    {
        let ISAppSettingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISAppSetting") as! ISAppSetting
        
        self.navigationController?.pushViewController(ISAppSettingz, animated: true)
    }
    @IBAction func profileBtn(sender: AnyObject)
    {
//        let ISProfileViewz = self.storyboard!.instantiateViewControllerWithIdentifier("ISProfileView") as! ISProfileView
//        
//        self.navigationController?.pushViewController(ISProfileViewz, animated: true)
        
        isCheckOutForPeopleSelected = false
        
        btnCheck = "dasfczc"
        
        let ISPeopleListingz = storyboard!.instantiateViewControllerWithIdentifier("ISPeopleListing") as! ISPeopleListing
    
        self.navigationController?.pushViewController(ISPeopleListingz, animated: true)
    }
    @IBAction func locationBtn(sender: AnyObject)
    {
        
        isCheckInForLocationSelected = false
        
        let ISLocationListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISLocationListing") as! ISLocationListing
        
        self.navigationController?.pushViewController(ISLocationListingz, animated: true)
        
    }

    @IBAction func inTransitBtn(sender: AnyObject)
    {
        let InTransitListingz = self.storyboard!.instantiateViewControllerWithIdentifier("InTransitListing") as! InTransitListing
        
        self.navigationController?.pushViewController(InTransitListingz, animated: true)
    }
  
    @IBAction func recentBtn(sender: AnyObject)
    {
//        let textToShare = "Swift is awesome!  Check out this website about it!"
//        
//        if let myWebsite = NSURL(string: "http://www.codingexplorer.com/")
//        {
//            let objectsToShare = [textToShare, myWebsite]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            
//            //New Excluded Activities Code
//            activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList,UIActivityTypePostToTwitter,UIActivityTypePostToFacebook,UIActivityTypePostToVimeo,UIActivityTypePostToFlickr ]
//            //
//            
//            activityVC.popoverPresentationController?.sourceView = sender as? UIView
//            self.presentViewController(activityVC, animated: true, completion: nil)
//        }
//        
        let RecentItemListingz = self.storyboard!.instantiateViewControllerWithIdentifier("RecentItemListing") as! RecentItemListing
        
        self.navigationController?.pushViewController(RecentItemListingz, animated: true)
    }

    @IBAction func addBtnUnderHome(sender: AnyObject)
    {
        let ISThreeTabz = self.storyboard!.instantiateViewControllerWithIdentifier("ISThreeTab") as! ISThreeTab
        
        self.navigationController?.pushViewController(ISThreeTabz, animated: true)
    }
    @IBAction func itemBtn(sender: AnyObject)
    {
        let ISItemListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISItemListing") as! ISItemListing
        
        self.navigationController?.pushViewController(ISItemListingz, animated: true)
    }
    @IBAction func sacnBarcodeBtn(sender: AnyObject)
    {
        let ISTwoTabz = self.storyboard!.instantiateViewControllerWithIdentifier("ISTwoTab") as! ISTwoTab
        
        self.navigationController?.pushViewController(ISTwoTabz, animated: true)
        
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
//        {
//            // print("Button capture")
//            
//            let imag = UIImagePickerController()
//            imag.delegate = self
//            imag.sourceType = UIImagePickerControllerSourceType.Camera;
//            imag.mediaTypes = [kUTTypeImage as String]
//            imag.allowsEditing = false
//            
//            self.presentViewController(imag, animated: true, completion: nil)
//        }
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        // print("delegate method")
        
        let ISShowBarcodeDetailz = self.storyboard!.instantiateViewControllerWithIdentifier("ISShowBarcodeDetail") as! ISShowBarcodeDetail
        
        ISShowBarcodeDetailz.SaveImage = image
         self.dismissViewControllerAnimated(false, completion: nil)
        self.navigationController?.pushViewController(ISShowBarcodeDetailz, animated: false)
        
        
    }

}