//
//  ISLocationProfileEditing.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/11/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

 var selectedCategoryFromCategoryListing: String = NSString() as String
 var isBackFromEditLocation = false
var numberForFetchLoc:NSString = NSString()

class ISLocationProfileEditing: UIViewController
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var username = NSString()
    
    var decriptionForEditLocation:NSString = NSString()
    var addressForEditLocation:NSString = NSString()
    var numberForEditLocation:NSString = NSString()
    
   
    
    ///////////////////////////////////////////////////////
    
    @IBOutlet weak var addressLblEditLoc: UITextField!
    @IBOutlet weak var descriptionLblEditLoc: UITextField!
    
    @IBOutlet weak var selectedCategoryUnderEditLoc: UILabel!
    @IBOutlet weak var usernameEditLocation: UILabel!
    @IBOutlet weak var profilePicImageView: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        myMenuView.hidden = true

        let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]

        let imagePath = "\(filePath)/profilePic.png"
        
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
            profilePicImageView.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
            
            // self.profileImageView.image = UIImage(data: fileManager.contentsAtPath(imagePath)!)
            //  buttonName.setTitle("Replace Image", forState: UIControlState.Normal)
        }
        else
        {
            profilePicImageView.image =  UIImage(named: "img.png")
            //self.profileImageView.image = UIImage(named: "default.png")
            //  buttonName.setTitle("Add Image", forState: UIControlState.Normal)
        }
        
        username = NSUserDefaults.standardUserDefaults().objectForKey("USER") as! String
//        usernameEditLocation.text = username as String
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        fetchLocationDetailsDataBase()
        
//        addressLblEditLoc.text = addressForEditLocation as String
//
//        if isBackFromEditLocation == true
//        {
//            descriptionLblEditLoc.text = globalDescriptionEditLoc
//            globalDescriptionEditLoc = ""
//            isBackFromEditLocation = false
//        }
//        else
//        {
//            descriptionLblEditLoc.text = decriptionForEditLocation as String
//        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func descBtnEditLocation(sender: AnyObject)
    {
        let ISDescriptionz = self.storyboard!.instantiateViewControllerWithIdentifier("ISDescription") as! ISDescription
        self.navigationController?.pushViewController(ISDescriptionz, animated: true)
        descriptionLblEditLoc.text = ""
    }
    @IBAction func DoneProfileEditing(sender: AnyObject)
    {
        updateLocation()
        myMenuView.hidden = true
        self.navigationController!.popViewControllerAnimated(true)
    }
    @IBAction func backProfileEditing(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    
    @IBAction func location_AddBtn(sender: AnyObject)
    {
        let ISCategoryListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISCategoryListing") as! ISCategoryListing
        
        self.presentViewController(ISCategoryListingz, animated: true, completion: nil)
    }
    
    func updateLocation()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["LocationAddress"] = addressLblEditLoc.text
        tblFields["LocationDescription"] = descriptionLblEditLoc.text
        tblFields["LocationCategory"] = selectedCategoryUnderEditLoc.text
        
        _ = ModelManager.instance.updateTableData("Location", tblFields: tblFields, whereString: "LocationNumber='\(numberForEditLocation)'", whereFields: [])
        
        // print("tblFields under update is \(tblFields)")
    }
    func fetchLocationDetailsDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        //        var itemDesc = ""
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Location", selectColumns: ["*"], whereString: "LocationNumber = '\(numberForEditLocation)'", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("LocationAddress"), forKey: "LocAddress")
                fetchFields.setObject(resultSet.stringForColumn("LocationDescription"), forKey: "LocDescription")
                // itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
                //                globalItemArray.addObject(fetchFields)
                // print("fetchFields in selectedItem is \(fetchFields)")
                
                descriptionLblEditLoc.text = resultSet.stringForColumn("LocationDescription")
                addressLblEditLoc.text = resultSet.stringForColumn("LocationAddress")
                let strCategory = resultSet.stringForColumn("LocationAddress")
                if(strCategory == "")
                {
                    selectedCategoryUnderEditLoc.text = "Select Category"
                }
                else
                {
                    selectedCategoryUnderEditLoc.text = strCategory as String
                }
                
                
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }

}
