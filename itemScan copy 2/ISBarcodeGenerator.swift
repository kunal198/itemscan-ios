//
//  ISBarcodeGenerator.swift
//  itemScan
//
//  Created by mrinal khullar on 4/28/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MessageUI

class ISBarcodeGenerator: UIViewController,UITextFieldDelegate,MFMailComposeViewControllerDelegate
{
    var GeneratedQrcode:String = NSString() as String
    var emailImage:UIImage = UIImage()
    let imageData:NSString = NSString()
    var QRItem:NSString = NSString()
    
    @IBOutlet var generatelabel: UILabel!
    var qrcodeImage: CIImage!
    @IBOutlet var randomnum: UILabel!
    @IBOutlet var qrbackview: UIView!
    @IBOutlet var barQRtextfeild: UITextField!
    @IBOutlet var barcodeimg: UIImageView!
    
    @IBOutlet var donebtn: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       placeHolderMethodForAddPeople()
   generatelabel.text = NSUserDefaults.standardUserDefaults().objectForKey("generation") as? String
        
        print("QRItem is \(QRItem)")
        
        barQRtextfeild.text = QRItem as String
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    @IBAction func barcodegenerator(sender: AnyObject)
    {
//        if (generatelabel.text == "Enter Text To Generate QR code")
//        {
            barcodeimg.frame = CGRectMake(100,300, 100, 100)
            if barQRtextfeild.text == ""
            {
                return
            }
            let data = barQRtextfeild.text!.dataUsingEncoding(NSISOLatin1StringEncoding, allowLossyConversion: false)
            let filter = CIFilter(name: "CIQRCodeGenerator")
            filter!.setValue(data, forKey: "inputMessage")
            filter!.setValue("Q", forKey: "inputCorrectionLevel")
            qrcodeImage = filter!.outputImage
            barQRtextfeild.resignFirstResponder()
            displayQRCodeImage()
    }
    
        func displayQRCodeImage()
        {
            let scaleX = barcodeimg.frame.size.width / qrcodeImage.extent.size.width
            let scaleY = barcodeimg.frame.size.height / qrcodeImage.extent.size.height
            
            let transformedImage = qrcodeImage.imageByApplyingTransform(CGAffineTransformMakeScale(scaleX, scaleY))
            
            barcodeimg.image = UIImage(CIImage: transformedImage)
            
            print("transformedImage is \(transformedImage)")
            
            emailImage = UIImage(CIImage: transformedImage)
            
            donebtn.hidden = false;
            
            composeMail()
    }

    @IBAction func backAction(sender: AnyObject)
    {
        
          isCheckOutForPeopleSelected = true
      self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func DoneQRcode(sender: AnyObject)
    {
        isbackfromqR = true
         GeneratedQrcode = "QR code Added Successfully"
        NSUserDefaults.standardUserDefaults().setObject("QR code Added successfully", forKey: "qr/Barcode")
        NSUserDefaults.standardUserDefaults().synchronize()
      self.navigationController?.popViewControllerAnimated(true)
    }

    
    //Mark : Add background in Textfeild
    
    func placeHolderMethodForAddPeople()
    {
        let paddingView = UIView(frame: CGRectMake(0, 0, 15, barQRtextfeild.frame.height))
        barQRtextfeild.leftView = paddingView
        barQRtextfeild.leftViewMode = UITextFieldViewMode.Always
        
        barQRtextfeild.attributedPlaceholder = NSAttributedString(string:"Add QR Code", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        qrbackview.backgroundColor = UIColor.clearColor()
        qrbackview.layer.cornerRadius = 6
        qrbackview.layer.borderWidth = 2
        qrbackview.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
    }
//    func textFieldShouldReturn(textField: UITextField) -> Bool
//    {
//       barQRtextfeild.resignFirstResponder()
//        
//        return true
//    }
//    
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
//    {
//      barQRtextfeild.resignFirstResponder()
//    }
    
    func composeMail()
    {
        barcodeimg.frame = CGRectMake(100,300, 100, 100)
        if barQRtextfeild.text == ""
        {
            return
        }
        let data = barQRtextfeild.text!.dataUsingEncoding(NSISOLatin1StringEncoding, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter!.setValue(data, forKey: "inputMessage")
        filter!.setValue("Q", forKey: "inputCorrectionLevel")
        qrcodeImage = filter!.outputImage
        barQRtextfeild.resignFirstResponder()
//        displayQRCodeImage()

        ///////////////////////////
        
        let mailComposeVC = MFMailComposeViewController()
        
        mailComposeVC.mailComposeDelegate = self;
        
        print(UIImage(CIImage: qrcodeImage))
        
        emailImage = UIImage(named: "edit.png")!
        
        print("emailImage is \(emailImage)")
        
//        let newImage = UIImage(CIImage:filter!.outputImage!)
        
//        let myImageData = UIImagePNGRepresentation( newImage)
        
        
        
        // 1
        let context = CIContext(options:nil)
        
        // 2
        let cgimg = context.createCGImage(filter!.outputImage!, fromRect: filter!.outputImage!.extent)

        // 3
        var newImagez = UIImage(CGImage: cgimg)
        
        
         newImagez = resizeImage(newImagez, newWidth: 100)
        
        print("imageViewForQR.image is \(newImagez)")
        
         let myImageData = UIImagePNGRepresentation(newImagez)
        
        mailComposeVC.addAttachmentData(myImageData!, mimeType: "image/png", fileName:  "MyImageName.png")
        
        mailComposeVC.setSubject("QR Code of item ")
        
//        mailComposeVC.setMessageBody("<html><body><p>This is your message</p></body></html>", isHTML: true)
        
        self.presentViewController(mailComposeVC, animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage
    {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    @IBAction func sendbtn(sender: AnyObject)
    {
        composeMail()
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        switch result {
        case MFMailComposeResultCancelled:
            print("Mail cancelled")
        case MFMailComposeResultSaved:
            print("Mail saved")
        case MFMailComposeResultSent:
            print("Mail sent")
        case MFMailComposeResultFailed:
            print("Mail sent failure: \(error?.localizedDescription)")
        default:
            break
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
}


