//
//  ISLocationProfileView.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/11/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISLocationProfileView: UIViewController,UITextFieldDelegate,UITableViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var didSelectValue = String()
    
   
    
     var ItemName = String()
    
    var recentListingArray:NSMutableArray = NSMutableArray()
    var recentItemName:NSString = NSString()

    
    var descriptionLocationDetail:NSString = NSString()
    var locLocationDetail:NSString = NSString()
    var numberOfLocationDetail:NSString = NSString()
      var LocationName:NSString = NSString()
    
    @IBOutlet var Locationname: UILabel!
    
    @IBOutlet var locationScrollView: UIScrollView!
    @IBOutlet var LocationTableView: UITableView!
    //+++++++++++++++++++++++++//
    
    @IBOutlet weak var locDescriptionLocationDetail: UILabel!
    @IBOutlet weak var currentLocLocationDetail: UILabel!

    
    @IBOutlet var Locatondetailimg: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
    }
    
    override func viewWillAppear(animated: Bool)
    {
        
        
        var ScrollHeight = CGFloat()
        ScrollHeight = self.LocationTableView.frame.origin.y + self.LocationTableView.frame.size.height + 20
        
        self.locationScrollView.contentSize = CGSizeMake(self.locationScrollView.frame.size.width, ScrollHeight)
        
        print("scroll view height is = \(ScrollHeight)")
        
        
        
        
        

        
        
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        //+++++++++++++++++++++++++//
        
        locDescriptionLocationDetail.text = descriptionLocationDetail as String
        currentLocLocationDetail.text = locLocationDetail as String
        Locationname.text = LocationName as String
       // numberOfLocationDetail
        
        
        let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        let imagePath = "\(filePath)/\(numberOfLocationDetail).jpeg"
        
        // print("imagePath in \(imagePath)")
        
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
            Locatondetailimg.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
        }
        else
        {
            Locatondetailimg.image =  UIImage(named: "img.png")
        }
        
        
        
        databasevalues()
           
        
        // print("numberOfLocationDetail is \(numberOfLocationDetail)")
        
        fetchLocationDetailsDataBase()
    }


    
    
    
    
   func databasevalues()
   {
 
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemLocation = '\(didSelectValue)'", whereFields: [])
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
//                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
//                
//                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
//                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
//                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
//                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
//                fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
//                fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ITEMASSIGNTO")
                
                //                        itemNumberStr = resultSet.stringForColumn("ItemNumber")
                arrayForScannedBarcode.addObject(fetchFields)
                
                
                
                
                print("arrayForScannedBarcode is \(arrayForScannedBarcode)")
                
               ItemName = arrayForScannedBarcode[0].valueForKey("NAME") as! String
//                
//                   let resultSetRecentDB: FMResultSet! = ModelManager.instance.getTableData("Recent", selectColumns: ["*"], whereString: "RecentName = '\(ItemName)'", whereFields: [])
                fetchInRecentDataBase()
                
                
            }
        }
        resultSet.close()
        
   
    }
    
    
    
    
    
    func fetchInRecentDataBase()
    {
        //        // print(recentListingArray.count)
        let str = "5451511"
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Recent", selectColumns: ["*"], whereString: "RecentName = '\(ItemName)'ORDER BY dateCreated DESC LIMIT 0,10", whereFields: [])
        
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("RecentName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("RecentStatus"), forKey: "STATUS")
                
                recentItemName = (resultSet.stringForColumn("RecentName"))
                
                recentListingArray.addObject(fetchFields)
                
                  print("arrayForScannedBarcode is \(recentListingArray)")
                
                             
                
            }
        }
        resultSet.close()
    }

    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //        // print("recentListingArray.count is \(recentListingArray.count)")
        return recentListingArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        print("recent listing array = \(recentListingArray)")
        
        cell.recentItemName.text = recentListingArray[indexPath.row].valueForKey("NAME") as? String
        
        let plzCheck = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
        //        // print("plzCheck is \(plzCheck)")
        if(plzCheck == "Checked Out")
        {
            cell.recentStatus.text = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
            cell.recentStatus.textColor = UIColor.redColor()
        }
        else if(plzCheck == "Checked In")
        {
            cell.recentStatus.text = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
            cell.recentStatus.textColor = UIColor.greenColor()
        }
        else
        {
            
        }
        
        //        cell.transitItemAssignTo.text = transitListingArray[indexPath.row].valueForKey("ItemAssignTo") as? String
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func locDescriptionBtn(sender: AnyObject) {
    }
    @IBAction func locationEdit(sender: AnyObject)
    {
        let ISLocationProfileEditingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISLocationProfileEditing") as! ISLocationProfileEditing
        ISLocationProfileEditingz.decriptionForEditLocation = locDescriptionLocationDetail.text!
        ISLocationProfileEditingz.addressForEditLocation = currentLocLocationDetail.text!
        ISLocationProfileEditingz.numberForEditLocation = numberOfLocationDetail
        
        self.navigationController?.pushViewController(ISLocationProfileEditingz, animated: true)
    }

    @IBAction func locationBack(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
         
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                myMenuView.frame.origin.x = self.x
                
        })
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {   //delegate method
        
        currentLocLocationDetail.resignFirstResponder()
        
        return true
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        currentLocLocationDetail.resignFirstResponder()
    }
    func fetchLocationDetailsDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        //        var itemDesc = ""
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Location", selectColumns: ["*"], whereString: "", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("LocationAddress"), forKey: "LocAddress")
                fetchFields.setObject(resultSet.stringForColumn("LocationDescription"), forKey: "LocDescription")
                fetchFields.setObject(resultSet.stringForColumn("LocationNumber"), forKey: "LocNum")
                
                // itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
                //                globalItemArray.addObject(fetchFields)
                // print("fetchFields in selectedItem is \(fetchFields)")
                
                locDescriptionLocationDetail.text = resultSet.stringForColumn("LocationDescription")
                currentLocLocationDetail.text = resultSet.stringForColumn("LocationAddress")
                let  numberOfEditItem = resultSet.stringForColumn("LocationNumber")
                /////
             
                
                
                       
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }

}
