
//
//  RecentItemListing.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/21/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class RecentItemListing: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var recentListingArray:NSMutableArray = NSMutableArray()
    var recentItemName:NSString = NSString()
  
    @IBOutlet weak var recentTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        recentListingArray = []
        recentTableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        fetchInRecentDataBase()
    }
    
    @IBAction func backTransit(sender: AnyObject)
    {
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
//        // print("recentListingArray.count is \(recentListingArray.count)")
        return recentListingArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        print("recent listing array = \(recentListingArray)")
        
        cell.recentItemName.text = recentListingArray[indexPath.row].valueForKey("NAME") as? String
        
        let plzCheck = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
//        // print("plzCheck is \(plzCheck)")
        if(plzCheck == "Checked Out")
        {
            cell.recentStatus.text = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
            cell.recentStatus.textColor = UIColor.redColor()
        }
        else if(plzCheck == "Checked In")
        {
            cell.recentStatus.text = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
            cell.recentStatus.textColor = UIColor.greenColor()
        }
        else
        {
            
        }
       
//        cell.transitItemAssignTo.text = transitListingArray[indexPath.row].valueForKey("ItemAssignTo") as? String
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
        //        var didselectItem = transitListingArray[indexPath.row].valueForKey("NAME") as! String
        //        print("value selected is = \(didselectItem)")
        //
        //        var didSelectGlobalArray = globalItemArray[indexPath.row].valueForKey("NAME") as! String
        //          print("value selected from global array = \(didselectItem)")
        //
        //        if didselectItem == didSelectGlobalArray
        //        {
        //            print("same values")
        //        }
        //        else
        //        {
        //             print("not same values")
        //        }
        
        
        //        let ISSelectedItemz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectedItem") as! ISSelectedItem
        //
        //        print(globalItemArray[indexPath.row])
        //
        //        ISSelectedItemz.nameOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("NAME") as? String)!
        //        ISSelectedItemz.barcodeOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("BARCODE") as? String)!
        //        ISSelectedItemz.currentLocOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("LOCATION") as? String)!
        //        ISSelectedItemz.decriptionOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("DESCRIPTION") as? String)!
        //        ISSelectedItemz.colorOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("COLOR") as? String)!
        //        ISSelectedItemz.itemNumber = (globalItemArray[indexPath.row].valueForKey("NUMBER") as? String)!
        //
        //        self.navigationController?.pushViewController(ISSelectedItemz, animated: true)
        
        let ISItemListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISItemListing") as! ISItemListing
        
        self.navigationController?.pushViewController(ISItemListingz, animated: true)
        
    }
    

    
    
    
    func fetchInRecentDataBase()
    {
//        // print(recentListingArray.count)
        let str = "5451511"
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Recent", selectColumns: ["*"], whereString: "RecentName != '\(str)'ORDER BY dateCreated DESC LIMIT 0,10", whereFields: [])
        
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()

                fetchFields.setObject(resultSet.stringForColumn("RecentName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("RecentStatus"), forKey: "STATUS")

                recentItemName = (resultSet.stringForColumn("RecentName"))
                
                recentListingArray.addObject(fetchFields)
            }
        }
        resultSet.close()
    }

    @IBAction func backFromRecent(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
}
