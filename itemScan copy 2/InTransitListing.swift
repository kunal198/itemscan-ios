//
//  InTransitListing.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/21/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class InTransitListing: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var transitListingArray:NSMutableArray = NSMutableArray()
    var strItemName:NSString = NSString()
    var strAssignToName:NSString = NSString()
    
    
    var globalItemArray:NSMutableArray = NSMutableArray()
    var ImageArray:NSMutableArray = NSMutableArray()
    var itemNumberStr:String = NSString() as String

    
    //++++++++++++++++++++++++++++//
    
    @IBOutlet weak var transitTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        transitTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        print (myMenuView.frame)
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        fetchInTransitDataBase()
        fetchDataBase()
    }
    
    
    
    func fetchDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        let ItemNumber = " 23151561561456156159"
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemNumber != '\(ItemNumber)'ORDER BY dateCreated DESC LIMIT \(0),10", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                
                //                itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
                itemNumberStr = resultSet.stringForColumn("ItemNumber")
                globalItemArray.addObject(fetchFields)
                print("globalItemArray is \(globalItemArray)")
            }
        }
        resultSet.close()
        //        // print("arr is \(arr)")
    }

    
    
    @IBAction func backTransit(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("transitListingArray.count is \(transitListingArray.count)")
        return transitListingArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        
        print("listing is as = \(transitListingArray)")
        
        cell.transitItemNAme.text = transitListingArray[indexPath.row].valueForKey("NAME") as? String
        cell.transitItemAssignTo.text = transitListingArray[indexPath.row].valueForKey("ItemAssignTo") as? String
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
//        var didselectItem = transitListingArray[indexPath.row].valueForKey("NAME") as! String
//        print("value selected is = \(didselectItem)")
//        
//        var didSelectGlobalArray = globalItemArray[indexPath.row].valueForKey("NAME") as! String
//          print("value selected from global array = \(didselectItem)")
//        
//        if didselectItem == didSelectGlobalArray
//        {
//            print("same values")
//        }
//        else
//        {
//             print("not same values")
//        }
        
        
//        let ISSelectedItemz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectedItem") as! ISSelectedItem
//        
//        print(globalItemArray[indexPath.row])
//        
//        ISSelectedItemz.nameOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("NAME") as? String)!
//        ISSelectedItemz.barcodeOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("BARCODE") as? String)!
//        ISSelectedItemz.currentLocOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("LOCATION") as? String)!
//        ISSelectedItemz.decriptionOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("DESCRIPTION") as? String)!
//        ISSelectedItemz.colorOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("COLOR") as? String)!
//        ISSelectedItemz.itemNumber = (globalItemArray[indexPath.row].valueForKey("NUMBER") as? String)!
//        
//        self.navigationController?.pushViewController(ISSelectedItemz, animated: true)
        
        let ISItemListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISItemListing") as! ISItemListing
        
        self.navigationController?.pushViewController(ISItemListingz, animated: true)
        
    }

    

    func fetchInTransitDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
         let str = " "
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemAssignTo != '\(str)'", whereFields: [])
        
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
//                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
//                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
//                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
//                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
//                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ItemAssignTo")
                strAssignToName = (resultSet.stringForColumn("ItemAssignTo"))
                strItemName = (resultSet.stringForColumn("ItemName"))

                arr.addObject(fetchFields)
                transitListingArray.addObject(fetchFields)
            }
        }
        resultSet.close()
//        // print("arr is \(arr)")
    }
}
