//
//  ISPrivacyPolicy.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/13/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISPrivacyPolicy: UIViewController
{

    @IBOutlet weak var privacyPlociyTextView: UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        privacyPlociyTextView.editable = false;
        privacyPlociyTextView.selectable = false;
    }

    @IBAction func backFromPrivacyPolicy(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
