//
//  ISSelectedItem.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISSelectedItem: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    
    var peopleString = String()
    
    
    var SelectedItemName = String()
    var recentListingArray:NSMutableArray = NSMutableArray()
    var recentItemName:NSString = NSString()
    
    //var SelectedItemName = String()
    
    var barcodeOfSelectedItem:NSString = NSString()
    var currentLocOfSelectedItem:NSString = NSString()
    var nameOfSelectedItem:NSString = NSString()
    var decriptionOfSelectedItem:NSString = NSString()
    var colorOfSelectedItem:NSString = NSString()
    var itemNumber:String = NSString() as String
    var itemScanNumber:String = NSString() as String
    var itemQRNumber:String = NSString() as String
    var isEditSelectedLocationForScanning:Bool = Bool()
    var isEditQR:Bool = Bool()
    
    //+++++++++++++++++++++++++++++++++++++++++++++//
 
    @IBOutlet var LocationRecentTableView: UITableView!
    
    @IBOutlet var AssignToView: UIView!
    @IBOutlet var CheckInButton: UIView!
    @IBOutlet var ScrollViewItemDetails: UIScrollView!
    @IBOutlet weak var assignLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var colorLbl: UILabel!
    
    @IBOutlet weak var itemDetailsImageView: UIImageView!
    @IBOutlet weak var currentLocLbl: UILabel!
    @IBOutlet weak var barcodeLbl: UILabel!
    @IBOutlet weak var assignToOutlet: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        recentListingArray = []
        // Do any additional setup after loading the view.
        
        
        print("selected name = \(nameOfSelectedItem)")
        
        assignToOutlet.backgroundColor = UIColor.clearColor()
        assignToOutlet.layer.cornerRadius = 6
        assignToOutlet.layer.borderWidth = 2
        assignToOutlet.layer.borderColor = UIColor(red: 194.0/255.0, green: 141.0/255.0, blue: 36.0/255.0, alpha: 1.0).CGColor
        descLbl.textColor = UIColor.whiteColor()
        
        
        
//        CheckInButton.backgroundColor = UIColor.clearColor()
//        CheckInButton.layer.cornerRadius = 6
//        CheckInButton.layer.borderWidth = 2
//        CheckInButton.layer.borderColor = UIColor(red: 194.0/255.0, green: 141.0/255.0, blue: 36.0/255.0, alpha: 1.0).CGColor

        
        colorLbl.text = colorOfSelectedItem as String
        descLbl.text = decriptionOfSelectedItem as String
        currentLocLbl.text = currentLocOfSelectedItem as String
        barcodeLbl.text = barcodeOfSelectedItem as String
        itemNameLbl.text = nameOfSelectedItem as String

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
//        isCheckInForLocationSelected = true
//        isCheckOutForPeopleSelected = true
        
        var ScrollHeight = CGFloat()
        ScrollHeight = self.AssignToView.frame.origin.y + self.AssignToView.frame.size.height + 20
        
        self.ScrollViewItemDetails.contentSize = CGSizeMake(self.ScrollViewItemDetails.frame.size.width, ScrollHeight)
        
        print("scroll view height is = \(ScrollHeight)")
        
      
        
        self.AssignToView.frame.origin.y = self.LocationRecentTableView.frame.origin.y + self.LocationRecentTableView.frame.size.height + 10
        
        //++++++++++++++++++++++++++++++++++++++++//
        
        if(isCheckInForLocationSelected == true)
        {
            fetchScannedbarcode()
            
        }
        else if (isCheckOutForPeopleSelected == true)
        {
           fetchScannedbarcode()
        }
        else if(isCheckInQRForLcoation == true || isCheckOutQRForPeople == true)
        {
            fetchQRBarcode()
        }
        else
        {
            fetchSelectedItemDataBase()
        }
        
        fetchInRecentDataBase()
        
        if recentListingArray.count == 0
        {
            self.LocationRecentTableView.hidden = true
            
            self.AssignToView.frame.origin.y = self.CheckInButton.frame.origin.y + self.CheckInButton.frame.size.height + 10
        }
        else
        {
            self.LocationRecentTableView.hidden = false
            
            
        }

    }

    
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //        // print("recentListingArray.count is \(recentListingArray.count)")
        return recentListingArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        
        print("recent listing array = \(recentListingArray)")
        
        cell.recentItemName.text = recentListingArray[indexPath.row].valueForKey("NAME") as? String
        
        let plzCheck = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
        //        // print("plzCheck is \(plzCheck)")
        if(plzCheck == "Checked Out")
        {
            cell.recentStatus.text = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
            cell.recentStatus.textColor = UIColor.redColor()
        }
        else if(plzCheck == "Checked In")
        {
            cell.recentStatus.text = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
            cell.recentStatus.textColor = UIColor.greenColor()
        }
        else
        {
            
        }
        
        //        cell.transitItemAssignTo.text = transitListingArray[indexPath.row].valueForKey("ItemAssignTo") as? String
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func fetchInRecentDataBase()
    {
        //        // print(recentListingArray.count)
        var str = "5451511"
        
        str = nameOfSelectedItem as String
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Recent", selectColumns: ["*"], whereString: "RecentName == '\(str)'ORDER BY dateCreated DESC LIMIT 0,10", whereFields: [])
        
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("RecentName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("RecentStatus"), forKey: "STATUS")
                
                recentItemName = (resultSet.stringForColumn("RecentName"))
                
                recentListingArray.addObject(fetchFields)
                
            }
        }
        resultSet.close()
    }
    

    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func assignToBtn(sender: AnyObject)
    {
        let ISAssignPeoplez = self.storyboard!.instantiateViewControllerWithIdentifier("ISAssignPeople") as! ISAssignPeople
        ISAssignPeoplez.assignName = itemNameLbl.text!
        ISAssignPeoplez.numForAssignName = itemNumber
        ISAssignPeoplez.numForScannedAssignName = itemScanNumber
        ISAssignPeoplez.numForQRAssignName = itemQRNumber
         self.presentViewController(ISAssignPeoplez, animated: true, completion: nil)
    }

    @IBAction func EditSelectedItem(sender: AnyObject)
    {
        let ISEditItemz = self.storyboard!.instantiateViewControllerWithIdentifier("ISEditItem") as! ISEditItem
        
        if(isEditSelectedLocationForScanning == true)
        {
            isEditSelectedLocationForScanning = false
            ISEditItemz.currentLocOfEditItem = currentLocLbl.text!
            ISEditItemz.barcodeOfEditItem = barcodeLbl.text!
            ISEditItemz.decriptionOfEditItem = descLbl.text!
            ISEditItemz.numberOfEditItem = itemScanNumber
            ISEditItemz.colorOfEditItem =  colorLbl.text!
            ISEditItemz.nameOfEditItem = itemNameLbl.text!
        
        }
        else if(isEditQR == true)
        {
            isEditQR = false
            ISEditItemz.currentLocOfEditItem = currentLocLbl.text!
            ISEditItemz.barcodeOfEditItem = barcodeLbl.text!
            ISEditItemz.decriptionOfEditItem = descLbl.text!
            ISEditItemz.numberOfEditItem = itemQRNumber
            ISEditItemz.colorOfEditItem =  colorLbl.text!
            ISEditItemz.nameOfEditItem = itemNameLbl.text!
        }
        else
        {
            ISEditItemz.currentLocOfEditItem = currentLocLbl.text!
            ISEditItemz.barcodeOfEditItem = barcodeLbl.text!
            ISEditItemz.decriptionOfEditItem = descLbl.text!
            ISEditItemz.numberOfEditItem = itemNumber
            ISEditItemz.colorOfEditItem =  colorLbl.text!
            ISEditItemz.nameOfEditItem = itemNameLbl.text!
        }
        self.navigationController?.pushViewController(ISEditItemz, animated: true)
    }
    
    @IBAction func backFromSelectedItem(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    
    
    
    
    @IBAction func RecentCheckInOutButton(sender: AnyObject)
    {
//        let ISDescriptionz = self.storyboard!.instantiateViewControllerWithIdentifier("RecentItemCheckInOutViewController") as! RecentItemCheckInOutViewController
//        
//        // itemNameLbl.text = resultSet.stringForColumn("ItemName")
//        
//        ISDescriptionz.SelectedItemName = itemNameLbl.text!
//        
//        self.navigationController?.pushViewController(ISDescriptionz, animated: true)
    }
    
    
    
    
    func fetchSelectedItemDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        //        var itemDesc = ""
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemNumber = '\(itemNumber)'", whereFields: [])
        number = itemNumber
        
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ITEMASSIGNTO")
                
                // itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
//                globalItemArray.addObject(fetchFields)
                // print("fetchFields in selectedItem is \(fetchFields)")
                
                barcodeLbl.text = resultSet.stringForColumn("ItemBarcode")
                currentLocLbl.text = resultSet.stringForColumn("ItemLocation")
                descLbl.text = resultSet.stringForColumn("ItemDescription")
                colorLbl.text = resultSet.stringForColumn("ItemColor")
                
                
                print("\(resultSet.stringForColumn("ItemAssignTo"))")
                
                if(resultSet.stringForColumn("ItemAssignTo") == nil)
                {
                    assignLbl.text = "Assign To"
                }
                else
                {
                    
                    if (resultSet.stringForColumn("ItemAssignTo") == " ")
                    {
                        assignLbl.text = "Assign To Me"
                    }
                    else
                    {
                         assignLbl.text = "Assign To \(resultSet.stringForColumn("ItemAssignTo"))"
                    }
                   
                }
                
                let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
                let imagePath = "\(filePath)/\(itemNumber).jpeg"
                
                // print("imagePath in \(imagePath)")
                
                let fileManager = NSFileManager.defaultManager()
                
                if (fileManager.fileExistsAtPath(imagePath))
                {
                    itemDetailsImageView.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
                }
                else
                {
                    itemDetailsImageView.image =  UIImage(named: "img.png")
                }
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }
    
    
    func fetchScannedbarcode()
    {
         UpdateLocation()
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemBarcode = '\(itemBarcodeForSelectedItem)'", whereFields: [])
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                
                 itemScanNumber = resultSet.stringForColumn("ItemNumber")
                
                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ITEMASSIGNTO")
                
                //                        itemNumberStr = resultSet.stringForColumn("ItemNumber")
                arrayForScannedBarcode.addObject(fetchFields)
                
                print("arrayForScannedBarcode is \(arrayForScannedBarcode)")
 
                barcodeLbl.text = resultSet.stringForColumn("ItemBarcode")
                currentLocLbl.text = resultSet.stringForColumn("ItemLocation")
                descLbl.text = resultSet.stringForColumn("ItemDescription")
                colorLbl.text = resultSet.stringForColumn("ItemColor")
                itemNameLbl.text = resultSet.stringForColumn("ItemName")
               
                
                if(resultSet.stringForColumn("ItemAssignTo") == nil)
                {
                    assignLbl.text = "Assign To"
                }
                else
                {
                    assignLbl.text = "Assign To \(resultSet.stringForColumn("ItemAssignTo"))"
                }
                
                let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
                let imagePath = "\(filePath)/\(itemScanNumber).jpeg"
                
                // print("imagePath in \(imagePath)")
                
                let fileManager = NSFileManager.defaultManager()
                
                if (fileManager.fileExistsAtPath(imagePath))
                {
                    itemDetailsImageView.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
                }
                else
                {
                    itemDetailsImageView.image =  UIImage(named: "img.png")
                }
            }
        }
        resultSet.close()
    }
    
    func UpdateLocation()
    {
        isEditSelectedLocationForScanning = true
        print("locationSelectedByScanning is \(locationSelectedByScanning)")
        print("peopleSeclectedForScannedBarcode is \(peopleSeclectedForScannedBarcode)")
        
        var tblFields: Dictionary! = [String: String]()
        
        if(isCheckOutForPeopleSelected == true)
        {
            
            tblFields["ItemAssignTo"]  = peopleSeclectedForScannedBarcode as String
        }
        else
        {
           
            tblFields["ItemLocation"] = locationSelectedByScanning as String
        }

        _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemBarcode = '\(itemBarcodeForSelectedItem)'", whereFields: [])
    }
    
    func fetchQRBarcode()
    {
        UpdateLocationForQR()
        
        print ("searchTextField.text is\(SearchString)")
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemName = '\(SearchString)'", whereFields: [])
        
        resultSet.next()
        
        let messageCount = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        if messageCount == 0
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Item QR Code does not exist!!"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
            
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemName = '\(SearchString)'", whereFields: [])
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                    
                    fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                    itemQRNumber = resultSet.stringForColumn("ItemNumber")
                    
                    fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                    fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                    fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                    fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                    fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                    fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                    fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ITEMASSIGNTO")
                    
                    //                        itemNumberStr = resultSet.stringForColumn("ItemNumber")
                    arrayForQRCode.addObject(fetchFields)
                    
                    print("arrayForQRCode for assigning people is \(arrayForQRCode)")
                    
                    
                    barcodeLbl.text = resultSet.stringForColumn("ItemBarcode")
                    currentLocLbl.text = resultSet.stringForColumn("ItemLocation")
                    descLbl.text = resultSet.stringForColumn("ItemDescription")
                    colorLbl.text = resultSet.stringForColumn("ItemColor")
                    itemNameLbl.text = resultSet.stringForColumn("ItemName")
                    
                    
                    if(resultSet.stringForColumn("ItemAssignTo") == nil)
                    {
                        assignLbl.text = "Assign To"
                    }
                    else
                    {
                        assignLbl.text = "Assign To \(resultSet.stringForColumn("ItemAssignTo"))"
                    }
                    
                    let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
                    let imagePath = "\(filePath)/\(itemQRNumber).jpeg"
                    
                    // print("imagePath in \(imagePath)")
                    
                    let fileManager = NSFileManager.defaultManager()
                    
                    if (fileManager.fileExistsAtPath(imagePath))
                    {
                        itemDetailsImageView.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
                    }
                    else
                    {
                        itemDetailsImageView.image =  UIImage(named: "img.png")
                    }
                }
            }
            resultSet.close()
        }
    }
    
    func UpdateLocationForQR()
    {
        isEditQR = true
        print("locationSelectedByScanning is \(locationSelectedByScanning)")
        print("peopleSeclectedForScannedBarcode is \(peopleSeclectedForScannedBarcode)")
        
        var tblFields: Dictionary! = [String: String]()
        
        if(isCheckOutQRForPeople == true)
        {
            
            tblFields["ItemAssignTo"]  = peopleSeclectedForScannedBarcode as String
        }
        else
        {
            
            tblFields["ItemLocation"] = locationSelectedByScanning as String
        }
        
        _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemName = '\(SearchString)'", whereFields: [])
    }
}
