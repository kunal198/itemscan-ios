//
//  ISEditProfile.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/8/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MobileCoreServices

var isEditTrue = Bool()

class ISEditProfile: UIViewController,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITextFieldDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var username:String = String()
 
    @IBOutlet weak var editProfileImgView: UIImageView!
    @IBOutlet weak var repeatPasswordEdit: UITextField!
    @IBOutlet weak var passwordEdit: UITextField!
    @IBOutlet weak var emailEdit: UITextField!
    @IBOutlet weak var fullnameEdit: UITextField!
    @IBOutlet weak var usernameEdit: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placeHolderMethodForEditProfile()
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        // Do any additional setup after loading the view.
        
        
        let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
        let imagePath = "\(filePath)/profilePic.png"
        
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
            editProfileImgView.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
            
            // self.profileImageView.image = UIImage(data: fileManager.contentsAtPath(imagePath)!)
            //  buttonName.setTitle("Replace Image", forState: UIControlState.Normal)
        }
        else
        {
            editProfileImgView.image =  UIImage(named: "img.png")
            //self.profileImageView.image = UIImage(named: "default.png")
            //  buttonName.setTitle("Add Image", forState: UIControlState.Normal)
        }
        
        
        username = NSUserDefaults.standardUserDefaults().objectForKey("USER") as! String
        emailEdit.text = emailIdName
        usernameEdit.text = username as String
        

    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func placeHolderMethodForEditProfile()
    {
        usernameEdit.attributedPlaceholder = NSAttributedString(string:"User Name", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 15)!])
        fullnameEdit.attributedPlaceholder = NSAttributedString(string:"Name", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 15)!])
//        passwordEdit.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 15)!])
//        repeatPasswordEdit.attributedPlaceholder = NSAttributedString(string:"Repeat Password", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 15)!])
        emailEdit.attributedPlaceholder = NSAttributedString(string:"Email", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 15)!])

    }
    
    @IBAction func setImage(sender: AnyObject)
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    @IBAction func backEditProfile(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }
        
        
        UIView.animateWithDuration(0.2, animations:
            {
            
            // self.navBar.frame.origin.x = self.navx
            myMenuView.frame.origin.x = self.x
            
        })
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        usernameEdit.resignFirstResponder()
        fullnameEdit.resignFirstResponder()
        emailEdit.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        usernameEdit.resignFirstResponder()
        fullnameEdit.resignFirstResponder()
        emailEdit.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.view.frame.origin.y = 0.0
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        if textField == emailEdit
        {
            self.view.frame.origin.y = -25.0
        }
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex)
        {
        case 0:
             print("Cancel")
        case 1:
            Camera()
        case 2:
            Gallery()
        default:
             print("Default")
            
        }
    }
    
    func Camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            // print("Camera capture")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
            
            
        }
    }
    
    func Gallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        // print("ISShowBarcodeDetail.swift delegate method")
        self.dismissViewControllerAnimated(false, completion: nil)
        
        editProfileImgView.image = image
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
        let fileManager = NSFileManager.defaultManager()
        
        let filePathToWrite = "\(documentDirectory)/profilePic.png"
        
        let data: NSData = UIImagePNGRepresentation(image)!
        
        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
        
  
        
        // print("filePathToWrite is \(filePathToWrite)")
        
    }
    
    func isValidEmail(emailid: NSString)->Bool
    {
        var isValid = true
        
        var isTrimmed = true
        
        var emailId = emailid
        
        while(isTrimmed)
        {
            
            let startSpace = emailId.substringWithRange(NSRange(location: 0, length: 1))
            
            let endSpace = emailId.substringWithRange(NSRange(location: emailId.length-1, length: 1))
            
            if startSpace == " " || endSpace == " "
            {
                if startSpace == " "
                {
                    emailId = emailId.stringByReplacingCharactersInRange(NSRange(location: 0, length: 1), withString: "")
                }
                if endSpace == " "
                {
                    emailId = emailId.stringByReplacingCharactersInRange(NSRange(location: emailId.length-1, length: 1), withString: "")
                }
            }
            else
            {
                isTrimmed = false
            }
        }
        
        
        if !emailId.containsString(" ")
        {
            var atRateSplitArray = emailId.componentsSeparatedByString("@")
            
            if(atRateSplitArray.count>=2)
            {
                for component in atRateSplitArray
                {
                    if component == ""
                    {
                        isValid = false
                    }
                }
                
                if(isValid)
                {
                    let dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                    
                    if(dotSplitArray.count>=2)
                    {
                        for component in dotSplitArray
                        {
                            if component == ""
                            {
                                isValid = false
                            }
                        }
                    }
                    else
                    {
                        isValid = false
                    }
                }
            }
            else
            {
                isValid = false
            }
            
        }
        else
        {
            isValid = false
        }
        
        return isValid
    }


    @IBAction func nextBtnEditProfile(sender: AnyObject)
    {
        myMenuView.hidden = true
        

        if usernameEdit.text == "" || fullnameEdit.text == "" || emailEdit.text == ""
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Fill text fields"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
        else if !isValidEmail(emailEdit.text!)
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Enter valid emailid."
            alert1.addButtonWithTitle("Ok")
            alert1.show()
            
        }
        else
        {
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(fullnameEdit.text, forKey: "user")
            defaults.setObject(emailEdit.text, forKey: "email")
            defaults.setObject(usernameEdit.text,forKey: "nameEdit")
            isEditTrue = true
            
            self.navigationController!.popViewControllerAnimated(true)
        }

    }
}
