//
//  ISCustomCell.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/8/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISCustomCell: UITableViewCell
{
    
    @IBOutlet weak var recentStatus: UILabel!
    @IBOutlet weak var recentItemName: UILabel!
      //*********************************//
    
    @IBOutlet var StatusLbl: UILabel!
    @IBOutlet weak var transitItemAssignTo: UILabel!
    @IBOutlet weak var transitItemNAme: UILabel!
    
   //*********************************//
    @IBOutlet weak var assignPeopleHighlightview: UIImageView!
    @IBOutlet weak var assignPeople: UILabel!
    @IBOutlet weak var peopleName: UILabel!
    @IBOutlet weak var peopleHighlightview: UIImageView!
    @IBOutlet weak var selectedItem: UILabel!
    @IBOutlet weak var selectedImgView: UIImageView!
    @IBOutlet weak var selectedHighlitedImgView: UIImageView!
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var menuImageView: UIImageView!
    //*********************************//
    @IBOutlet weak var settingLbl: UILabel!
    //*********************************//
    @IBOutlet weak var showChecked: UILabel!
    @IBOutlet weak var showSample: UILabel!
    
    @IBOutlet weak var showStatus: UILabel!
   
    @IBOutlet weak var showItemLbl: UILabel!
    //*********************************//
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryListingHighlightView: UIImageView!
    //****************************//
    @IBOutlet weak var Loc_name: UILabel!
    @IBOutlet weak var Loc_listingImgView: UIImageView!
    @IBOutlet weak var Loc_ListingHighlightview: UIImageView!
    
    @IBOutlet weak var itemStatus: UILabel!
    @IBOutlet weak var itemAddress: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemHighlightView: UIImageView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
