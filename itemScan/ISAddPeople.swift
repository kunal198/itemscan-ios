//
//  ISAddPeople.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/20/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISAddPeople: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate
{
    
    var PeopleNameForInsertion:NSString = NSString()

    let alertNameSuccess = UIAlertView()
      var peopleListingArray:NSMutableArray = NSMutableArray()
    
    @IBOutlet var PeopleTableView: UITableView!
    @IBOutlet var NameMeLbl: UILabel!
    @IBOutlet weak var pplView: UIView!
    @IBOutlet weak var addPeopleLbl: UITextField!
    
    @IBOutlet var backview: UIView!
    override func viewDidLoad()
    {
        placeHolderMethodForAddPeople()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        let defaultsArray = NSUserDefaults.standardUserDefaults()
        // let defaultsArray = NSUserDefaults.standardUserDefaults()
        let savedValueArray = defaultsArray.objectForKey("SavedStringArray") as? [AnyObject] ?? [AnyObject]()
        
        print("saved array is = \(savedValueArray)")
        
        
        var savedName = String()
        savedName =  (savedValueArray[0].valueForKey("name") as? String)!
        
        //   self.peopleTableView.reloadData()
        
        if savedName != ""
        {
            self.NameMeLbl.text = "Me"
        }

        
          fetchPeopleListingDataBase()
    }
    
    func fetchPeopleListingDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("PeopleListing", selectColumns: ["*"], whereString: "", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("PeopleListName"), forKey: "PEOPLELIST")
                arr.addObject(fetchFields)
                
                
                peopleListingArray.addObject(fetchFields)
                // print("peopleListingArray is \(arr)")
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // print("peopleListingArray.count is \(peopleListingArray.count)")
        return peopleListingArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        
        let defaultsArray = NSUserDefaults.standardUserDefaults()
        // let defaultsArray = NSUserDefaults.standardUserDefaults()
        let savedValueArray = defaultsArray.objectForKey("SavedStringArray") as? [AnyObject] ?? [AnyObject]()
        
        print("saved array is = \(savedValueArray)")
        
        //        if indexPath.row == 0
        //        {
        //            cell.peopleName.text = savedValueArray[0].valueForKey("name") as? String
        //        }
        //        else
        //        {
        cell.peopleName.text = peopleListingArray[indexPath.row].valueForKey("PEOPLELIST") as? String
        //      }
        
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        //        peopleSeclectedForScannedBarcode = (peopleListingArray[indexPath.row].valueForKey("PEOPLELIST") as? String)!
        //
        //            let createAccountErrorAlert: UIAlertView = UIAlertView()
        //            createAccountErrorAlert.delegate = self
        //            createAccountErrorAlert.title = "Alert"
        //            createAccountErrorAlert.message = "Please Choose one"
        //            createAccountErrorAlert.addButtonWithTitle("Scan QR code")
        //            createAccountErrorAlert.addButtonWithTitle(" Scan Bar Code")
        //            createAccountErrorAlert.addButtonWithTitle("Cancel")
        //            createAccountErrorAlert.show()
        
        //        if  btnCheck == "Back"
        //        {
        //            isCheckOutForPeopleSelected = true
        //        }
        //        else{
        //            isCheckOutForPeopleSelected = false
        //        }
        
        
        if(isCheckOutForPeopleSelected == true)
        {
            peopleSeclectedForScannedBarcode = (peopleListingArray[indexPath.row].valueForKey("PEOPLELIST") as? String)!
            
            let createAccountErrorAlert: UIAlertView = UIAlertView()
            createAccountErrorAlert.delegate = self
            createAccountErrorAlert.title = "Alert"
            createAccountErrorAlert.message = "Please Choose one"
            createAccountErrorAlert.addButtonWithTitle("Scan QR code")
            createAccountErrorAlert.addButtonWithTitle(" Scan Bar Code")
            createAccountErrorAlert.addButtonWithTitle("Cancel")
            createAccountErrorAlert.show()
            
            
            //  isCheckOutForPeopleSelected = false
        }
        else
        {
            let ISLocationProfileViewz = self.storyboard!.instantiateViewControllerWithIdentifier("PeopleProfileViewController") as! PeopleProfileViewController
            //            ISLocationProfileViewz.descriptionLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCDESCRIPTION") as? String)!
            //            ISLocationProfileViewz.locLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCADDRESS") as? String)!
            //            ISLocationProfileViewz.LocationName = (globalLocationListingArray[indexPath.row].valueForKey("LOCNAME") as? String)!
            //            ISLocationProfileViewz.numberOfLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCNUMBER") as? String)!
            
            ISLocationProfileViewz.peopleName = (peopleListingArray[indexPath.row].valueForKey("PEOPLELIST") as? String)!
            
            ISLocationProfileViewz.peopleString = "PeopleProfile"
            
            self.navigationController?.pushViewController(ISLocationProfileViewz, animated: true)
            
            
            //isCheckOutForPeopleSelected = true
        }
        
    }

    
    
    @IBAction func backAddPeople(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func saveBtnAddPeople(sender: AnyObject)
    {
        if (addPeopleLbl.text == "")
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "All text fields are mandatory"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
        else
        {
            insertPeopleDataBase()
            fetchPeopleDataBase()
           // self.navigationController?.popViewControllerAnimated(true)
        }
    }

    func placeHolderMethodForAddPeople()
    {
        addPeopleLbl.attributedPlaceholder = NSAttributedString(string:" Add People", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        pplView.backgroundColor = UIColor.clearColor()
        pplView.layer.cornerRadius = 6
        pplView.layer.borderWidth = 2
        pplView.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        addPeopleLbl.resignFirstResponder()
    }
    
    func insertPeopleDataBase()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["PeopleName"] = addPeopleLbl.text!
        PeopleNameForInsertion = addPeopleLbl.text!

        _ = ModelManager.instance.addTableData("People", primaryKey:"PeopleNumber", tblFields: tblFields)
        
        // print("insertPeopleDataBase is \(tblFields)")
    }
    
    
    
    
    func fetchPeopleDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        // print("PeopleNameForInsertion is \(PeopleNameForInsertion)")
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("PeopleListing", selectColumns: ["COUNT(*) as count"], whereString: "PeopleListName = '\(PeopleNameForInsertion)'", whereFields: [])
        arr = []
        
        resultSet.next()
        
        let messageCount = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        if messageCount == 0
        {
            var tblFields: Dictionary! = [String: String]()
            tblFields["PeopleListName"] = PeopleNameForInsertion as String
            
            _ = ModelManager.instance.addTableData("PeopleListing", primaryKey:"PeopleListNumber", tblFields: tblFields)
            
           // let alert1 = UIAlertView()
            alertNameSuccess.title = "Alert"
            alertNameSuccess.message = "People added successfully."
            alertNameSuccess.addButtonWithTitle("Ok")
            alertNameSuccess.show()
            alertNameSuccess.delegate = self
        }
        else
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "People with this name already exists."
            alert1.addButtonWithTitle("Ok")
            alert1.show()
            addPeopleLbl.text  = ""
        }
    }

    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertView == alertNameSuccess
        {
            addPeopleLbl.resignFirstResponder()
             self.navigationController?.popViewControllerAnimated(true)
        }
    }
}

