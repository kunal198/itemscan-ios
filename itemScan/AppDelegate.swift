//
//  AppDelegate.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/7/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit


var LocationButtonCheck = String()


var x: CGFloat = 0.0
var urlLink = String()
var navBarTitle = String()
var currentVC:UIViewController = UIViewController()
var postPropertyVC: UIViewController = UIViewController()
var myMenuView :UIView = UIView()
var closeMenuBtn:UIButton = UIButton()
var barButtonCheck = String()
let dashBoard_btn:UIButton = UIButton()
let advancedSearch_btn:UIButton = UIButton()
let propertyForSale_btn:UIButton = UIButton()
let propertyForRent_btn:UIButton = UIButton()
let postProperty_btn:UIButton = UIButton()
let callnow_btn:UIButton = UIButton()
let contactUS_btn:UIButton = UIButton()
let profile_Btn:UIButton = UIButton()
let back_Btn:UIButton = UIButton()
let people_Btn:UIButton = UIButton()
let aboutUs_Btn:UIButton = UIButton()
let feedBack_btn:UIButton = UIButton()
var dashBoard_Image : UIImageView = UIImageView()
var advancedSearch_Image : UIImageView = UIImageView()
var propertyForSale_Image : UIImageView = UIImageView()
var propertyForRent_Image : UIImageView = UIImageView()
var postProperty_Image : UIImageView = UIImageView()
var aboutUs_Image : UIImageView = UIImageView()
var feedBack_Image : UIImageView = UIImageView()
var callnow_image: UIImageView = UIImageView()
var contactUS_Image : UIImageView = UIImageView()
var profile_Image : UIImageView = UIImageView()
var back_Image : UIImageView = UIImageView()
var people_Image : UIImageView = UIImageView()
let shopnow:UILabel = UILabel()
let dashBoardLAbel:UILabel = UILabel()
let advancedSearchLabel:UILabel = UILabel()
let propertyForSaleLabel:UILabel = UILabel()
let propertyForRentLabel:UILabel = UILabel()
let postPropertyLabel:UILabel = UILabel()
let aboutUsLabel:UILabel = UILabel()
let feedBackLabel:UILabel = UILabel()
let callnow_label :UILabel = UILabel()
let contactUSLabel:UILabel = UILabel()
let profileLabel:UILabel = UILabel()
let backLabel:UILabel = UILabel()
let peopleLabel:UILabel = UILabel()
var isMenuSelected = false
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    
    var menu_Label:UILabel = UILabel()
    
    let lineImage: UIImageView = UIImageView()
 
    //MARK: 1.DashBoard View Variables
    var dashBoardView :UIView = UIView()
    var dashBoardView_line:UILabel = UILabel()

    //MARK: 2.About View Variables
    var advancedSearchView :UIView?
    var advanceSearchView_line:UILabel = UILabel()
    
    //MARK: 4.Resale Properties View Variables
    var propertyForSaleView :UIView?
    var propertyForSaleView_line:UILabel = UILabel()
    
    
    //MARK: 5.Exchange Property View Variables
    var propertyForRentView :UIView?
    var propertyForRentView_line:UILabel = UILabel()

    //MARK: 6.Post Property View Variables
    var postPropertyView :UIView?
    var postPropertyView_line:UILabel = UILabel()
    
    //MARK: 7.Contact Us View Variables
    var  aboutUsView:UIView?
    var aboutUsView_line:UILabel = UILabel()
    
    
    //MARK: 8.Contact Us View Variables
    var  profileView:UIView?
    var profileView_line:UILabel = UILabel()
    
    //MARK: 10.back Us View Variable
    var  backView:UIView?
    var backView_line:UILabel = UILabel()
    
    //MARK: 11.people Us View Variable
    var  peopleView:UIView?
    var peopleView_line:UILabel = UILabel()
    
    
    
    //MARK: 7.Contact Us View Variables
    var  feedBackView:UIView?
    var feedBackView_line:UILabel = UILabel()
    
    
    
    //MARK: 9.Contact US View Variables
    var  contactUSView:UIView?
    var contactUSView_line:UILabel = UILabel()
    
    //MARK: 9.Contact US View Variables
    var  callnowView:UIView?
    var callnowView_lINE:UILabel = UILabel()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        
//        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        // Override point for customization after application launch.
        if NSUserDefaults.standardUserDefaults().boolForKey("Login")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let ISHomeScreenz = storyboard.instantiateViewControllerWithIdentifier("ISHomeScreen") as! ISHomeScreen
            
            let navigationController = self.window?.rootViewController as! UINavigationController
            navigationController.pushViewController(ISHomeScreenz, animated: false)
        }
        creatingMenu()
        
        GetFilePath.copyFile("ItemScanDB.sqlite")
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
        return true
    }
  
    ///////++++++++++++++++++++/////////////////
    
    //MARK: 9.1 createmenu
    func creatingMenu()
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad)
        {
            myMenuView = UIView(frame:CGRectMake(0, 60,(220/320)*UIScreen.mainScreen().bounds.size.width, (780/568)*UIScreen.mainScreen().bounds.size.height))
            
        }
        else
        {
            if (UIScreen.mainScreen().bounds.size.height == 568)
            {
                myMenuView = UIView(frame:CGRectMake(0, 20, 250, 525))
                
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 480)
            {
                
                myMenuView = UIView(frame: CGRectMake(0, 20, 250, 780))
                
            }
                
            else if(UIScreen.mainScreen().bounds.size.height == 736)
            {
                myMenuView = UIView(frame: CGRectMake(0, 20, 250, UIScreen.mainScreen().bounds.size.height))
            }
            else
            {
                myMenuView = UIView(frame:CGRectMake(0, 20,250 , UIScreen.mainScreen().bounds.size.height))
            }
        }
        
        closeMenuBtn.frame = CGRectMake(64,64,UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height)
        closeMenuBtn.backgroundColor = UIColor.clearColor()
        closeMenuBtn.alpha = 0.5
        closeMenuBtn.hidden = true
                    closeMenuBtn.addTarget(self, action:"closeMenuBtnClicked:",forControlEvents: UIControlEvents.TouchUpInside)
        
        window?.addSubview(myMenuView)
        
        myMenuView.backgroundColor = UIColor.whiteColor()
        


        if(UIScreen.mainScreen().bounds.size.height == 667)
        {
            
            dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (50/568)*UIScreen.mainScreen().bounds.size.height))
            
            dashBoardView.backgroundColor =  UIColor.clearColor()
            
            
        }
        else if(UIScreen.mainScreen().bounds.size.height == 736)
        {
            
            dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (50/568)*UIScreen.mainScreen().bounds.size.height))
            
            dashBoardView.backgroundColor =  UIColor.clearColor()
        }
            
        else if(UIScreen.mainScreen().bounds.size.height == 568)
        {
            dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (50/568)*UIScreen.mainScreen().bounds.size.height))
            
            dashBoardView.backgroundColor =  UIColor.clearColor()
        }
            
        else
        {
            dashBoardView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (250/320)*UIScreen.mainScreen().bounds.size.width, (65/568)*UIScreen.mainScreen().bounds.size.height))
            
            dashBoardView.backgroundColor =  UIColor.clearColor()
        }
        myMenuView .addSubview(dashBoardView)
        
        
        dashBoardLAbel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(15/568)*UIScreen.mainScreen().bounds.size.height,(190/414)*UIScreen.mainScreen().bounds.size.width,(20/568)*UIScreen.mainScreen().bounds.size.height)
      print (dashBoardView.frame)
        dashBoardLAbel.textColor = UIColor.blackColor()
        dashBoardLAbel.textAlignment = NSTextAlignment.Left
        dashBoardLAbel.text = "Home"
        dashBoardLAbel.backgroundColor = UIColor.clearColor()
        dashBoardLAbel.font = UIFont(name:"HelveticaNeue", size: 18.0)
        
        dashBoardLAbel.tag = 0
        
        myMenuView .addSubview(dashBoardLAbel)
        

        // homeLabel.backgroundColor = UIColor.whiteColor()
        
        
        dashBoard_Image  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (10/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        dashBoard_Image.image = UIImage(named:"home.png")
        
        
        dashBoard_btn .addSubview(dashBoard_Image)
        
    dashBoard_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(250/320)*UIScreen.mainScreen().bounds.size.width,(46/568)*UIScreen.mainScreen().bounds.size.height)
        
        dashBoard_btn.backgroundColor = UIColor.clearColor()
        dashBoard_btn.setTitle ("", forState: UIControlState.Normal)
        dashBoard_btn.addTarget(self, action:"Home:",forControlEvents: UIControlEvents.TouchUpInside)
        
        dashBoardView.addSubview(dashBoard_btn)
        
        dashBoard_btn.tag = 0
        
        dashBoard_btn.backgroundColor = UIColor.clearColor()

        dashBoardView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(46/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        dashBoardView_line.backgroundColor =  UIColor.grayColor()
        
        myMenuView.addSubview(dashBoardView_line)

        //MARK: - 2.About View
        advancedSearchView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (50/568)*UIScreen.mainScreen().bounds.size.height, (190/320)*UIScreen.mainScreen().bounds.size.width, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        advancedSearchView!.backgroundColor = UIColor.clearColor()
        
        myMenuView.addSubview(advancedSearchView!)
        
        advancedSearch_Image = UIImageView(frame:CGRectMake((12/320)*UIScreen.mainScreen().bounds.size.width, (10/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        advancedSearch_Image.image = UIImage(named:"IntransitMenuBAr.png")
        advancedSearch_Image.contentMode = UIViewContentMode.ScaleAspectFit
        
        advancedSearch_btn.addSubview(advancedSearch_Image)
        
        
        
        advancedSearchLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(20/568)*UIScreen.mainScreen().bounds.size.height,(150/414)*UIScreen.mainScreen().bounds.size.width,(15/568)*UIScreen.mainScreen().bounds.size.height)
        
        //newEssayLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        advancedSearchLabel.textColor = UIColor(red: 85.0/255, green: 85.0/255, blue: 85.0/255, alpha: 1.0)
        advancedSearchLabel.textAlignment = NSTextAlignment.Left
        advancedSearchLabel.text = "In Transit"
         advancedSearchLabel.textColor = UIColor.blackColor()
        advancedSearchLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)
        
        
        advancedSearchLabel.tag = 1
        
        
        advancedSearch_btn.addSubview(advancedSearchLabel)
 
        advancedSearch_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(190/320)*UIScreen.mainScreen().bounds.size.width,(30/568)*UIScreen.mainScreen().bounds.size.height)
        
        advancedSearch_btn.backgroundColor = UIColor.clearColor()
        advancedSearch_btn.setTitle ("", forState: UIControlState.Normal)
        advancedSearch_btn.addTarget(self, action:"goToAdvancedSearchVC:",forControlEvents: UIControlEvents.TouchUpInside)

        // advancedSearch_btn.backgroundColor = UIColor.blackColor()
        
        advancedSearchView!.tag = 95
        
        advancedSearchView?.addSubview(advancedSearch_btn)
        
        advancedSearch_btn.tag = 1
        
        
        
        advanceSearchView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(100/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        advanceSearchView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(advanceSearchView_line)
        
        //MARK: - 3.Mega Projects View
        propertyForSaleView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (115/568)*UIScreen.mainScreen().bounds.size.height, (190/320)*UIScreen.mainScreen().bounds.size.width, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForSaleView!.backgroundColor = UIColor.clearColor()
        
        myMenuView.addSubview(propertyForSaleView!)
        
        
        
        propertyForSale_Image  = UIImageView(frame:CGRectMake((12/320)*UIScreen.mainScreen().bounds.size.width,(-6/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForSale_Image.image = UIImage(named:"recentMenuBar.png")
        propertyForSale_Image.contentMode = UIViewContentMode.ScaleAspectFit
        
        propertyForSale_btn.addSubview(propertyForSale_Image)
        
        
        
        propertyForSale_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(190/320)*UIScreen.mainScreen().bounds.size.width,(30/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForSale_btn.backgroundColor = UIColor.clearColor()
        
        propertyForSale_btn.setTitle ("", forState: UIControlState.Normal)
        propertyForSale_btn.addTarget(self, action:"goToPropertiesForSaleVC:",forControlEvents: UIControlEvents.TouchUpInside)

        propertyForSaleView?.addSubview(propertyForSale_btn)
        
        propertyForSale_btn.tag = 2

        propertyForSaleLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(2/568)*UIScreen.mainScreen().bounds.size.height,(120/320)*UIScreen.mainScreen().bounds.size.width,(15/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        propertyForSaleLabel.textColor = UIColor(red: 85.0/255, green: 85.0/255, blue: 85.0/255, alpha: 1.0)
        propertyForSaleLabel.textAlignment = NSTextAlignment.Left
        propertyForSaleLabel.text = "Recent"
         propertyForSaleLabel.textColor = UIColor.blackColor()
        propertyForSaleLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)

        propertyForSale_btn.addSubview(propertyForSaleLabel)
        
        propertyForSale_btn.tag = 2

        propertyForSaleView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(150/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForSaleView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(propertyForSaleView_line)

        //MARK: - 4.Resale Properties View
        propertyForRentView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (160/568)*UIScreen.mainScreen().bounds.size.height, (190/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        propertyForRentView!.backgroundColor = UIColor.clearColor()
        
        myMenuView.addSubview(propertyForRentView!)
        
        
        
        propertyForRent_Image  = UIImageView(frame:CGRectMake((12/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,  (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        
        propertyForRent_Image.image = UIImage(named:"locationBlack.png")
        propertyForRent_Image.contentMode = UIViewContentMode.ScaleAspectFit
        
        
        propertyForRent_btn.addSubview(propertyForRent_Image)
        
        propertyForRent_btn.tag = 3
        
        propertyForRentLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(10/568)*UIScreen.mainScreen().bounds.size.height,(160/414)*UIScreen.mainScreen().bounds.size.width,(15/568)*UIScreen.mainScreen().bounds.size.height)
        
        // underReviewLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        propertyForRentLabel.textColor = UIColor(red: 85.0/255, green: 85.0/255, blue: 85.0/255, alpha: 1.0)
        
        propertyForRentLabel.textAlignment = NSTextAlignment.Left
        propertyForRentLabel.text = "Location"
        propertyForRentLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)
         propertyForRentLabel.textColor = UIColor.blackColor()
        propertyForRent_btn.addSubview(propertyForRentLabel)
        
        
        //resalePropertiesLabel.backgroundColor = UIColor.greenColor()
        
        propertyForRentLabel.tag = 3
        
        
        propertyForRent_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(200/320)*UIScreen.mainScreen().bounds.size.width,(50/568)*UIScreen.mainScreen().bounds.size.height)
        
        
        propertyForRent_btn.backgroundColor = UIColor.clearColor()
        //myEssayButton.textColor = UIColor.blackColor()
        propertyForRent_btn.setTitle ("", forState: UIControlState.Normal)
        propertyForRent_btn.addTarget(self, action:"goToPropertiesOnRentVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        propertyForRentView?.addSubview(propertyForRent_btn)
        
        
        
        
        //propertyForRent_btn.backgroundColor = UIColor.blackColor()
        
        
        propertyForRentView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(200/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        propertyForRentView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(propertyForRentView_line)

        //MARK: - 5.Exchange Properties View
        postPropertyView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width, (215/568)*UIScreen.mainScreen().bounds.size.height, (190/320)*UIScreen.mainScreen().bounds.size.width, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        postPropertyView!.backgroundColor = UIColor.clearColor()
        
        myMenuView.addSubview(postPropertyView!)
   
        postProperty_Image  = UIImageView(frame:CGRectMake((11/320)*UIScreen.mainScreen().bounds.size.width, (-2/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        postProperty_Image.image = UIImage(named:"itemMenuBar.png")
        postProperty_Image.contentMode = UIViewContentMode.ScaleAspectFit
        
        
        postProperty_btn.addSubview(postProperty_Image)
        
        postProperty_btn.tag = 4

        postPropertyLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //MyPlanLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        postPropertyLabel.textColor = UIColor(red: 85.0/255, green: 85.0/255, blue: 85.0/255, alpha: 1.0)
        postPropertyLabel.textAlignment = NSTextAlignment.Left
        postPropertyLabel.text = "Items"
        postPropertyLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)
        postPropertyLabel.textColor = UIColor.blackColor()
        postProperty_btn.addSubview(postPropertyLabel)
 
        postPropertyLabel.tag = 4
        
        postProperty_btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(190/320)*UIScreen.mainScreen().bounds.size.width,(60/568)*UIScreen.mainScreen().bounds.size.height)
        
        postProperty_btn.backgroundColor = UIColor.clearColor()
        //myEssayButton.textColor = UIColor.blackColor()
        postProperty_btn.setTitle ("", forState: UIControlState.Normal)
        postProperty_btn.addTarget(self, action:"goToExchangePropertyVC:",forControlEvents: UIControlEvents.TouchUpInside)
        //        postPropertyView!.bringSubviewToFront(postProperty_btn)
        postProperty_btn.userInteractionEnabled = true
        postPropertyView?.addSubview(postProperty_btn)
        
        
        
        // postProperty_btn.backgroundColor = UIColor.blackColor()
        
        
        
        postPropertyView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(250/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        postPropertyView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(postPropertyView_line)

        //MARK: - 6.Post Property View
        aboutUsView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(260/568)*UIScreen.mainScreen().bounds.size.height, (190/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        aboutUsView!.backgroundColor = UIColor.clearColor()
        
        myMenuView.addSubview(aboutUsView!)
        
        
        aboutUs_Image  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        aboutUs_Image.image = UIImage(named:"settings.png")
        aboutUs_Image.contentMode = UIViewContentMode.ScaleAspectFit
        
        aboutUs_Btn.addSubview(aboutUs_Image)
        
        aboutUs_Btn.tag = 5
        
        aboutUsLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //myAccountLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        aboutUsLabel.textColor =  UIColor(red: 85.0/255, green: 85.0/255, blue: 85.0/255, alpha: 1.0)
        
        aboutUsLabel.textAlignment = NSTextAlignment.Left
        aboutUsLabel.text = "Settings"
        aboutUsLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)
         aboutUsLabel.textColor = UIColor.blackColor()
        aboutUsLabel.tag = 5
        
        
        aboutUs_Btn.addSubview(aboutUsLabel)
        
        
        aboutUs_Btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(190/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        aboutUs_Btn.backgroundColor = UIColor.clearColor()
        aboutUs_Btn.setTitle ("", forState: UIControlState.Normal)
        aboutUs_Btn.addTarget(self, action:"goToAboutUsVC:",forControlEvents: UIControlEvents.TouchUpInside)

        aboutUsView?.addSubview(aboutUs_Btn)

        aboutUsView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(300/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        aboutUsView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(aboutUsView_line)
 
        //////////////////////
        
        //MARK: - 8.profile Property View
        profileView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(310/568)*UIScreen.mainScreen().bounds.size.height, (190/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        profileView!.backgroundColor = UIColor.clearColor()
        
        myMenuView.addSubview(profileView!)
        
        
        profile_Image  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        profile_Image.image = UIImage(named:"profile.png")
        profile_Image.contentMode = UIViewContentMode.ScaleAspectFit
        
        profile_Btn.addSubview(profile_Image)
        
        profile_Btn.tag = 6

        profileLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(5/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //myAccountLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        profileLabel.textColor =  UIColor(red: 85.0/255, green: 85.0/255, blue: 85.0/255, alpha: 1.0)
         profileLabel.textColor = UIColor.blackColor()
        profileLabel.textAlignment = NSTextAlignment.Left
        profileLabel.text = "Profile"
        profileLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)
        
        profileLabel.tag = 6
        
        
        profile_Btn.addSubview(profileLabel)
        
        
        profile_Btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(240/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        profile_Btn.backgroundColor = UIColor.clearColor()
        profile_Btn.setTitle ("", forState: UIControlState.Normal)
        profile_Btn.addTarget(self, action:"goToProfileVC:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        profileView?.addSubview(profile_Btn)
        
        
        // aboutUs_Btn.backgroundColor = UIColor.blackColor()
        
        
        profileView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(350/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        profileView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(profileView_line)
        
        
        
        ///////+++++++++++++++++///////////
        //MARK: - 10.back Property View
        backView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(400/568)*UIScreen.mainScreen().bounds.size.height, (190/320)*UIScreen.mainScreen().bounds.size.width, (60/568)*UIScreen.mainScreen().bounds.size.height))
        
        backView!.backgroundColor = UIColor.clearColor()
        
        myMenuView.addSubview(backView!)
 
        back_Image  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (8/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height, (30/568)*UIScreen.mainScreen().bounds.size.height))
        
        back_Image.image = UIImage(named:"BlackBackArrow.png")
        back_Image.contentMode = UIViewContentMode.ScaleAspectFit
        
        back_Btn.addSubview(back_Image)
        
        back_Btn.tag = 7

        // postPropertyLabel.backgroundColor = UIColor.greenColor()
        
        backLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(10/568)*UIScreen.mainScreen().bounds.size.height,(100/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //myAccountLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        backLabel.textColor =  UIColor(red: 85.0/255, green: 85.0/255, blue: 85.0/255, alpha: 1.0)
        backLabel.textAlignment = NSTextAlignment.Left
        backLabel.text = "Back"
         backLabel.textColor = UIColor.blackColor()
        backLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)
        
        backLabel.tag = 7
        
        
        back_Btn.addSubview(backLabel)
        
        
        back_Btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(240/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        back_Btn.backgroundColor = UIColor.clearColor()
        back_Btn.setTitle ("", forState: UIControlState.Normal)
        back_Btn.addTarget(self, action:"goToBack:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        backView?.addSubview(back_Btn)

        backView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(450/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        backView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(backView_line)
        
        //////////////////////

        //MARK: - 11.People Property View
        peopleView = UIView(frame:CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(360/568)*UIScreen.mainScreen().bounds.size.height, (190/320)*UIScreen.mainScreen().bounds.size.width, (50/568)*UIScreen.mainScreen().bounds.size.height))
        
        peopleView!.backgroundColor = UIColor.clearColor()
        
        myMenuView.addSubview(peopleView!)
        
        people_Image  = UIImageView(frame:CGRectMake((10/320)*UIScreen.mainScreen().bounds.size.width, (0/568)*UIScreen.mainScreen().bounds.size.height, (32/568)*UIScreen.mainScreen().bounds.size.height, (32/568)*UIScreen.mainScreen().bounds.size.height))
        
        people_Image.image = UIImage(named:"people.png")
        people_Image.contentMode = UIViewContentMode.ScaleAspectFit
        
        people_Btn.addSubview(people_Image)
        
        people_Btn.tag = 8
        
        // postPropertyLabel.backgroundColor = UIColor.greenColor()
        
        
        peopleLabel.frame = CGRectMake((50/320)*UIScreen.mainScreen().bounds.size.width,(3/568)*UIScreen.mainScreen().bounds.size.height,(150/320)*UIScreen.mainScreen().bounds.size.width,(25/568)*UIScreen.mainScreen().bounds.size.height)
        
        //myAccountLabel.backgroundColor = UIColor(red: 242/255, green: 111/255, blue:112/255, alpha: 1)
        peopleLabel.textColor =  UIColor(red: 85.0/255, green: 85.0/255, blue: 85.0/255, alpha: 1.0)
         peopleLabel.textColor = UIColor.blackColor()
        peopleLabel.textAlignment = NSTextAlignment.Left
        peopleLabel.text = "People"
        peopleLabel.font = UIFont(name:"HelveticaNeue", size: 18.0)
        
        peopleLabel.tag = 8
        
        
        people_Btn.addSubview(peopleLabel)
        
        
        people_Btn.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(0/568)*UIScreen.mainScreen().bounds.size.height,(240/320)*UIScreen.mainScreen().bounds.size.width,(40/568)*UIScreen.mainScreen().bounds.size.height)
        
        people_Btn.backgroundColor = UIColor.clearColor()
        people_Btn.setTitle ("", forState: UIControlState.Normal)
        people_Btn.addTarget(self, action:"goToPeople:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        peopleView?.addSubview(people_Btn)
        
        
        // aboutUs_Btn.backgroundColor = UIColor.blackColor()
        
        
        peopleView_line.frame = CGRectMake((0/320)*UIScreen.mainScreen().bounds.size.width,(400/568)*UIScreen.mainScreen().bounds.size.height,myMenuView.frame.size.width,(1/568)*UIScreen.mainScreen().bounds.size.height)
        
        peopleView_line.backgroundColor = UIColor.grayColor()
        
        myMenuView.addSubview(peopleView_line)
    }

    
    func closeMenuBtnClicked(sender: UIButton)
    {
        
        x = -myMenuView.frame.size.width
         sender.hidden = true
        
        
        isCheckOutForPeopleSelected = false
        UIView.animateWithDuration(0.3, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = -myMenuView.frame.size.width
                
        })
        
        
    }
    func Home(sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let ISHomeScreenz = storyboard.instantiateViewControllerWithIdentifier("ISHomeScreen") as! ISHomeScreen
        
        let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.pushViewController(ISHomeScreenz, animated: true)
    }
    func goToPeople(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        isCheckOutForPeopleSelected = false
        
        let ISPeopleListingz = storyboard.instantiateViewControllerWithIdentifier("ISPeopleListing") as! ISPeopleListing
        
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        navigationController.pushViewController(ISPeopleListingz, animated: true)
    }
    
    func goToAdvancedSearchVC(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let InTransitListingz = storyboard.instantiateViewControllerWithIdentifier("InTransitListing") as! InTransitListing
        
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        navigationController.pushViewController(InTransitListingz, animated: true)
    }
    func goToBack(sender: UIButton!)
    {
        myMenuView.hidden = true
        
         btnCheck = "Back"
        
//        isCheckInForLocationSelected = true
//        isCheckOutForPeopleSelected = true
        
       let navigationController = self.window?.rootViewController as! UINavigationController
        navigationController.popViewControllerAnimated(true)
    }
    
    func goToPropertiesForSaleVC(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let RecentItemListingz = storyboard.instantiateViewControllerWithIdentifier("RecentItemListing") as! RecentItemListing
        
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        navigationController.pushViewController(RecentItemListingz, animated: true)
    }
    
    
    func goToPropertiesOnRentVC(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
        isCheckInForLocationSelected = false
        
        let ISLocationListingz = storyboard.instantiateViewControllerWithIdentifier("ISLocationListing") as! ISLocationListing
        
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        navigationController.pushViewController(ISLocationListingz, animated: true)
    }
    
    func goToExchangePropertyVC(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let ISItemListingz = storyboard.instantiateViewControllerWithIdentifier("ISItemListing") as! ISItemListing
        
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        navigationController.pushViewController(ISItemListingz, animated: true)
    }
    
    func goToAboutUsVC(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let ISAppSettingz = storyboard.instantiateViewControllerWithIdentifier("ISAppSetting") as! ISAppSetting
        
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        navigationController.pushViewController(ISAppSettingz, animated: true)
    }
    
    func goToProfileVC(sender: UIButton!)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let ISProfileViewz = storyboard.instantiateViewControllerWithIdentifier("ISProfileView") as! ISProfileView
        
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        navigationController.pushViewController(ISProfileViewz, animated: true)
    }

    /////////++++++++++++++++++++/////////////////
    
    
    func applicationWillResignActive(application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

