//
//  ISSelectLocationCategory.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/15/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

var selectedLocFromLocListing: String = NSString() as String

class ISSelectLocationCategory: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    var selectedArray = NSMutableArray()
    
    var selectedValuePush = String()
    
    @IBOutlet weak var selectedLocationTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       // fillArrayForSelectedLocationListing()
        fetchSelectedLocationData()
        
        selectedLocationTableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backFromSelectedCategory(sender: AnyObject)
    {
       self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func fillArrayForSelectedLocationListing()
    {
        selectedArray.addObject(["Location":"Location1"])
        selectedArray.addObject(["Location":"Location2"])
        selectedArray.addObject(["Location":"Location3"])
        selectedArray.addObject(["Location":"Location4"])
        selectedArray.addObject(["Location":"Location5"])
        selectedArray.addObject(["Location":"Location6"])
        selectedArray.addObject(["Location":"Location7"])
        // print("selectedArray is\(selectedArray)")
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return selectedArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        cell.selectedItem.text = selectedArray[indexPath.row].valueForKey("LOCNAME") as? String
        cell.selectedImgView.image = UIImage(named:"goldenImage.png")!
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        selectedLocFromLocListing = (selectedArray[indexPath.row].valueForKey("LOCNAME") as? String)!
       self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func fetchSelectedLocationData()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        //        var itemDesc = ""
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Location", selectColumns: ["*"], whereString: "", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("LocationName"), forKey: "LOCNAME")
                fetchFields.setObject(resultSet.stringForColumn("LocationDescription"), forKey: "LOCDESCRIPTION")
                fetchFields.setObject(resultSet.stringForColumn("LocationAddress"), forKey: "LOCADDRESS")
                fetchFields.setObject(resultSet.stringForColumn("LocationNumber"), forKey: "LOCNUMBER")
                
                //                itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
                selectedArray.addObject(fetchFields)
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }


}
