
import UIKit

class ISThreeTab: UIViewController
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        x = -myMenuView.frame.size.width
    }

    
    @IBAction func menuBtnThreeTab(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
        })
    }
    

    @IBAction func QRCodeGenerator(sender: AnyObject)
    {
        NSUserDefaults.standardUserDefaults().setObject("Enter Text To Generate QR code", forKey: "generation")
        NSUserDefaults.standardUserDefaults().synchronize()
        let ISBarcodegenerator = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeGenerator") as! ISBarcodeGenerator
        self.navigationController?.pushViewController(ISBarcodegenerator, animated: true)
    }
    
    
    @IBAction func barcodegenerator(sender: AnyObject)
    {
        let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
        self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
    }
    
    @IBAction func peopleBtnThreeTab(sender: AnyObject)
    {
        let ISAddPeoplez = self.storyboard!.instantiateViewControllerWithIdentifier("ISAddPeople") as! ISAddPeople
        self.navigationController?.pushViewController(ISAddPeoplez, animated: true)
    }
    
    
    @IBAction func locationBtnThreeTab(sender: AnyObject)
    {
        let ISLocationTabz = self.storyboard!.instantiateViewControllerWithIdentifier("ISLocationTab") as! ISLocationTab
        self.navigationController?.pushViewController(ISLocationTabz, animated: true)
    }

    @IBAction func itemsBtnThreeTab(sender: AnyObject)
    {
        let ISItemTabz = self.storyboard!.instantiateViewControllerWithIdentifier("ISItemTab") as! ISItemTab
         ISItemTabz.selectedCategoryValue = "ItemBtnPush"
        self.navigationController?.pushViewController(ISItemTabz, animated: true)
    }
}
