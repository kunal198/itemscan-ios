//
//  ISEditItem.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

var isBackFromEdit = false
var isbackfromqR = false
class ISEditItem: UIViewController,UIImagePickerControllerDelegate,UIAlertViewDelegate,UITextViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITextFieldDelegate,AVCaptureMetadataOutputObjectsDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    private weak var saveAction : UIAlertAction?
    private weak var textFieldEmail : UITextField?
    var highlightViewForScanning : UIView = UIView()
    let session : AVCaptureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer!
    
    /////////////////+++++++++++++++++++////////////////////
    
    var barcodeOfEditItem:NSString = NSString()
    var currentLocOfEditItem:NSString = NSString()
    var nameOfEditItem:NSString = NSString()
    var decriptionOfEditItem:NSString = NSString()
    var colorOfEditItem:NSString = NSString()
    var numberOfEditItem:NSString = NSString()
    
    //+++++++++++++++++++++++++++++++++++//
    
    @IBOutlet weak var addCategoryEditItem: UILabel!
    
   
    @IBOutlet weak var nameEditItem: UILabel!
    
    @IBOutlet weak var barcodeEditItem: UILabel!
    @IBOutlet weak var currentLocEditItem: UILabel!
   
    @IBOutlet weak var colorEditItem: UITextField!
    @IBOutlet weak var editImageView: UIImageView!
   
    @IBOutlet weak var descriptionEditItem: UILabel!
    
    var color:String = NSString() as String
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "qr/Barcode")
        NSUserDefaults.standardUserDefaults().synchronize()
        let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        let imagePath = "\(filePath)/\(numberOfEditItem).jpeg"
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
            editImageView.image = UIImage(data: fileManager.contentsAtPath(imagePath)!)
        }
        else
        {
            editImageView.image = UIImage(named: "img.png")
        }
//        placeHolderMethodForColor()
       
    }
    
    func alertForSelectingColor()
    {
      //  saveAction?.enabled = false
        
        let alert = UIAlertController(title: "Add Color",
                                      message: "Enter the color of the Item.",
                                      preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel",style: .Default)
        {
            (action: UIAlertAction) -> Void in
        }
        
        alert.addTextFieldWithConfigurationHandler
            {
            (textFieldEmail: UITextField!) in
            textFieldEmail.placeholder = "Enter a valid email address"
            textFieldEmail.keyboardType = .EmailAddress
            textFieldEmail.delegate = self
            self.textFieldEmail = textFieldEmail
        }
        
        let saveAction = UIAlertAction(title: "Save",style: .Default)
        {
            (action: UIAlertAction) -> Void in
                                        
            let emailTextField = (alert.textFields![0] ).text
                self.color = emailTextField!
            // print("self.color is \(self.color)")
            self.colorEditItem.text = self.color
        }
        
        saveAction.enabled = false
        self.saveAction = saveAction
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        presentViewController(alert,animated: true,completion: nil)
        
    }
    
    
    @IBAction func addqrcode(sender: AnyObject)
    {
        var createAccountErrorAlert: UIAlertView = UIAlertView()
        createAccountErrorAlert.delegate = self
        createAccountErrorAlert.title = "Alert"
        createAccountErrorAlert.message = "Please Choose one"
        createAccountErrorAlert.addButtonWithTitle("Generate QR code")
        createAccountErrorAlert.addButtonWithTitle(" scan Bar Code")
        createAccountErrorAlert.addButtonWithTitle("cancel")
        createAccountErrorAlert.show()
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        
        
        if globalDescription != ""
        {
            descriptionEditItem.text = globalDescription
        }
        
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)

        x = -myMenuView.frame.size.width
        fetchEditItemDataBase()

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func placeHolderMethodForColor()
    {
        colorEditItem.attributedPlaceholder = NSAttributedString(string:"", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 14)!])
    }
    
 
    @IBAction func selectImage(sender: AnyObject)
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex)
        {
        case 0:
             print("Cancel")
        case 1:
            Camera()
        case 2:
            Gallery()
        default:
             print("Default")
            
        }
    }
    
    func Camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            // print("Camera capture")
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
        self.presentViewController(imag, animated: true, completion: nil)
            
            
        }
    }
    
    func Gallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        // print("ISShowBarcodeDetail.swift delegate method")
        
        self.dismissViewControllerAnimated(false, completion: nil)
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
        let fileManager = NSFileManager.defaultManager()
        
        let filePathToWrite = "\(documentDirectory)/\(numberOfEditItem).jpeg"
        
        let data: NSData = UIImageJPEGRepresentation(image,0.0)!
        
        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
        
        editImageView.image = image
      
    }
        
    @IBAction func backFromEditItem(sender: AnyObject)
    {
        myMenuView.hidden = false
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
            
        else
        {
            //            x = -myMenuView.frame.size.width
            //            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    
    

    @IBAction func addCategoryEditItem(sender: AnyObject)
    {
        let ISCategoryListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISCategoryListing") as! ISCategoryListing
        
        self.presentViewController(ISCategoryListingz, animated: true, completion: nil)
    }
   
   
    @IBAction func currentLocationBtn(sender: AnyObject)
    {
        let ISSelectLocationCategoryz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectLocationCategory") as! ISSelectLocationCategory
        
         self.presentViewController(ISSelectLocationCategoryz, animated: true, completion: nil)
        
    }
    @IBAction func doneFromEditItem(sender: AnyObject)
    {
         updateItems()
         self.navigationController?.popViewControllerAnimated(true)
    }
    
    func updateItems()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["ItemName"] = nameEditItem.text
        tblFields["ItemDescription"] = descriptionEditItem.text
        tblFields["ItemLocation"] = currentLocEditItem.text
        tblFields["ItemColor"] = colorEditItem.text
        tblFields["ItemCategory"] = addCategoryEditItem.text
        tblFields["ItemBarcode"] = barcodeEditItem.text
        
        print("number of edit item is = \(numberOfEditItem)")
        
        _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numberOfEditItem)'", whereFields: [])
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
        let fileManager = NSFileManager.defaultManager()
        
        let filePathToWrite = "\(documentDirectory)/\(numberOfEditItem).jpeg"
        
        let data: NSData = UIImageJPEGRepresentation(editImageView.image!,0.0)!
        
        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
        
        // print("filePathToWrite is \(filePathToWrite)")

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {   //delegate method
        
        colorEditItem.resignFirstResponder()
        
        return true
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        descriptionEditItem.resignFirstResponder()
        colorEditItem.resignFirstResponder()
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        
        let newText = NSString(string: textField.text!).stringByReplacingCharactersInRange(range, withString: string)
        
        if textField == ""
        {
           self.saveAction?.enabled = false
        }
        else
        {
            self.saveAction?.enabled = true
        }
        
        
        return true
    }
    
    @IBAction func DescBtn(sender: AnyObject)
    {
        let ISDescriptionz = self.storyboard!.instantiateViewControllerWithIdentifier("ISDescription") as! ISDescription
        ISDescriptionz.isFromEditItem = "true"
        self.navigationController?.pushViewController(ISDescriptionz, animated: true)
      //  descriptionEditItem.text = ""
    }
    
    @IBAction func colorBtn(sender: AnyObject)
    {
        saveAction?.enabled = false
        alertForSelectingColor()
        
    }
    func fetchEditItemDataBase()
    {
        
        var arr:NSMutableArray = NSMutableArray()
        
        //        var itemDesc = ""
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemNumber = '\(numberOfEditItem)'", whereFields: [])
        
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ITEMASSIGNTO")
                // itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
                barcodeEditItem.text = resultSet.stringForColumn("ItemBarcode")
                descriptionEditItem.text = resultSet.stringForColumn("ItemDescription")
                colorEditItem.text = resultSet.stringForColumn("ItemColor")
                nameEditItem.text = resultSet.stringForColumn("ItemName")
                let categoryString = resultSet.stringForColumn("ItemCategory")
                
                print("resultSet.stringForColumn is \(resultSet.stringForColumn("ItemColor"))")
                
                
                if(categoryString == "")
                {
                     addCategoryEditItem.text = "Add Category"
                }
                else
                {
                    addCategoryEditItem.text = categoryString
                }
                
                let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
                let imagePath = "\(filePath)/\(numberOfEditItem).jpeg"
                
                // print("imagePath in \(imagePath)")
                
                let fileManager = NSFileManager.defaultManager()
                
                if (fileManager.fileExistsAtPath(imagePath))
                {
                    editImageView.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
                }
                else
                {
                    editImageView.image =  UIImage(named: "img.png")
                }
                
                if isBackFromEdit == true
                {
                    descriptionEditItem.text = globalDescription
                    globalDescription = ""
                    isBackFromEdit = false
                }
                if  isbackfromqR == true
                {
                    isbackfromqR = false
                    let  str = NSUserDefaults.standardUserDefaults().objectForKey("qr/Barcode") as? String
                    if str == nil
                    {
                        
                    }
                    else
                    {
                        if   str == "Bar code Added successfully"
                            
                        {
                            let str1 = NSUserDefaults.standardUserDefaults().objectForKey("barcode")
                            
                            barcodeEditItem.text = "Bar code:" + " " + (str1 as! String)
                            
                        }
                        else
                        {
                            barcodeEditItem.text = NSUserDefaults.standardUserDefaults().objectForKey("qr/Barcode") as? String
                        }
                    }
                }
                
                let locationStr = resultSet.stringForColumn("ItemLocation")
                if(selectedLocFromLocListing == "")
                {
                    currentLocEditItem.text = locationStr
                }
                else
                {
                    currentLocEditItem.text = selectedLocFromLocListing
                }
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }
    
    //MARK : Alertview Delegate
    
    func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        
        switch buttonIndex
        {
        case 0:
//            let ISBarcodegenerator = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeGenerator") as! ISBarcodeGenerator
//            
//            self.navigationController?.pushViewController(ISBarcodegenerator, animated: true)
            
            
//            let ISBarcodescanner = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
//            self.navigationController?.pushViewController(ISBarcodescanner, animated: true)
            
            
            
            let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
            ISBarcodescannerz.isSearch = true
            self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
            
        case 1:
            
//            let ISBarcodescanner = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
//            self.navigationController?.pushViewController(ISBarcodescanner, animated: true)
            
            
            let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
            ISBarcodescannerz.isSearch = true
            self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
            
        case 2:
            
            print ("retry")
            
            
        default:
            print ("retry")
        }
    }
}
