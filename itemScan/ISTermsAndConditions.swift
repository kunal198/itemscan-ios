//
//  ISTermsAndConditions.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/13/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISTermsAndConditions: UIViewController
{

    @IBOutlet weak var termsAndConditionsTextView: UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        termsAndConditionsTextView.editable = false;
        termsAndConditionsTextView.selectable = false;
    }

    @IBAction func backFromTermsAndConditions(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
