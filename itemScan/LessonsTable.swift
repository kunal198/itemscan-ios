//
//  LessonsTable.swift
//  DigitalLearningTree
//
//  Created by mrinal khullar on 1/18/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class LessonsTable: NSObject
{
    var Class_ID: String = String()
    var Lesson_ID: String = String()
    var Position: Int = Int()

    var itemNumber:String = String()
    var itemBarcode:String = String()
    var itemLocation:String = String()
    var itemAssignTo:String = String()
    var itemDescription:String = String()
    var itemStatus:String = String()
    var itemColor:String = String()
}
