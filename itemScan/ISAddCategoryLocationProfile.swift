//
//  ISAddCategoryLocationProfile.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/11/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISAddCategoryLocationProfile: UIViewController {

    @IBOutlet weak var location_address: UITextField!
    @IBOutlet weak var loc_add_category: UITextField!
    @IBOutlet weak var location_description: UITextField!
    @IBOutlet weak var location_name: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placeHolderMethodForLocationEditing()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func selectcategoryBtn(sender: AnyObject)
    {
        let ISCategoryListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISCategoryListing") as! ISCategoryListing
        
        self.presentViewController(ISCategoryListingz, animated: true, completion: nil)
        
    }
    @IBAction func loc_nextBtnOfAddCategory(sender: AnyObject)
    {
          self.navigationController!.popViewControllerAnimated(true)
    }
    @IBAction func loc_backBtnOfAddCategory(sender: AnyObject)
    {
          self.navigationController!.popViewControllerAnimated(true)
    }
    
    func placeHolderMethodForLocationEditing()
    {
        location_name.attributedPlaceholder = NSAttributedString(string:"Location Name", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        location_address.attributedPlaceholder = NSAttributedString(string:"Address", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        location_description.attributedPlaceholder = NSAttributedString(string:"Description", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        loc_add_category.attributedPlaceholder = NSAttributedString(string:"Select Categories", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
    }
}
