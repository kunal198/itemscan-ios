//
//  ISDescription.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/15/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

var globalDescription: String = NSString() as String
var globalDescriptionEditLoc: String = NSString() as String
var DescriptionForAddItem:String = NSString() as String


class ISDescription: UIViewController,UITextViewDelegate
{
//
    
    @IBOutlet weak var descTextView: UITextView!
    
    var isFromNewItem = ""
    var isFromLocationItem = ""
    var isFromEditItem = ""
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DescriptionForAddItem = ""
        descTextView.text = ""
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        if globalDescription == ""
        {
           descTextView.text = "Write here something"
        }
        else
        {
            descTextView.text = globalDescription
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backFromDesc(sender: AnyObject)
    {
        if isFromNewItem == "true"
        {
           newItemDesc = descTextView.text
        }
        else
        {
            globalDescription = descTextView.text
            DescriptionForAddItem = descTextView.text
            isBackFromEdit = true
        }
        if isFromLocationItem == "true"
        {
            newLocationDesc = descTextView.text
        }
        else
        {
            globalDescriptionEditLoc = descTextView.text
            DescriptionForAddItem = descTextView.text
            isBackFromEditLocation = true
        }
        if isFromEditItem == "true"
        {
            globalDescription = descTextView.text
        }
        
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        descTextView.resignFirstResponder()
    }

}
