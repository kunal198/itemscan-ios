
//
//  ISAssignPeople.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/21/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

var peopleSeclectedForScannedBarcode:NSString = NSString()

class ISAssignPeople: UIViewController,UITableViewDataSource,UITableViewDelegate
{

    @IBOutlet weak var assignPeopletableView: UITableView!
    
    var assignPeopleListingArray:NSMutableArray = NSMutableArray()
    var assignName:NSString = NSString()
    var numForAssignName:NSString = NSString()
    var numForScannedAssignName:NSString = NSString()
    var numForQRAssignName:NSString = NSString()
    var selectedPeople:NSString = NSString()
    var isOutOrIn = Bool()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        assignPeopletableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
//       let user = NSUserDefaults.standardUserDefaults().objectForKey("USER") as! String
        assignPeopleListingArray = [" "]
        fetchPeopleListingDataBase()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backArrow(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // print("assignPeopleListingArray is \(assignPeopleListingArray.count)")
        return assignPeopleListingArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        // print(selectedPeople)
        if(indexPath.row == 0)
        {
            cell.assignPeople.text = "Nobody(Me)"
        }
//        else if (indexPath.row == 1)
//        {
//            cell.assignPeople.text = "ME"
//        }
        else
        {
            cell.assignPeople.text = assignPeopleListingArray[indexPath.row].valueForKey("ASSIGNPEOPLELIST") as? String
        }

        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
            if(indexPath.row == 0)
            {
                isOutOrIn = false
                selectedPeople = " "
            }
            else
            {
                isOutOrIn = true
                selectedPeople = (assignPeopleListingArray[indexPath.row].valueForKey("ASSIGNPEOPLELIST") as? String)!
                
                print("selected people = \(selectedPeople)")
                
                peopleSeclectedForScannedBarcode = (assignPeopleListingArray[indexPath.row].valueForKey("ASSIGNPEOPLELIST") as? String)!
            }

        
        
        updateItemListing()
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func fetchPeopleListingDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("PeopleListing", selectColumns: ["*"], whereString: "", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("PeopleListName"), forKey: "ASSIGNPEOPLELIST")
                arr.addObject(fetchFields)
                assignPeopleListingArray.addObject(fetchFields)
                // print("assignPeopleListingArray is \(assignPeopleListingArray)")
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }
    
    func insertToInTransitDataBase()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["InTransitName"] = selectedPeople as String
        
        _ = ModelManager.instance.addTableData("InTransit", primaryKey:"InTransitNumber", tblFields: tblFields)
        // print("tblFields is \(tblFields)")
    }
    
    func updateItemListing()
    {
        /////// right one
        if(isCheckInForLocationSelected == true)
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["ItemName"] = assignName as String
            tblFields["ItemAssignTo"] = selectedPeople as String
            
            _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numForScannedAssignName)'", whereFields: [])
        }
        else if(isCheckInQRForLcoation == true || isCheckOutQRForPeople == true)
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["ItemName"] = assignName as String
            tblFields["ItemAssignTo"] = selectedPeople as String
            
            _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numForQRAssignName)'", whereFields: [])
        }
        else
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["ItemName"] = assignName as String
            tblFields["ItemAssignTo"] = selectedPeople as String
            
            _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numForAssignName)'", whereFields: [])
        }

        
        // print("tblFields under update is \(tblFields)")
        
        insertRecentBase()
    }
    
    func updateNobodyListing()
    {
        if(isCheckInForLocationSelected == true)
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["ItemName"] = assignName as String
            tblFields["ItemAssignTo"] = " "
            
            _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numForScannedAssignName)'", whereFields: [])
        }
        else if(isCheckInQRForLcoation == true || isCheckOutQRForPeople == true)
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["ItemName"] = assignName as String
            tblFields["ItemAssignTo"] = " "
            
            _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numForQRAssignName)'", whereFields: [])
        }
        else
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["ItemName"] = assignName as String
            tblFields["ItemAssignTo"] = " "
            
            _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numForAssignName)'", whereFields: [])
        }

        
        insertRecentBase()
    }
    
    func insertRecentBase()
    {
        var tblFields: Dictionary! = [String: String]()
 
        if isOutOrIn == true
        {
             tblFields["RecentStatus"] = "Checked Out"
             tblFields["RecentName"] = assignName as String
        }
        else
        {
             tblFields["RecentStatus"] = "Checked In"
             tblFields["RecentName"] = assignName as String
        }
        
       
        
        _ = ModelManager.instance.addTableData("Recent", primaryKey:"RecentNumber", tblFields: tblFields)

    }

    @IBAction func backAssignArrow(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
