//
//  ISLogin.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/8/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit


class ISLogin: UIViewController,UITextFieldDelegate
{

    @IBOutlet weak var registerOutlet: UIButton!
    @IBOutlet weak var loginOutlet: UIButton!
    
    @IBOutlet weak var loginPassowrdTextField: UITextField!
    @IBOutlet weak var loginUsernameTextField: UITextField!
    @IBOutlet weak var passwordHighlightView: UIImageView!
    @IBOutlet weak var loginUserNameHighlightView: UIImageView!
    
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.hidden = true
        placeHolderMethodForISLogin()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        func fetchDataBase()
        {
            var arr:NSMutableArray = NSMutableArray()
            
            arr = []
            var itemDesc = ""
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemBarcode = '123456789'", whereFields: [])
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    itemDesc = resultSet.stringForColumn("ItemDescription")
                    arr.addObject(itemDesc)
                }
            }
            resultSet.close()
        }
    }
    
    func placeHolderMethodForISLogin()
    {
        loginUsernameTextField.attributedPlaceholder = NSAttributedString(string:"Email ID", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        loginPassowrdTextField.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        loginOutlet.backgroundColor = UIColor.clearColor()
        loginOutlet.layer.cornerRadius = 6
        loginOutlet.layer.borderWidth = 2
        loginOutlet.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
        
        registerOutlet.backgroundColor = UIColor.clearColor()
        registerOutlet.layer.cornerRadius = 6
        registerOutlet.layer.borderWidth = 2
        registerOutlet.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        loginUsernameTextField.resignFirstResponder()
        loginPassowrdTextField.resignFirstResponder()
    
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        loginUsernameTextField.resignFirstResponder()
        loginPassowrdTextField.resignFirstResponder()
    }
    
    @IBAction func loginBtn(sender: AnyObject)
    {
        let ISRegistrationz = self.storyboard!.instantiateViewControllerWithIdentifier("ISRegistration") as! ISRegistration
        
        self.navigationController?.pushViewController(ISRegistrationz, animated: true)
        
    }
    @IBAction func registerBtn(sender: AnyObject)
    {
        if loginUsernameTextField.text == "" && loginPassowrdTextField.text == ""
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "All textfields are mandatory."
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
        else
        {
            //usernameTextFieldRegistration.text
            let username = NSUserDefaults.standardUserDefaults().objectForKey(loginUsernameTextField.text!)
           
            
            if username == nil
            {
                let alert1 = UIAlertView()
                alert1.title = "Alert"
                alert1.message = "Invalid Email ID/Password."
                alert1.addButtonWithTitle("Ok")
                alert1.show()
            }
            else
            {
                let emailTransfer = username!.valueForKey("email") as! String
                let passwordTransfer = username!.valueForKey("password") as! String
                let usernameTransfer = username!.valueForKey("username") as! String
                
                
                print("username = \(usernameTransfer)")
                
                
                var isLogin = false
                
                if emailTransfer == loginUsernameTextField.text
                {
                    isLogin = true
                }
//                else if usernameTransfer == loginUsernameTextField.text
//                {
//                    isLogin = true
//                }
                else
                {
                    isLogin = false
                }
                
                
                if isLogin == true && passwordTransfer == loginPassowrdTextField.text
                {
                
                    NSUserDefaults.standardUserDefaults().setObject(Bool(true), forKey:"Login")
                    let ISHomeScreenz = self.storyboard!.instantiateViewControllerWithIdentifier("ISHomeScreen") as! ISHomeScreen
                    self.navigationController?.pushViewController(ISHomeScreenz, animated: true)
                }
                else
                {
                    let alert1 = UIAlertView()
                    alert1.title = "Alert"
                    alert1.message = "Invalid Email ID/Password."
                    alert1.addButtonWithTitle("Ok")
                    alert1.show()
                }
                
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(usernameTransfer, forKey: "USER")
                defaults.setObject(emailTransfer, forKey: "EMAILID")
            }
        }
        }
}
