//
//  ISItemTab.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/18/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

var newItemDesc = ""
class ISItemTab: UIViewController,UIActionSheetDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate
{
    
    var mainPushStr = String()
    var selectedValuePush = String()
    
    var selectedCategoryValue = String()
    
    var highlightViewForScanning : UIView = UIView()
    let session : AVCaptureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer!
    
    var CategoryNameForInsertion:String = NSString() as String
    var addItemNumberForImage:String = NSString() as String
    var scanQRCodeStr:String = NSString() as String
    var saveImgFromTwoTab:UIImage = UIImage() as UIImage
    
    var itemFetchArray:NSMutableArray = NSMutableArray()
    
    var ItemNameForInsertion:NSString = NSString()
    var itemString:NSString = NSString()
    
    /////////////////+++++++++++++++++++////////////////////
    
    @IBOutlet var AddCategoryBtn: UIButton!
    @IBOutlet var AddLocationBtn: UIButton!
    @IBOutlet weak var currentLocName: UITextField!
    @IBOutlet weak var descName: UITextField!

    @IBOutlet weak var colorName: UITextField!
    @IBOutlet weak var imageName: UIImageView!
    @IBOutlet weak var addCategoryName: UITextField!
    @IBOutlet weak var barcodeName: UITextField!
    @IBOutlet weak var itemNAme: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "qr/Barcode")
        NSUserDefaults.standardUserDefaults().synchronize()
        placeHolderMethodForAddItem()
        

        descName.text = ""
        
        if(scanQRCodeStr == "")
        {
            barcodeName.text = ""
        }
        else
        {
            barcodeName.text = scanQRCodeStr
        }
        
//        let imageData = UIImagePNGRepresentation(saveImgFromTwoTab)
        let imageData = UIImageJPEGRepresentation(saveImgFromTwoTab,0.0)
        
        if imageData != nil
        {
            imageName.image = saveImgFromTwoTab
        }
        else
        {
            imageName.image = UIImage(named:"img_icon.png")
        }

        // Do any additional setup after loading the view.
    }
    @IBOutlet var addbarqrcode: UITextField!

    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(animated: Bool)
    {
        
        
//        if selectedCategoryValue == "ItemBtnPush"
//        {
//            currentLocName.text = ""
//            addCategoryName.text = ""
//        }
//        else
//        {
            currentLocName.text = selectedLocFromLocListing
            addCategoryName.text = addCategoryStr as String
 //       }
        
      
        
        barcodeName.text = NSUserDefaults.standardUserDefaults().objectForKey("qr/Barcode") as? String
        
        if   barcodeName.text == "Bar code Added successfully"
        {
            let str1 = NSUserDefaults.standardUserDefaults().objectForKey("barcode")
            
//            barcodeName.text = "Bar code:" + " " + (str1 as! String)
            
            barcodeName.text = str1 as? String
        }
        else
        {
            barcodeName.text = NSUserDefaults.standardUserDefaults().objectForKey("qr/Barcode") as? String
        }
//        addCategoryName.text = selectedCategoryFromCategoryListing
        
        CategoryNameForInsertion = addCategoryName.text!
        
        if newItemDesc == ""
        {
            descName.text = ""
        }
        else
        {
            descName.text = newItemDesc
            newItemDesc = ""
        }
        
//       let imagezz: UIImage! = saveImgFromTwoTab
//        // print("imagezz is \(imagezz)")
        
}
    
    
    
    
    
    
    
    
    func placeHolderMethodForAddItem()
    {
        itemNAme.attributedPlaceholder = NSAttributedString(string:"Item Name", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        barcodeName.attributedPlaceholder = NSAttributedString(string:"QR Code / Bar code", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        currentLocName.attributedPlaceholder = NSAttributedString(string:"Current Location", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        descName.attributedPlaceholder = NSAttributedString(string:"Description", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        addCategoryName.attributedPlaceholder = NSAttributedString(string:"Add Category", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
         colorName.attributedPlaceholder = NSAttributedString(string:"Add Color", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex)
        {
        case 0:
             print("Cancel")
        case 1:
            Camera()
        case 2:
            Gallery()
        default:
             print("Default")
        }
    }
    
    func Camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            // print("Camera capture")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
        }
    }
    
    func Gallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        // print("ISShowBarcodeDetail.swift delegate method")
        self.dismissViewControllerAnimated(false, completion: nil)
        
        imageName.image = image
    }

    @IBAction func doneBtnName(sender: AnyObject)
    {
        if(itemNAme.text == "" )
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Enter Item Name"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }

        else if currentLocName.text == ""
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Enter Current Location"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
        else if(barcodeName.text == "" )
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Enter Barcode"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
//        else if(addCategoryName.text == "")
//        {
//            let alert1 = UIAlertView()
//            alert1.title = "Alert"
//            alert1.message = "All text fields are mandatory"
//            alert1.addButtonWithTitle("Ok")
//            alert1.show()
//        }
//        else if(colorName.text == "")
//        {
//            let alert1 = UIAlertView()
//            alert1.title = "Alert"
//            alert1.message = "All text fields are mandatory"
//            alert1.addButtonWithTitle("Ok")
//            alert1.show()
//        }
//        else if(descName.text == "")
//        {
//            let alert1 = UIAlertView()
//            alert1.title = "Alert"
//            alert1.message = "All text fields are mandatory"
//            alert1.addButtonWithTitle("Ok")
//            alert1.show()
//        }
        else
        {
           // insertItemTable()
            fetchItemNAmeDataBase()
            
            fetchCategoryDataBase()
            fetchImageForAddTabDataBase()
        }
        
    }
    
    
    
    @IBAction func AddLocationBtn(sender: AnyObject)
    {
        let ISSelectLocationCategoryz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectLocationCategory") as! ISSelectLocationCategory
        
        ISSelectLocationCategoryz.selectedValuePush = "LocationAdded"
        
        self.presentViewController(ISSelectLocationCategoryz, animated: true, completion: nil)
    }
    
    
    @IBAction func AddCategoryBtn(sender: AnyObject)
    {
        let ISCategoryListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISCategoryListing") as! ISCategoryListing
        
        ISCategoryListingz.selectedCategoryValue = "CategorySelection"
        
        self.presentViewController(ISCategoryListingz, animated: true, completion: nil)
    }
    
    
    @IBAction func menuBtnName(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func addCategoryBtn(sender: AnyObject)
    {
        let ISCategoryListingz = self.storyboard!.instantiateViewControllerWithIdentifier("ISCategoryListing") as! ISCategoryListing
        
        self.presentViewController(ISCategoryListingz, animated: true, completion: nil)
    }
    @IBAction func Descbtn(sender: AnyObject)
    {
        let ISDescriptionz = self.storyboard!.instantiateViewControllerWithIdentifier("ISDescription") as! ISDescription
        ISDescriptionz.isFromNewItem = "true"
        self.navigationController?.pushViewController(ISDescriptionz, animated: true)
        descName.text = ""
        
    }
    @IBAction func currentLocBtnAddItem(sender: AnyObject)
    {
        let ISSelectLocationCategoryz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectLocationCategory") as! ISSelectLocationCategory
        
        self.presentViewController(ISSelectLocationCategoryz, animated: true, completion: nil)
    }
    
    @IBAction func scanBarCodeAddItem(sender: AnyObject)
    {
        let createAccountErrorAlert: UIAlertView = UIAlertView()
        createAccountErrorAlert.delegate = self
        createAccountErrorAlert.title = "Alert"
        createAccountErrorAlert.message = "Please Choose one"
        createAccountErrorAlert.addButtonWithTitle("Generate QR code")
        createAccountErrorAlert.addButtonWithTitle(" Scan Bar Code")
         createAccountErrorAlert.addButtonWithTitle("Cancel")
        createAccountErrorAlert.show()
    }
    
    

    @IBAction func addImageBtn(sender: AnyObject)
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        itemNAme.resignFirstResponder()
        colorName.resignFirstResponder()
        currentLocName.resignFirstResponder()
        addCategoryName.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        itemNAme.resignFirstResponder()
        colorName.resignFirstResponder()
        currentLocName.resignFirstResponder()
        addCategoryName.resignFirstResponder()
    }
    
    func insertItemTable()
    {
        var tblFields: Dictionary! = [String: String]()
        tblFields["ItemBarcode"] = barcodeName.text!
        tblFields["ItemLocation"] = currentLocName.text!
        tblFields["ItemDescription"] = descName.text!
        tblFields["ItemColor"] = colorName.text!
        tblFields["ItemName"] = itemNAme.text!
        
        ItemNameForInsertion = itemNAme.text!
        
        tblFields["ItemCategory"] = addCategoryName.text!
        tblFields["ItemAssignTo"] = " "
        // print(addCategoryName.text!)
        CategoryNameForInsertion = addCategoryName.text!
        
        let itemNumberId = ModelManager.instance.addTableData("Item", primaryKey:"ItemNumber", tblFields: tblFields)
        
        print("itemNumberId is \(itemNumberId)")
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
        let fileManager = NSFileManager.defaultManager()
        
        let filePathToWrite = "\(documentDirectory)/\(itemNumberId).jpeg"
        
        let data: NSData = UIImageJPEGRepresentation(imageName.image!,0.0)!
        
        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
    }
    
    
    func fetchItemNAmeDataBase()
    {
        
        itemString = itemNAme.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let CategoryString = addCategoryName.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//        let QRStrString = itemNAme.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let ColorString = colorName.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let DescString = descName.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        let LocString = currentLocName.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        
        
//        var arr:NSMutableArray = NSMutableArray()
        // print("PeopleNameForInsertion is \(PeopleNameForInsertion)")
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemName = '\(itemNAme.text!)'", whereFields: [])
//        arr = []
        
        resultSet.next()
        
        let messageCount = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        if messageCount == 0
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["ItemBarcode"] = barcodeName.text!
            tblFields["ItemLocation"] = LocString
            tblFields["ItemDescription"] = DescString
            tblFields["ItemColor"] = ColorString
            tblFields["ItemCategory"] = CategoryString
            tblFields["ItemAssignTo"] = " "
            CategoryNameForInsertion = CategoryString
            
            tblFields["ItemName"] = itemString as String
            
            let itemNumberId = ModelManager.instance.addTableData("Item", primaryKey:"ItemNumber", tblFields: tblFields)
            
             print("tblFields is \(tblFields)")
            
            print("itemNumberId is \(itemNumberId)")
            
            let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
            
            let fileManager = NSFileManager.defaultManager()
            
            let filePathToWrite = "\(documentDirectory)/\(itemNumberId).jpeg"
            
            let data: NSData = UIImageJPEGRepresentation(imageName.image!,0.0)!
            
            fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
            
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Item with same name cannot be registered!!"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
    }

    func fetchImageForAddTabDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        //        var itemDesc = ""
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
        
                arr.addObject(fetchFields)
                addItemNumberForImage = resultSet.stringForColumn("ItemNumber")
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.view.frame.origin.y = 0.0
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        
        if self.view.frame.size.width == 375
        {
            if textField == addCategoryName
            {
                self.view.frame.origin.y = -125.0
            }
            else if textField == currentLocName
            {
               self.view.frame.origin.y = -80.0
            }
        }
        else
        {
            if textField == addCategoryName
            {
                 self.view.frame.origin.y = -100.0
            }
            else if textField == currentLocName
            {
                self.view.frame.origin.y = -50.0
            }
        }
    }
    func fetchCategoryDataBase()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("CategoryListing", selectColumns: ["COUNT(*) as count"], whereString: "CategoryName = '\(CategoryNameForInsertion)'", whereFields: [])

        resultSet.next()
        
        let messageCount = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        if messageCount == 0
        {
            var tblFields: Dictionary! = [String: String]()
            tblFields["CategoryName"] = CategoryNameForInsertion
            
            _ = ModelManager.instance.addTableData("CategoryListing", primaryKey:"CategoryNumber", tblFields: tblFields)
        }
    }
    
    //MARK: Alertview Delegate 
    
    func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
            
        case 0:
            let ISBarcodegenerator = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeGenerator") as! ISBarcodeGenerator
            
           let itemStringz = itemNAme.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            ISBarcodegenerator.QRItem = itemStringz
            
            print("ISBarcodegenerator.QRItem is \(itemStringz)")
            
            self.navigationController?.pushViewController(ISBarcodegenerator, animated: true)
            
//            let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
//            ISBarcodescannerz.isSearch = true
//            self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
            
            
          case 1:
            
            let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
            ISBarcodescannerz.isSearch = true
            self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
        case 2:
            
            print ("Retry")

          
        default:
            print ("Default Retry")
        }
    }
    
    //MARK: - Fetch Item Table From Database
    
    func fetchItemsDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        // print("PeopleNameForInsertion is \(PeopleNameForInsertion)")
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemName = '\(ItemNameForInsertion)'", whereFields: [])
        arr = []
        
        resultSet.next()
        
        let messageCount = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        if messageCount == 0
        {
            var tblFields: Dictionary! = [String: String]()
            tblFields["ItemName"] = ItemNameForInsertion as String
            
            _ = ModelManager.instance.addTableData("Item", primaryKey:"ItemNumber", tblFields: tblFields)
        }
        
        resultSet.close()
    }
}
