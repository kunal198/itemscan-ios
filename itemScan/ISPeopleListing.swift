//
//  ISPeopleListing.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/20/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISPeopleListing: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0

    var peopleName = String()
    var peopleString = String()
    
    @IBOutlet var PeopleNameLbl: UILabel!
    @IBOutlet weak var peopleTableView: UITableView!
    
    @IBOutlet var MeBtn: UIButton!
    var peopleListingArray:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        peopleTableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        
        let defaultsArray = NSUserDefaults.standardUserDefaults()
        // let defaultsArray = NSUserDefaults.standardUserDefaults()
        let savedValueArray = defaultsArray.objectForKey("SavedStringArray") as? [AnyObject] ?? [AnyObject]()
        
        print("saved array is = \(savedValueArray)")
        
        
        var savedName = String()
          savedName =  (savedValueArray[0].valueForKey("name") as? String)!
        
     //   self.peopleTableView.reloadData()
        
        if savedName != ""
        {
            self.PeopleNameLbl.text = "Me"
        }
        
        fetchPeopleListingDataBase()
        
        }
    
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backArrow(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }

    
    @IBAction func MeBtnProfile(sender: AnyObject)
    {
//        let ISProfileViewz = self.storyboard!.instantiateViewControllerWithIdentifier("ISProfileView") as! ISProfileView
//        self.navigationController?.pushViewController(ISProfileViewz, animated: true)
        
        
        let defaultsArray = NSUserDefaults.standardUserDefaults()
        // let defaultsArray = NSUserDefaults.standardUserDefaults()
        let savedValueArray = defaultsArray.objectForKey("SavedStringArray") as? [AnyObject] ?? [AnyObject]()
        
        print("saved array is = \(savedValueArray)")
        
        
        var savedName = String()
        savedName =  (savedValueArray[0].valueForKey("name") as? String)!


        
         peopleSeclectedForScannedBarcode = savedName
        
        let ISLocationProfileViewz = self.storyboard!.instantiateViewControllerWithIdentifier("PeopleProfileViewController") as! PeopleProfileViewController
//        ISLocationProfileViewz.peopleName = !
          ISLocationProfileViewz.differentiateLocationType = "PeopleValue"
          ISLocationProfileViewz.differentiateLocationType = "PeopleValue"
          ISLocationProfileViewz.differentiateMeType = "MeOrNoBody"
         ISLocationProfileViewz.peopleSeclectedForScannedBarcode = peopleSeclectedForScannedBarcode as String
        self.navigationController?.pushViewController(ISLocationProfileViewz, animated: true)

    }
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // print("peopleListingArray.count is \(peopleListingArray.count)")
        return peopleListingArray.count;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        let defaultsArray = NSUserDefaults.standardUserDefaults()
        // let defaultsArray = NSUserDefaults.standardUserDefaults()
        let savedValueArray = defaultsArray.objectForKey("SavedStringArray") as? [AnyObject] ?? [AnyObject]()
        
        print("saved array is = \(savedValueArray)")
        
//        if indexPath.row == 0
//        {
//            cell.peopleName.text = savedValueArray[0].valueForKey("name") as? String
//        }
//        else
//        {
            cell.peopleName.text = peopleListingArray[indexPath.row].valueForKey("PEOPLELIST") as? String
  //      }
        
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        peopleSeclectedForScannedBarcode = (peopleListingArray[indexPath.row].valueForKey("PEOPLELIST") as? String)!
//        
//            let createAccountErrorAlert: UIAlertView = UIAlertView()
//            createAccountErrorAlert.delegate = self
//            createAccountErrorAlert.title = "Alert"
//            createAccountErrorAlert.message = "Please Choose one"
//            createAccountErrorAlert.addButtonWithTitle("Scan QR code")
//            createAccountErrorAlert.addButtonWithTitle(" Scan Bar Code")
//            createAccountErrorAlert.addButtonWithTitle("Cancel")
//            createAccountErrorAlert.show()
        
//        if  btnCheck == "Back"
//        {
//            isCheckOutForPeopleSelected = true
//        }
//        else{
//            isCheckOutForPeopleSelected = false
//        }

        
        if(isCheckOutForPeopleSelected == true)
        {
            peopleSeclectedForScannedBarcode = (peopleListingArray[indexPath.row].valueForKey("PEOPLELIST") as? String)!
            
            
//            if(indexPath.row == 0)
//            {
//                isOutOrIn = false
//                selectedPeople = " "
//            }
//            else
//            {
                //isOutOrIn = true
            
               // selectedPeople = (assignPeopleListingArray[indexPath.row].valueForKey("ASSIGNPEOPLELIST") as? String)!
                
                print("selected people = \(peopleSeclectedForScannedBarcode)")
                
//                peopleSeclectedForScannedBarcode = (assignPeopleListingArray[indexPath.row].valueForKey("ASSIGNPEOPLELIST") as? String)!
//            }
            
            
            
           // updateItemListing()
            

            
            let createAccountErrorAlert: UIAlertView = UIAlertView()
            createAccountErrorAlert.delegate = self
            createAccountErrorAlert.title = "Alert"
            createAccountErrorAlert.message = "Please Choose one"
            createAccountErrorAlert.addButtonWithTitle("Scan QR code")
            createAccountErrorAlert.addButtonWithTitle(" Scan Bar Code")
            createAccountErrorAlert.addButtonWithTitle("Cancel")
            createAccountErrorAlert.show()

            
          //  isCheckOutForPeopleSelected = false
        }
        else
        {
           let ISLocationProfileViewz = self.storyboard!.instantiateViewControllerWithIdentifier("PeopleProfileViewController") as! PeopleProfileViewController
//            ISLocationProfileViewz.descriptionLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCDESCRIPTION") as? String)!
//            ISLocationProfileViewz.locLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCADDRESS") as? String)!
//            ISLocationProfileViewz.LocationName = (globalLocationListingArray[indexPath.row].valueForKey("LOCNAME") as? String)!
//            ISLocationProfileViewz.numberOfLocationDetail = (globalLocationListingArray[indexPath.row].valueForKey("LOCNUMBER") as? String)!
            
            ISLocationProfileViewz.peopleName = (peopleListingArray[indexPath.row].valueForKey("PEOPLELIST") as? String)!
            ISLocationProfileViewz.differentiateLocationType = "PeopleValue"
            ISLocationProfileViewz.peopleString = "PeopleValue"
            ISLocationProfileViewz.peopleSeclectedForScannedBarcode = peopleSeclectedForScannedBarcode as String
            
         self.navigationController?.pushViewController(ISLocationProfileViewz, animated: true)
            
            
          //isCheckOutForPeopleSelected = true
        }
        
    }
    
    
    
    
    
    func fetchPeopleListingDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("PeopleListing", selectColumns: ["*"], whereString: "", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("PeopleListName"), forKey: "PEOPLELIST")
                arr.addObject(fetchFields)
                
                
                peopleListingArray.addObject(fetchFields)
                // print("peopleListingArray is \(arr)")
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }
    
    //MARK: Alertview Delegate
    
    func alertView(View: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 0:
            
//            isCheckInForLocationSelected = true
//            isCheckOutForPeopleSelected = true

//            let ISSearchQRCodez = self.storyboard!.instantiateViewControllerWithIdentifier("ISSearchQRCode") as! ISSearchQRCode
//            self.navigationController?.pushViewController(ISSearchQRCodez, animated: true)
            
            let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
            
            ISBarcodescannerz.TypeCode = "QRCode"
            ISBarcodescannerz.peopleString = peopleSeclectedForScannedBarcode as String
            ISBarcodescannerz.differentiateLocationType = "PeopleValue"
            self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
            
        case 1:
            
            let ISBarcodescannerz = self.storyboard!.instantiateViewControllerWithIdentifier("ISBarcodeScanner") as! ISBarcodeScanner
            ISBarcodescannerz.peopleString = peopleSeclectedForScannedBarcode as String
            ISBarcodescannerz.TypeCode = "BarCode"
            ISBarcodescannerz.differentiateLocationType = "PeopleValue"
            self.navigationController?.pushViewController(ISBarcodescannerz, animated: true)
        case 2:
            
            print ("Retry")
            isCheckOutForPeopleSelected = true
            
        default:
            print ("Default Retry")
           // isCheckOutForPeopleSelected = true
        }
    }
}
