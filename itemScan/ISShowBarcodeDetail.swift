
//
//  ISShowBarcodeDetail.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation

class ISShowBarcodeDetail: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource, AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate
{

    var SaveImage = UIImage()
    var isArrowSelected = Bool()
    var bacrcodeListingArray = NSMutableArray()
    var highlightViewForScanning : UIView = UIView()
    let session : AVCaptureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer!
    
    //************************************************//
    
    @IBOutlet weak var arrowImage: UIImageView!
 
    @IBOutlet weak var showBarcodeTableView: UITableView!
    @IBOutlet weak var barcodeImageView: UIImageView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        fillArrayForShowListing()
        showBarcodeTableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool)
    {
        barcodeImageView.image = SaveImage
    }
    
    override func viewWillAppear(animated: Bool)
    {
         showBarcodeTableView.hidden = true
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func centerBtn(sender: AnyObject)
    {
        
        if isArrowSelected == false
        {
            arrowImage.image = UIImage(named: "UpArrow.png")
            isArrowSelected = true
            showBarcodeTableView.hidden = false
        }
        else
        {
            arrowImage.image = UIImage(named: "DownArrow.png")
            isArrowSelected = false
            showBarcodeTableView.hidden = true
            
        }
    }
    @IBAction func doneBtnUnderScanView(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func cameraBtn(sender: AnyObject)
    {
        self.highlightViewForScanning.autoresizingMask = [.FlexibleTopMargin,.FlexibleBottomMargin,.FlexibleLeftMargin,.FlexibleRightMargin]

        self.view.addSubview(self.highlightViewForScanning)
        do
        {
            let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
            let input = try AVCaptureDeviceInput(device: captureDevice)
             session.addInput(input)
            // Do the rest of your work...
        }
        catch let error as NSError
        {
            // Handle any errors
            // print(error)
        }

        let output = AVCaptureMetadataOutput()
        
        output.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        
        session.addOutput(output)
        
        output.metadataObjectTypes = output.availableMetadataObjectTypes
//        previewLayer = AVCaptureVideoPreviewLayer.layerWithSession(session) as AVCaptureVideoPreviewLayer
        previewLayer = AVCaptureVideoPreviewLayer(session: session) as AVCaptureVideoPreviewLayer
        previewLayer.frame = self.view.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.view.layer.addSublayer(previewLayer)
        
        // Start the scanner. You'll have to end it yourself later.
        session.startRunning()
        
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!)
    {
        var highlightViewRect = CGRectZero
        var barCodeObject : AVMetadataObject!
        var detectionString : String!
        
        let barCodeTypes = [AVMetadataObjectTypeUPCECode,
                            AVMetadataObjectTypeCode39Code,
                            AVMetadataObjectTypeCode39Mod43Code,
                            AVMetadataObjectTypeEAN13Code,
                            AVMetadataObjectTypeEAN8Code,
                            AVMetadataObjectTypeCode93Code,
                            AVMetadataObjectTypeCode128Code,
                            AVMetadataObjectTypePDF417Code,
                            AVMetadataObjectTypeQRCode,
                            AVMetadataObjectTypeAztecCode,
                            AVMetadataObjectTypePDF417Code,
                            AVMetadataObjectTypeDataMatrixCode,
                            AVMetadataObjectTypeInterleaved2of5Code,
                            AVMetadataObjectTypeITF14Code
        ]
        
        if metadataObjects == nil || metadataObjects.count == 0
        {
            // qrCodeFrameView?.frame = CGRectZero
            // print("No QR code is detected")
            return
        }
        
        
        // The scanner is capable of capturing multiple 2-dimensional barcodes in one scan.
        for metadata in metadataObjects
        {
            for barcodeType in barCodeTypes
            {
                if metadata.type == barcodeType
                {
                    
                    barCodeObject = self.previewLayer.transformedMetadataObjectForMetadataObject(metadata as! AVMetadataMachineReadableCodeObject)
                    
                    highlightViewRect = barCodeObject.bounds
                    
                    detectionString = (metadata as! AVMetadataMachineReadableCodeObject).stringValue
                    
                    self.session.stopRunning()
                    
                    break
                }
                else
                {
                    
                    if metadataObjects == nil || metadataObjects.count == 0
                    {
                        let alert = UIAlertView(title: "Alert", message: "Bar code type not matched", delegate: self, cancelButtonTitle: "OK")
                        
                        // print("error in detection")
                        
                        detectionString = "Bar code type not matched"
                        
                        alert.show()
                        
                        return
                    }
                    
                }
            }
        }
        
        previewLayer.hidden = true
        
        // print("detectionString is \(detectionString)")
        
        self.highlightViewForScanning.frame = highlightViewRect
        self.view.bringSubviewToFront(self.highlightViewForScanning)
    }

    
    func fillArrayForShowListing()
    {
        bacrcodeListingArray .addObject(["item":"sampleCode1","status":"checkedIn/Out"])
        bacrcodeListingArray .addObject(["item":"sampleCode2","status":"checkedIn/Out"])
        // print("bacrcodeListingArray is\(bacrcodeListingArray)")
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return bacrcodeListingArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        cell.showSample.text = bacrcodeListingArray[indexPath.row].valueForKey("item") as? String
        cell.showChecked.text = bacrcodeListingArray[indexPath.row].valueForKey("status") as? String
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex)
        {
            case 0:
                 print("Cancel")
            case 1:
                Camera()
            case 2:
                Gallery()
            default:
                 print("Default")

        }
    }
    
    func Camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            // print("Camera capture")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
            
            
        }
    }
    
    func Gallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){

            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        SaveImage = image

        self.dismissViewControllerAnimated(false, completion: nil)
 
    }
}
