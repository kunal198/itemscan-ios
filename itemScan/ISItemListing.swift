//
//  ISItemListing.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/8/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit



class ISItemListing: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate
{
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    var globalItemArray:NSMutableArray = NSMutableArray()
    var ImageArray:NSMutableArray = NSMutableArray()
    var itemNumberStr:String = NSString() as String
    @IBOutlet weak var itemListingTableView: UITableView!
    
    var itemListingArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        
        myMenuView.hidden = true
        
        fillArray()
        itemListingTableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        isCheckInForLocationSelected = false
        isCheckOutForPeopleSelected = false
        isCheckInQRForLcoation = false
        isCheckOutQRForPeople = false
        
        
        closeMenuBtn.frame = CGRectMake(0,0,self.view.frame.width,self.view.frame.height)
        closeMenuBtn.backgroundColor = UIColor.clearColor()
        closeMenuBtn.alpha = 0.5
        closeMenuBtn.hidden = true
        closeMenuBtn.addTarget(self, action:"closeMenuBtnClicked:",forControlEvents: UIControlEvents.TouchUpInside)
        self.view .addSubview(closeMenuBtn)
        closeMenuBtn.hidden = true
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        fetchDataBase()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backFromItemListing(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
            
        }
        else
        {
            
            navx = 0
            closeMenuBtn.hidden = false
         
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })
    }
    func fillArray()
    {
        itemListingArray .addObject(["Item":"Item1","Address":"Prospect Avenue, Norwich","status":"checked In"])
        itemListingArray .addObject(["Item":"Item2","Address":"Fall River, Florida","status":"Out"])
        itemListingArray .addObject(["Item":"Item3","Address":"Route 2, New Philadelphia","status":"checked In/Out"])
        itemListingArray .addObject(["Item":"Item4","Address":"Fall River, Florida","status":"checked In/Out"])
        
//        // print("itemListingArray is\(itemListingArray)")
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       
        return globalItemArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        cell.itemName.text = globalItemArray[indexPath.row].valueForKey("NAME") as? String
//        // print("globalItemArray is\(globalItemArray)")
        cell.itemAddress.text = globalItemArray[indexPath.row].valueForKey("LOCATION") as? String
        
        if indexPath.row == 0
        {
            cell.itemStatus.text = globalItemArray[indexPath.row].valueForKey("status") as? String
            cell.itemStatus.textColor = UIColor(red: 56/255, green: 146/255, blue:6/255, alpha: 1.0)
        }
        else if indexPath.row == 1
        {
            cell.itemStatus.text = globalItemArray[indexPath.row].valueForKey("status") as? String
            cell.itemStatus.textColor = UIColor(red: 231/255, green: 56/255, blue:8/255, alpha: 1.0)
        }
        else
        {
            cell.itemStatus.text = globalItemArray[indexPath.row].valueForKey("status") as? String
        }
        
        let filePath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        let imagename =  self.globalItemArray[indexPath.row].valueForKey("NUMBER") as! String
        let imagePath = "\(filePath)/\(imagename).jpeg"
        let fileManager = NSFileManager.defaultManager()
        
        
       //    print("imagePath in \(imagePath)")
        
        //++++++++++++++++++++++++++++++++++//
        dispatch_async(dispatch_get_main_queue())
        {
            if (fileManager.fileExistsAtPath(imagePath))
            {
                cell.itemImageView.image =  UIImage(data: fileManager.contentsAtPath(imagePath)!)
            }
            else
            {
                cell.itemImageView.image =  UIImage(named: "img.png")
            }
        }
        //+++++++++++++++++++++++++++++++++++//
        
        ImageArray.addObject(globalItemArray[indexPath.row].valueForKey("NUMBER")!)
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
    
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let ISSelectedItemz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectedItem") as! ISSelectedItem
        
        // print(globalItemArray[indexPath.row])
        
        ISSelectedItemz.nameOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("NAME") as? String)!
        ISSelectedItemz.barcodeOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("BARCODE") as? String)!
        ISSelectedItemz.currentLocOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("LOCATION") as? String)!
        ISSelectedItemz.decriptionOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("DESCRIPTION") as? String)!
        ISSelectedItemz.colorOfSelectedItem = (globalItemArray[indexPath.row].valueForKey("COLOR") as? String)!
        ISSelectedItemz.itemNumber = (globalItemArray[indexPath.row].valueForKey("NUMBER") as? String)!
        
        self.navigationController?.pushViewController(ISSelectedItemz, animated: true)
    }
    
    func fetchDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
      
        let ItemNumber = " 23151561561456156159"
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemNumber != '\(ItemNumber)'ORDER BY dateCreated DESC LIMIT \(0),10", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")

//                itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
                itemNumberStr = resultSet.stringForColumn("ItemNumber")
                globalItemArray.addObject(fetchFields)
 //print("globalItemArray is \(globalItemArray)")
            }
        }
        resultSet.close()
//        // print("arr is \(arr)")
    }
    
    func scrollViewDidScroll(_scrollView: UIScrollView)
    {
        let newScroll = CGFloat(_scrollView.contentOffset.y)
        
        if newScroll > _scrollView.contentSize.height - _scrollView.frame.height
        {
            //refreshList()
            ScrollfetchDataBase()
        }
        
    }
    

    
    func ScrollfetchDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
                let ItemNumber = "1223344567"
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemNumber != '\(ItemNumber)'ORDER BY dateCreated DESC LIMIT \(0),\(globalItemArray.count+10)", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                
                //                itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(fetchFields)
                itemNumberStr = resultSet.stringForColumn("ItemNumber")
                globalItemArray.addObject(fetchFields)
//                // print("globalItemArray is \(globalItemArray)")
            }
        }
        resultSet.close()
//        // print("arr is \(arr)")
    }
    
    //MArk :Close MenuButton
    func closeMenuBtnClicked(sender: UIButton)
    {
        
        myMenuView.hidden = true
        x = -myMenuView.frame.size.width
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = -myMenuView.frame.size.width
                
        })
        
        
    }

}
