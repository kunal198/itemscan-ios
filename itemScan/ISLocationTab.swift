//
//  ISLocationTab.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/18/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MobileCoreServices

var newLocationDesc = ""
class ISLocationTab: UIViewController,UIActionSheetDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{

    @IBOutlet weak var statusViewImage: UIView!
    @IBOutlet weak var addressTab: UITextField!
    @IBOutlet weak var addImageAddLoc: UIImageView!
    @IBOutlet weak var desTab: UITextField!
    @IBOutlet weak var locationNameTab: UITextField!
    
    var LocationNameForInsertion:NSString = NSString()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placeHolderMethodForLocTab()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        if newLocationDesc == ""
        {
            desTab.text = ""
        }
        else
        {
            desTab.text = newLocationDesc
            newLocationDesc = ""
        }
    }
    
    @IBAction func DesAddLoc(sender: AnyObject)
    {
        let ISDescriptionz = self.storyboard!.instantiateViewControllerWithIdentifier("ISDescription") as! ISDescription
        ISDescriptionz.isFromLocationItem = "true"
        self.navigationController?.pushViewController(ISDescriptionz, animated: true)
        desTab.text = ""
    }
    func placeHolderMethodForLocTab()
    {
        locationNameTab.attributedPlaceholder = NSAttributedString(string:"Location Name", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        addressTab.attributedPlaceholder = NSAttributedString(string:"Address (Optional)", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        desTab.attributedPlaceholder = NSAttributedString(string:"Description (Optional)", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
//        addCategoryTab.attributedPlaceholder = NSAttributedString(string:"Add Category", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex)
        {
        case 0:
             print("Cancel")
        case 1:
            Camera()
        case 2:
            Gallery()
        default:
             print("Default")
            
        }
    }
    
    func Camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
            
            
        }
    }
    
    func Gallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        // print("ISShowBarcodeDetail.swift delegate method")
        self.dismissViewControllerAnimated(false, completion: nil)
        
        addImageAddLoc.image = image
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
//        let fileManager = NSFileManager.defaultManager()
//        
//        let filePathToWrite = "\(documentDirectory)/profilePic.png"
//        
//        let data: NSData = UIImagePNGRepresentation(image)!
//        
//        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
//        
//         print("filePathToWrite is \(filePathToWrite)")
        
    }
    
    @IBAction func menuBtnLocTab(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func addImageAddLoc(sender: AnyObject)
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    
    @IBAction func doneBtnAddLoc(sender: AnyObject)
    {
        if (locationNameTab.text == "")
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "All text fields are mandatory"
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
//        else if (desTab.text == "")
//        {
//            let alert1 = UIAlertView()
//            alert1.title = "Alert"
//            alert1.message = "All text fields are mandatory"
//            alert1.addButtonWithTitle("Ok")
//            alert1.show()
//        }
//        else if (addressTab.text == "")
//        {
//            let alert1 = UIAlertView()
//            alert1.title = "Alert"
//            alert1.message = "All text fields are mandatory"
//            alert1.addButtonWithTitle("Ok")
//            alert1.show()
//        }
        else
        {
            insertLocationTable()
            // fetchLocationDataBase()
            self.navigationController?.popViewControllerAnimated(true)
        }

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        locationNameTab.resignFirstResponder()
        addressTab.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        
        if self.view.frame.size.width == 375
        {
            if textField == addressTab
            {
                
                UIView.animateWithDuration(0.2, animations:
                    {
                self.view.frame.origin.y = -80
                })
            
            }
           
        }
        else
        {
            if textField == addressTab
            {
                UIView.animateWithDuration(0.2, animations:
                    {
                        
                self.view.frame.origin.y = -50
                })
            }
           
            
            
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        locationNameTab.resignFirstResponder()
        addressTab.resignFirstResponder()
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.view.frame.origin.y = 0.0
    }
    
    
    func insertLocationTable()
    {
        var tblFields: Dictionary! = [String: String]()
        tblFields["LocationName"] = locationNameTab.text!
        tblFields["LocationDescription"] = desTab.text!
        tblFields["LocationAddress"] = addressTab.text!
        LocationNameForInsertion = locationNameTab.text!
        //        tblFields["LocationCategory"] = addCategoryTab.text!
        
       let Locaionid = ModelManager.instance.addTableData("Location", primaryKey:"LocationNumber", tblFields: tblFields)
     
        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
        let fileManager = NSFileManager.defaultManager()
        
        let filePathToWrite = "\(documentDirectory)/\(Locaionid).jpeg"
        
        let data: NSData = UIImageJPEGRepresentation(addImageAddLoc.image!,0.0)!
        
        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
        
    }
    
    func fetchLocationDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        // print(LocationNameForInsertion)
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("LocationListing", selectColumns: ["COUNT(*) as count"], whereString: "LocationListName = '\(LocationNameForInsertion)'", whereFields: [])
        arr = []
        
        resultSet.next()
        
        let messageCount = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        if messageCount == 0
        {
            var tblFields: Dictionary! = [String: String]()
            tblFields["LocationListName"] = LocationNameForInsertion as String
            
            _ = ModelManager.instance.addTableData("LocationListing", primaryKey:"LocationListNumber", tblFields: tblFields)
            
        }
    }

}
