//
//  Location.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/14/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class Location: NSObject
{
    var LocationName:String = String()
    var LocationDescription:String = String()
    var LocationAddress:String = String()
    var LocationCategory:String = String()

}
