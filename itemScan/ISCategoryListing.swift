//
//  ISCategoryListing.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

    var number:NSString = NSString()
   var addCategoryStr:NSString = NSString()

class ISCategoryListing: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var categoryListingArray = NSMutableArray()
   
    var isselectedCategory = false
    
    var selectedCategoryValue = String()
    

    
    @IBOutlet weak var categoryListingTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //fillArrayForCategoryListing()
       

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backFromCategoryListing(sender: AnyObject)
    {
         self.dismissViewControllerAnimated(true, completion: nil)
    }
  
    func fillArrayForCategoryListing()
    {
        categoryListingArray .addObject(["Category":"Category1"])
        categoryListingArray .addObject(["Category":"Category2"])
        categoryListingArray .addObject(["Category":"Category3"])
        categoryListingArray .addObject(["Category":"Category4"])
        categoryListingArray .addObject(["Category":"Category5"])
        categoryListingArray .addObject(["Category":"Category6"])
        categoryListingArray .addObject(["Category":"Category7"])
        
        
        // print("categoryListingArray is\(categoryListingArray)")
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        fetchCategoryListingDataBase()
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
         // print("categoryListingArray is\(categoryListingArray)")
        return categoryListingArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        cell.categoryName.text = categoryListingArray[indexPath.row].valueForKey("CATEGORY") as? String

        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if selectedCategoryValue == "CategorySelection"
        {
            addCategoryStr = (categoryListingArray[indexPath.row].valueForKey("CATEGORY") as? String)!
          //  updateItemListingForAddingCategory()
            self.dismissViewControllerAnimated(true, completion: nil)

        }
        else
        {
            addCategoryStr = (categoryListingArray[indexPath.row].valueForKey("CATEGORY") as? String)!
            updateItemListingForAddingCategory()
            self.dismissViewControllerAnimated(true, completion: nil)

        }
        
//        
//        addCategoryStr = (categoryListingArray[indexPath.row].valueForKey("CATEGORY") as? String)!
//        updateItemListingForAddingCategory()
//        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func updateItemListingForAddingCategory()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["ItemCategory"] = addCategoryStr as String
        
        print("number is \(number)")
        
        _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(number)'", whereFields: [])
        
    }
    func fetchCategoryListingDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("CategoryListing", selectColumns: ["*"], whereString: "", whereFields: [])
        arr = []
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("CategoryName"), forKey: "CATEGORY")
                arr.addObject(fetchFields)
                categoryListingArray.addObject(fetchFields)
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }
}
