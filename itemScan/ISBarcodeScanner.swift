//
//  ISBarcodeScanner.swift
//  itemScan
//
//  Created by mrinal khullar on 4/29/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import AVFoundation

var arrayForScannedBarcode:NSMutableArray = NSMutableArray()
var itemBarcodeForSelectedItem:NSString = NSString()

class ISBarcodeScanner: UIViewController, AVCaptureMetadataOutputObjectsDelegate
{
    
    
    
    var differentiateLocationType = String()
    var ItemName = NSString()
    var ItemNumber = NSString()
    
    var ItemAssignToValue:NSString = NSString()
    var ItemAssignNumberValue:NSString = NSString()
    var barCodeValue = NSString()
    var categoryValue = NSString()
    var colorValue = NSString()
    var descriptionValue = NSString()
    var itemNameValue = NSString()
    var ItemLocationValue = NSString()
    
    
    var numForScannedAssignName:NSString = NSString()
    var numForQRAssignName:NSString = NSString()

    var selectedPeople:NSString = NSString()
    var isOutOrIn = Bool()
   // var assignName:NSString = NSString()
    
  
    var peopleString = String()
    
    var ItemBarcode:String = NSString() as String
    
    var TypeCode = String()

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var closeBtn:UIButton = UIButton()
     var scanlabel:UILabel = UILabel()
    var backimg : UIImageView = UIImageView()
    
    var isSearch = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        scanbarcode()
        
        backimg.frame = CGRectMake(15,30,25,22)
        backimg.backgroundColor = UIColor.clearColor()
        scanlabel.frame = CGRectMake(50,30,60,30)
        scanlabel.backgroundColor = UIColor.clearColor()
        scanlabel.text = "Scan Your Bar code"
        scanlabel.attributedText = NSAttributedString(string:"Scan Your Bar code", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])

        backimg.image = UIImage (named: "back_arrow.png")
        backimg.contentMode = UIViewContentMode.ScaleAspectFit;
            closeBtn.frame = CGRectMake(3,3,50,40)
        closeBtn.backgroundColor = UIColor.clearColor()
        closeBtn.addTarget(self, action:"Goback",forControlEvents: UIControlEvents.TouchUpInside)
        // Do any additional setup after loading the view, typically from a nib.
        
        // Do any additional setup after loading the view.
    }

     func Goback()
     {
          isCheckOutForPeopleSelected = true
        self.navigationController?.popViewControllerAnimated(true)
      }
    
    
    
    func scanbarcode()
    {
        
        view.backgroundColor = UIColor.blackColor()
        captureSession = AVCaptureSession()
        
        let videoCaptureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        let videoInput: AVCaptureDeviceInput
        
        do
        {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            print("videoInput is \(videoInput)")
        }
        catch
        {
            return
        }
        
        if (captureSession.canAddInput(videoInput))
        {
            captureSession.addInput(videoInput)
        }
        else
        {
            failed()
            
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput))
        {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypePDF417Code,AVMetadataObjectTypeQRCode]
        }
        else
        {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
        previewLayer.frame = view.layer.bounds;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        view.layer.addSublayer(previewLayer);
        view.addSubview(closeBtn)
        view.addSubview(backimg)

        captureSession.startRunning();

    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func failed()
    {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .Alert)
        ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(ac, animated: true, completion: nil)
        captureSession = nil
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.running == false) {
            captureSession.startRunning();
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.running == true) {
            captureSession.stopRunning();
        }
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!)
    {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first
        {
            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            foundCode(readableObject.stringValue);
        }
        
       // dismissViewControllerAnimated(true, completion: nil)
    }
    
    func foundCode(code: String)
    {
        let createAccountErrorAlert: UIAlertView = UIAlertView()
        createAccountErrorAlert.delegate = self
        createAccountErrorAlert.title = "Alert"
        createAccountErrorAlert.message = code
        createAccountErrorAlert.addButtonWithTitle("Ok")
        createAccountErrorAlert.addButtonWithTitle("Retry")
        createAccountErrorAlert.show()
        NSUserDefaults.standardUserDefaults().setObject(code, forKey: "barcode")
        NSUserDefaults.standardUserDefaults().synchronize()
        ItemBarcode = code
        itemBarcodeForSelectedItem = code
        print("ItemBarcode under ISBarcodeScanner.swift is \(code)")
        
         print("peopel selected by didselect value = \(peopleSeclectedForScannedBarcode)")
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        return .Portrait
    }

    
    //MARK: AlertviewDelegate
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int)
    {
    
        if(isSearch ==  true)
            {
                if isCheckInForLocationSelected == false
                {
                    switch buttonIndex
                    {
                    case 0:
                        self.navigationController?.popViewControllerAnimated(true)
                        NSUserDefaults.standardUserDefaults().setObject("Bar code Added successfully", forKey: "qr/Barcode")
                        NSUserDefaults.standardUserDefaults().synchronize()
                        isbackfromqR = true
                    case 1:
                        scanbarcode()
                    default:
                        print ("retry")
                    }
                    

                }
                
                
            }
    
        
        
        if(isCheckInForLocationSelected == true)
        {
            switch buttonIndex
            {
            case 0:
                print ("Show Detail of barcode scanned item!!")

                
                let resultSet: FMResultSet!
                
                if TypeCode == "QRCode"
                {
                
                   resultSet = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemName = '\(ItemBarcode)'", whereFields: [])
                    
                }
                else
                {
                     resultSet = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemBarcode = '\(ItemBarcode)'", whereFields: [])
                }

                resultSet.next()
                
                let messageCount = Int(resultSet.intForColumn("count"))
                
                resultSet.close()
                
                if messageCount == 0
                {
                    
                    if TypeCode == "QRCode"
                    {
                        let alert1 = UIAlertView()
                        alert1.title = "Alert"
                        alert1.message = "Item QR code does not exist!!"
                        alert1.addButtonWithTitle("Ok")
                        alert1.show()

                    }
                    else
                    {
                        let alert1 = UIAlertView()
                        alert1.title = "Alert"
                        alert1.message = "Item Barcode does not exist!!"
                        alert1.addButtonWithTitle("Ok")
                        alert1.show()

                    }
                    
                    self.navigationController?.popViewControllerAnimated(true)
                }
                else
                {
                     let resultSet: FMResultSet!
                    
                    if TypeCode == "QRCode"
                    {
                        
                        resultSet =  ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemName = '\(ItemBarcode)'", whereFields: [])
                        
                    }
                    else
                    {
                         resultSet =  ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemBarcode = '\(ItemBarcode)'", whereFields: [])
                    }

                
                    
                    if (resultSet != nil)
                    {
                        while resultSet.next()
                        {
                            let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                            
                            fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                            
                            fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                            fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                            fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                            fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                            fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                            fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                            fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ITEMASSIGNTO")
                            
                            //                        itemNumberStr = resultSet.stringForColumn("ItemNumber")
                            arrayForScannedBarcode.addObject(fetchFields)
                            
                            
                            
                            
                            print("arrayForScannedBarcode is \(arrayForScannedBarcode)")
                            
                            print("last index values in array are = \(arrayForScannedBarcode.lastObject)")
                            
                            
                            
                            
                        ItemAssignToValue = arrayForScannedBarcode.lastObject!.valueForKey("ITEMASSIGNTO") as! String
                        ItemAssignNumberValue =  arrayForScannedBarcode.lastObject!.valueForKey("NUMBER") as! String
                         barCodeValue  = arrayForScannedBarcode.lastObject!.valueForKey("BARCODE") as! String
                         categoryValue  = arrayForScannedBarcode.lastObject!.valueForKey("CATEGORY") as! String
                          colorValue = arrayForScannedBarcode.lastObject!.valueForKey("COLOR") as! String
                         descriptionValue =  arrayForScannedBarcode.lastObject!.valueForKey("DESCRIPTION") as! String
                         itemNameValue  = arrayForScannedBarcode.lastObject!.valueForKey("NAME") as! String
                        peopleSeclectedForScannedBarcode = arrayForScannedBarcode.lastObject!.valueForKey("ITEMASSIGNTO") as! String

                        ItemLocationValue  = arrayForScannedBarcode.lastObject!.valueForKey("LOCATION") as! String

                            if differentiateLocationType == "PeopleValue"
                            {
                                isOutOrIn = true
                            }
                            else
                            {
                                isOutOrIn = false
                                peopleSeclectedForScannedBarcode = " "

                            }
                            
                            
                            selectedPeople = ""

                        }
                    }
                     resultSet.close()

//                    let ISSelectedItemz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectedItem") as! ISSelectedItem
//                    self.navigationController?.pushViewController(ISSelectedItemz, animated: true)
                    
                    updateItemListing()
                    updateItems()
                    
                        let ISSelectedItemz = self.storyboard!.instantiateViewControllerWithIdentifier("PeopleProfileViewController") as! PeopleProfileViewController
                        ISSelectedItemz.peopleString = "BarCodeProfile"
                      //  ISSelectedItemz.ItemName =

                    if differentiateLocationType == "PeopleValue"
                    {
                        ISSelectedItemz.differentiateLocationType = "PeopleValue"
                    }
                    else
                    {
                        ISSelectedItemz.differentiateLocationType = "LocationValue"
                    }
                    

                      ISSelectedItemz.peopleString = peopleSeclectedForScannedBarcode as String
                        self.navigationController?.pushViewController(ISSelectedItemz, animated: true)
                }
                
     
            case 1:
                scanbarcode()
                
            default:
                print ("Retry")
            }
        }
        else if (isCheckOutForPeopleSelected == true)
        {
            switch buttonIndex
            {
            case 0:
                print ("Show Detail of barcode scanned item For assigning people!!")
                
//                let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemBarcode = '\(ItemBarcode)'", whereFields: [])
//                
//                resultSet.next()
//                
//                let messageCount = Int(resultSet.intForColumn("count"))
//                
//                resultSet.close()
                var messageCount = Int()
                
                if TypeCode == "QRCode"
                {
                    let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemName = '\(ItemBarcode)'", whereFields: [])
                    
                    resultSet.next()
                    
                    messageCount = Int(resultSet.intForColumn("count"))
                                    
                    resultSet.close()
                }
                else
                {
                    let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemBarcode = '\(ItemBarcode)'", whereFields: [])
                    
                    resultSet.next()
                    
                    messageCount = Int(resultSet.intForColumn("count"))
                    
                    resultSet.close()
                }
                
                
                
                if messageCount == 0
                {
//                    let alert1 = UIAlertView()
//                    alert1.title = "Alert"
//                    alert1.message = "Item Barcode does not exist!!"
//                    alert1.addButtonWithTitle("Ok")
//                    alert1.show()
                    
                    if TypeCode == "QRCode"
                    {
                        let alert1 = UIAlertView()
                        alert1.title = "Alert"
                        alert1.message = "Item QR code does not exist!!"
                        alert1.addButtonWithTitle("Ok")
                        alert1.show()
                        
                    }
                    else
                    {
                        let alert1 = UIAlertView()
                        alert1.title = "Alert"
                        alert1.message = "Item Barcode does not exist!!"
                        alert1.addButtonWithTitle("Ok")
                        alert1.show()
                        
                    }

                    
                    self.navigationController?.popViewControllerAnimated(true)
                }
                else
                {
                    let resultSet: FMResultSet!
                    
                    
                  if TypeCode == "QRCode"
                  {
                     resultSet = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemName = '\(ItemBarcode)'", whereFields: [])
                  }
                  else
                  {
                      resultSet = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemBarcode = '\(ItemBarcode)'", whereFields: [])
                   }
                    
                    
                    
                    
                    
                    if (resultSet != nil)
                    {
                        while resultSet.next()
                        {
                            let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                            
                            fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                            
                            fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                            fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                            fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                            fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                            fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                            fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                            fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ITEMASSIGNTO")
                            
                            //                        itemNumberStr = resultSet.stringForColumn("ItemNumber")
                            arrayForScannedBarcode.addObject(fetchFields)
                            
                            print("arrayForScannedBarcode for assigning people is \(arrayForScannedBarcode)")
                            
                            
                            self.itemNameValue = arrayForScannedBarcode.lastObject!.valueForKey("NAME") as! String
                            ItemAssignNumberValue = arrayForScannedBarcode.lastObject!.valueForKey("NUMBER") as! String
                            
                            
                            if differentiateLocationType == "PeopleValue"
                            {
                                isOutOrIn = true
                            }
                            else
                            {
                                isOutOrIn = false
                                peopleSeclectedForScannedBarcode = " "
                                
                            }

                            
//                            isOutOrIn = true
//                            peopleSeclectedForScannedBarcode = " "
//                            selectedPeople = " "
                            

                        }
                    }
                    resultSet.close()
                    
                    updateItemListing()

                    
                    
//                    let ISSelectedItemz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectedItem") as! ISSelectedItem
//                    self.navigationController?.pushViewController(ISSelectedItemz, animated: true)
                    
                    
                    
                   
                    
                    let ISSelectedItemz = self.storyboard!.instantiateViewControllerWithIdentifier("PeopleProfileViewController") as! PeopleProfileViewController
                    ISSelectedItemz.peopleString = "BarCodeProfile"
                    
                    if differentiateLocationType == "PeopleValue"
                    {
                        ISSelectedItemz.differentiateLocationType = "PeopleValue"
                    }
                    else
                    {
                         ISSelectedItemz.differentiateLocationType = "LocationValue"
                    }

                    print("value of people selected = \(peopleSeclectedForScannedBarcode)")
                     ISSelectedItemz.peopleString = peopleSeclectedForScannedBarcode as String
                    self.navigationController?.pushViewController(ISSelectedItemz, animated: true)

                }
                
                
            case 1:
                scanbarcode()
                
            default:
                print ("retry")
            }
        }
        else
        {
            isCheckInForLocationSelected = false
            
//            switch buttonIndex
//            {
//            case 0:
//                self.navigationController?.popViewControllerAnimated(true)
//                NSUserDefaults.standardUserDefaults().setObject("Bar code Added successfully", forKey: "qr/Barcode")
//                NSUserDefaults.standardUserDefaults().synchronize()
//                isbackfromqR = true
//            case 1:
//                scanbarcode()
//            default:
//                print ("retry")
//            }
        }
    }
    
    
    
    
    
    func updateItems()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["ItemName"] = self.itemNameValue as String
        tblFields["ItemDescription"] = self.descriptionValue as String
        tblFields["ItemLocation"] = locationSelectedByScanning as String
        tblFields["ItemColor"] = self.colorValue as String
        tblFields["ItemCategory"] = self.categoryValue as String
        tblFields["ItemBarcode"] = self.barCodeValue as String
        
        print("number of edit item is = \(self.ItemAssignNumberValue)")
        
        _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(self.ItemAssignNumberValue)'", whereFields: [])
        
//        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
//        let fileManager = NSFileManager.defaultManager()
//        
//        let filePathToWrite = "\(documentDirectory)/\(numberOfEditItem).jpeg"
//        
//        let data: NSData = UIImageJPEGRepresentation(editImageView.image!,0.0)!
//        
//        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
        
        // print("filePathToWrite is \(filePathToWrite)")
        
    }

    
    
    
    func updateItemListing()
    {
//        if(isCheckInForLocationSelected == true)
//        {
//            var tblFields: Dictionary! = [String: String]()
//            
//            tblFields["ItemName"] = assignName as String
//            tblFields["ItemAssignTo"] = selectedPeople as String
//            
//            _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numForScannedAssignName)'", whereFields: [])
//        }
//        else if(isCheckInQRForLcoation == true || isCheckOutQRForPeople == true)
//        {
//            var tblFields: Dictionary! = [String: String]()
//            
//            tblFields["ItemName"] = assignName as String
//            tblFields["ItemAssignTo"] = selectedPeople as String
//            
//            _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(numForQRAssignName)'", whereFields: [])
//        }
//        else
//        {
         var tblFields: Dictionary! = [String: String]()
        
        
       
        
           tblFields["ItemName"] = self.itemNameValue as String
          tblFields["ItemAssignTo"] = peopleSeclectedForScannedBarcode as String
            
           _ = ModelManager.instance.updateTableData("Item", tblFields: tblFields, whereString: "ItemNumber='\(ItemAssignNumberValue)'", whereFields: [])
  //      }
        
        
        // print("tblFields under update is \(tblFields)")
        
        insertRecentBase()
    }

    
    func insertRecentBase()
    {
        var tblFields: Dictionary! = [String: String]()
        
        if isOutOrIn == true
        {
            tblFields["RecentStatus"] = "Checked Out"
            tblFields["RecentName"] = self.itemNameValue as String
        }
        else
        {
            tblFields["RecentStatus"] = "Checked In"
            tblFields["RecentName"] = self.itemNameValue as String
      }
        
        
        
        _ = ModelManager.instance.addTableData("Recent", primaryKey:"RecentNumber", tblFields: tblFields)
        
    }

}
