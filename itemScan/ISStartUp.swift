//
//  ISStartUp.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/13/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit
import MobileCoreServices

class ISStartUp: UIViewController,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITextFieldDelegate
{
    
    //++++++++++++++++++++++++++++++++++++++++++++++//
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var enterOutlet: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        insertDataBase()
        
        // set imagePath
        self.navigationController?.navigationBar.hidden = true
        placeHolderMethodForISEnter()
        
        enterOutlet.backgroundColor = UIColor.clearColor()
        enterOutlet.layer.cornerRadius = 6
        enterOutlet.layer.borderWidth = 2
        enterOutlet.layer.borderColor = UIColor(red: 244.0/255.0, green: 208.0/255.0, blue: 67.0/255.0, alpha: 1.0).CGColor
    }
    
    func saveImage(image: UIImage, path: String) -> Bool {
        let pngImageData = UIImagePNGRepresentation(image)
        let result = pngImageData!.writeToFile(path, atomically: true)
        return result
    }
    
    func loadImageFromPath(path: String) -> UIImage? {
        let data = NSData(contentsOfFile: path)
        let image = UIImage(data: data!)
        return image
    }
    
    func placeHolderMethodForISEnter()
    {
        userTextField.attributedPlaceholder = NSAttributedString(string:"Username", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        userTextField.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        userTextField.resignFirstResponder()
    }
    
    @IBAction func selectImageFromCameraOrGallery(sender: AnyObject)
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch (buttonIndex)
        {
        case 0:
             print("Cancel")
        case 1:
            Camera()
        case 2:
            Gallery()
        default:
             print("Default")
            
        }
    }
    
    func Camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            // print("Camera capture")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
            
            
        }
    }
    
    func Gallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?)
    {
        // print("ISShowBarcodeDetail.swift delegate method")
        self.dismissViewControllerAnimated(false, completion: nil)
        
        userImage.image = image
       
        let documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        
        let fileManager = NSFileManager.defaultManager()
        
        let filePathToWrite = "\(documentDirectory)/profilePic.png"
        
        let data: NSData = UIImagePNGRepresentation(image)!
        
        fileManager.createFileAtPath(filePathToWrite, contents: data, attributes: nil)
        
        // print("filePathToWrite is \(filePathToWrite)")
        
    }
    
    func insertDataBase()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["ItemBarcode"] = "1234567890"
        tblFields["ItemLocation"] = "mohali da bus adda"
        tblFields["ItemAssignTo"] = "p"
        tblFields["ItemDescription"] = "bahut zada kharab"
        tblFields["ItemColor"] = "Brown"
        tblFields["ItemStatus"] = "Heart broken"
        
        
         _ = ModelManager.instance.addTableData("Item", primaryKey:"ItemNumber", tblFields: tblFields)
        
        fetchDataBase()
    }
    
    func fetchDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        
        arr = []
        var itemDesc = ""
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemBarcode = '123456789'", whereFields: [])
        if (resultSet != nil)
        {
            while resultSet.next()
            {
              itemDesc = resultSet.stringForColumn("ItemDescription")
                arr.addObject(itemDesc)
            }
        }
        resultSet.close()
        // print("arr is \(arr)")
    }
    

    @IBAction func enterBtn(sender: AnyObject)
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(userTextField.text, forKey: "USER")
        
        let ISHomeScreenz = self.storyboard!.instantiateViewControllerWithIdentifier("ISHomeScreen") as! ISHomeScreen
        
        self.navigationController?.pushViewController(ISHomeScreenz, animated: true)
    }
}
