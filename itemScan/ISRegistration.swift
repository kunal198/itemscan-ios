//
//  ISRegistration.swift
//  itemScan
//
//  Created by Mrinal Khullar on 4/7/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class ISRegistration: UIViewController,UITextFieldDelegate
{

    
    
     var PeopleNameForInsertion:NSString = NSString()
    
    @IBOutlet weak var createAccountOutlet: UIButton!
    @IBOutlet weak var alreadyAccountOutlet: UIButton!
    @IBOutlet weak var rePasswordTextFieldRegistration: UITextField!
    @IBOutlet weak var passwordTextFieldRegistration: UITextField!
    @IBOutlet weak var emailTextFieldRegistration: UITextField!
    @IBOutlet weak var usernameTextFieldRegistration: UITextField!
    @IBOutlet weak var nameTextFieldRegistration: UITextField!
    @IBOutlet weak var rePasswordHighlightView: UIImageView!
    @IBOutlet weak var passwordHighlightView: UIImageView!
    @IBOutlet var Addressfeild: UITextField!
    @IBOutlet weak var emailHighlightView: UIImageView!
    @IBOutlet weak var nameHighlightView: UIImageView!
    @IBOutlet weak var usernameHighlightView: UIImageView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.hidden = true
         placeHolderMethod()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        usernameTextFieldRegistration.resignFirstResponder()
        nameTextFieldRegistration.resignFirstResponder()
        emailTextFieldRegistration.resignFirstResponder()
        passwordTextFieldRegistration.resignFirstResponder()
        rePasswordTextFieldRegistration.resignFirstResponder()
       

    }

    
    func placeHolderMethod()
    {
        rePasswordTextFieldRegistration.attributedPlaceholder = NSAttributedString(string:"Re-Password", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
         passwordTextFieldRegistration.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
         usernameTextFieldRegistration.attributedPlaceholder = NSAttributedString(string:"Username (optional)", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
         nameTextFieldRegistration.attributedPlaceholder = NSAttributedString(string:"Name", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
         emailTextFieldRegistration.attributedPlaceholder = NSAttributedString(string:"Email", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
         Addressfeild.attributedPlaceholder = NSAttributedString(string:"Address (optional)", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        createAccountOutlet.backgroundColor = UIColor.clearColor()
        createAccountOutlet.layer.cornerRadius = 6
        createAccountOutlet.layer.borderWidth = 2
        createAccountOutlet.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
        
        
        alreadyAccountOutlet.backgroundColor = UIColor.clearColor()
        alreadyAccountOutlet.layer.cornerRadius = 6
        alreadyAccountOutlet.layer.borderWidth = 2
        alreadyAccountOutlet.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
    }
    
    func isValidEmail(emailid: NSString)->Bool
    {
        var isValid = true
        
        var isTrimmed = true
        
        var emailId = emailid
        
        while(isTrimmed)
        {
            
            let startSpace = emailId.substringWithRange(NSRange(location: 0, length: 1))
            
            let endSpace = emailId.substringWithRange(NSRange(location: emailId.length-1, length: 1))
            
            if startSpace == " " || endSpace == " "
            {
                if startSpace == " "
                {
                    emailId = emailId.stringByReplacingCharactersInRange(NSRange(location: 0, length: 1), withString: "")
                }
                if endSpace == " "
                {
                    emailId = emailId.stringByReplacingCharactersInRange(NSRange(location: emailId.length-1, length: 1), withString: "")
                }
            }
            else
            {
                isTrimmed = false
            }
        }
        
        
        if !emailId.containsString(" ")
        {
            var atRateSplitArray = emailId.componentsSeparatedByString("@")
            
            if(atRateSplitArray.count>=2)
            {
                for component in atRateSplitArray
                {
                    if component == ""
                    {
                        isValid = false
                    }
                }
                
                if(isValid)
                {
                    let dotSplitArray = atRateSplitArray[atRateSplitArray.count-1].componentsSeparatedByString(".")
                    
                    if(dotSplitArray.count>=2)
                    {
                        for component in dotSplitArray
                        {
                            if component == ""
                            {
                                isValid = false
                            }
                        }
                    }
                    else
                    {
                        isValid = false
                    }
                }
            }
            else
            {
                isValid = false
            }
            
        }
        else
        {
            isValid = false
        }
        
        return isValid
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.view.frame.origin.y = 0.0
    }
    
    func textFieldDidBeginEditing(textField: UITextField)
    {
        if textField == passwordTextFieldRegistration
        {
            self.view.frame.origin.y = -40.0
        }
        else if textField == rePasswordTextFieldRegistration
        {
            self.view.frame.origin.y = -55.0
        }
        else if textField == Addressfeild
        {
            self.view.frame.origin.y = -80.0
        }
       
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == nameTextFieldRegistration
        {
            nameTextFieldRegistration.resignFirstResponder()
            usernameTextFieldRegistration.becomeFirstResponder()
        }
        else if textField == usernameTextFieldRegistration
        {
            usernameTextFieldRegistration.resignFirstResponder()
            emailTextFieldRegistration.becomeFirstResponder()
        }
        else if textField == emailTextFieldRegistration
        {
            emailTextFieldRegistration.resignFirstResponder()
            passwordTextFieldRegistration.becomeFirstResponder()
        }
        else if textField == passwordTextFieldRegistration
        {
            passwordTextFieldRegistration.resignFirstResponder()
            rePasswordTextFieldRegistration.becomeFirstResponder()
        }
        else if textField == rePasswordTextFieldRegistration
        {
            rePasswordTextFieldRegistration.resignFirstResponder()
            Addressfeild.becomeFirstResponder()
        }
        else
        {
           Addressfeild.resignFirstResponder()
        }
        return true
    }

    

    @IBAction func createBtn(sender: AnyObject)
    {
        if nameTextFieldRegistration.text == "" || emailTextFieldRegistration .text == "" || passwordTextFieldRegistration.text == "" || rePasswordTextFieldRegistration.text == ""// || Addressfeild.text == ""
        {
          let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "All text fields are mandatory"
            alert1.addButtonWithTitle("Ok")
            alert1.show()

        }
        else if !isValidEmail(emailTextFieldRegistration.text!)
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Enter valid emailid."
            alert1.addButtonWithTitle("Ok")
            alert1.show()
            
        }
        else if passwordTextFieldRegistration.text?.characters.count < 4
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Enter strong password."
            alert1.addButtonWithTitle("Ok")
            alert1.show()
        }
        else if passwordTextFieldRegistration.text != rePasswordTextFieldRegistration.text
        {
            let alert1 = UIAlertView()
            alert1.title = "Alert"
            alert1.message = "Password does not match."
            alert1.addButtonWithTitle("Ok")
            alert1.show()
            
        }
            
        else
        {
            defaultRegistration()
            
          //  insertPeopleDataBase()
            
          //   fetchPeopleDataBase()
            
            
//            NSUserDefaults.standardUserDefaults().setObject(Bool(true), forKey:"Login")
//            let ISHomeScreenz = self.storyboard!.instantiateViewControllerWithIdentifier("ISHomeScreen") as! ISHomeScreen
//            self.navigationController?.pushViewController(ISHomeScreenz, animated: true)
            
            self.navigationController!.popViewControllerAnimated(true)
        }
    }
    
    
    
    func insertPeopleDataBase()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["PeopleName"] =  nameTextFieldRegistration.text!
        PeopleNameForInsertion =  nameTextFieldRegistration.text!
        
        _ = ModelManager.instance.addTableData("People", primaryKey:"PeopleNumber", tblFields: tblFields)
        
       print("insertPeopleDataBase is \(tblFields)")
    }

    
    func fetchPeopleDataBase()
    {
        var arr:NSMutableArray = NSMutableArray()
        // print("PeopleNameForInsertion is \(PeopleNameForInsertion)")
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("PeopleListing", selectColumns: ["COUNT(*) as count"], whereString: "PeopleListName = '\(PeopleNameForInsertion)'", whereFields: [])
        arr = []
        
        resultSet.next()
        
        let messageCount = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        if messageCount == 0
        {
            var tblFields: Dictionary! = [String: String]()
            tblFields["PeopleListName"] = PeopleNameForInsertion as String
            
            _ = ModelManager.instance.addTableData("PeopleListing", primaryKey:"PeopleListNumber", tblFields: tblFields)
        }
    }

    
    @IBAction func alreadyAccountBtn(sender: AnyObject)
    {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func defaultRegistration()
    {
        let tblFields: NSMutableDictionary! = NSMutableDictionary()
        let defaultzz = NSUserDefaults.standardUserDefaults()
        tblFields.setObject(nameTextFieldRegistration.text!, forKey: "name")
        tblFields.setObject(usernameTextFieldRegistration.text!, forKey: "username")
        
        print("default is \((usernameTextFieldRegistration.text!, forKey: "username"))")
        
        tblFields.setObject(emailTextFieldRegistration.text!, forKey: "email")
        tblFields.setObject(passwordTextFieldRegistration.text!, forKey: "password")
        tblFields.setObject(rePasswordTextFieldRegistration.text!, forKey: "repassword")
        
        defaultzz.setObject(tblFields, forKey: emailTextFieldRegistration.text!)
        defaultzz.setObject(tblFields, forKey: usernameTextFieldRegistration.text!)
        
        
        print("tblfields are = \(tblFields)")
        
        
        let array = NSMutableArray()
        array.addObject(tblFields)
        
         print("tblfields  array are = \(array)")
     
        
//         let ArrayValueInDefaults = NSUserDefaults.standardUserDefaults()
//       
//        ArrayValueInDefaults.setObject(array, forKey: "SavedArray")
//        ArrayValueInDefaults.synchronize()
//       let sss = ArrayValueInDefaults.objectForKey("SavedArray") as? [AnyObject] ?? [AnyObject]()
//        
//        print("saved ay = \(sss)")
        
        
        
       
        
        let defaultsArray = NSUserDefaults.standardUserDefaults()
        defaultsArray.setObject(array, forKey: "SavedStringArray")
  
        
       // let defaultsArray = NSUserDefaults.standardUserDefaults()
        let savedValueArray = defaultsArray.objectForKey("SavedStringArray") as? [AnyObject] ?? [AnyObject]()
    }
    
    func insertDataBaseRegistration()
    {
        var tblFields: Dictionary! = [String: String]()
        tblFields["name"] = nameTextFieldRegistration.text
        tblFields["username"] = usernameTextFieldRegistration.text
        tblFields["email"] = emailTextFieldRegistration.text
        tblFields["password"] = passwordTextFieldRegistration.text
        tblFields["repassword"] = rePasswordTextFieldRegistration.text
       
        
        _ = ModelManager.instance.addTableData("User", primaryKey:"userNumber", tblFields: tblFields)
        
    }
}
