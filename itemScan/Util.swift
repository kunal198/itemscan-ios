//
//  Util.swift
//  DemoProject
//
//  Created by Krupa-iMac on 24/07/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

class Util: NSObject
{
    
    class func getPath(fileName: String) -> String
    {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let fileURL = documentsURL.URLByAppendingPathComponent(fileName)
        
//        // print("fileURL is \(fileURL)")
        return fileURL.path!
    }
    
     class func copyFile(fileName: NSString) {
        var dbPath: String = getPath(fileName as String)
        //// println(dbPath)

        var fileManager = NSFileManager.defaultManager()
        if !fileManager.fileExistsAtPath(dbPath)
        {
            var fromPath: String? = ((NSBundle.mainBundle().resourcePath)! as NSString).stringByAppendingPathComponent(fileName as String)

            var error : NSError?

            let docsPath = NSBundle.mainBundle().resourcePath!
            let fileManager = NSFileManager.defaultManager()

            let docsArray: [AnyObject]?
            do {
                docsArray = try fileManager.contentsOfDirectoryAtPath(docsPath)
            } catch var error1 as NSError {
                error = error1
                docsArray = nil
            }

            do {
                //// println(docsArray)

    
                try fileManager.copyItemAtPath(fromPath!, toPath: dbPath)
            } catch var error1 as NSError {
                error = error1
            }
            var alert: UIAlertView = UIAlertView()
            if (error != nil) {
                alert.title = "Error Occured"
                alert.message = error?.localizedDescription
                alert.delegate = nil
                alert.addButtonWithTitle("Ok")
                alert.show()
            }
            else
            {
                alert.title = "Successfully Copy"
                alert.message = "Your database copy successfully"
            }
        }
    }

    class func invokeAlertMethod(strTitle: NSString, strBody: NSString, delegate: AnyObject?)
    {
        let alert: UIAlertView = UIAlertView()
        alert.message = strBody as String
        alert.title = strTitle as String
        alert.delegate = delegate
        alert.addButtonWithTitle("Ok")
        alert.show()
    }
}