//
//  ISSearchQRCode.swift
//  itemScan
//
//  Created by Mrinal Khullar on 6/1/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

var arrayForQRCode:NSMutableArray = NSMutableArray()
var SearchString:NSString = NSString()

var btnCheck = String()

class ISSearchQRCode: UIViewController,UITextFieldDelegate
{

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placeHolderMethodForSearchQR()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtn(sender: AnyObject)
    {
        
          btnCheck = "Back"
        isCheckInForLocationSelected = true
        isCheckOutForPeopleSelected = true
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func searchBtn(sender: AnyObject)
    {
        isCheckInForLocationSelected = false
        isCheckOutForPeopleSelected = false
        
        if(isCheckInQRForLcoation == true || isCheckOutQRForPeople == true)
        {
            SearchString = searchTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            print ("searchTextField.text is\(SearchString)")
            
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["COUNT(*) as count"], whereString: "ItemName = '\(SearchString)'", whereFields: [])
            
            resultSet.next()
            
            let messageCount = Int(resultSet.intForColumn("count"))
            
            resultSet.close()
            
            if messageCount == 0
            {
                let alert1 = UIAlertView()
                alert1.title = "Alert"
                alert1.message = "Item QR Code does not exist!"
                alert1.addButtonWithTitle("Ok")
                alert1.show()
                isCheckInForLocationSelected = true
                isCheckOutForPeopleSelected = true
                self.navigationController?.popViewControllerAnimated(true)
            }
            else
            {
                let resultSet: FMResultSet! = ModelManager.instance.getTableData("Item", selectColumns: ["*"], whereString: "ItemName = '\(SearchString)'", whereFields: [])
                
                if (resultSet != nil)
                {
                    while resultSet.next()
                    {
                        let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                        
                        fetchFields.setObject(resultSet.stringForColumn("ItemNumber"), forKey: "NUMBER")
                        
                        fetchFields.setObject(resultSet.stringForColumn("ItemDescription"), forKey: "DESCRIPTION")
                        fetchFields.setObject(resultSet.stringForColumn("ItemBarcode"), forKey: "BARCODE")
                        fetchFields.setObject(resultSet.stringForColumn("ItemLocation"), forKey: "LOCATION")
                        fetchFields.setObject(resultSet.stringForColumn("ItemColor"), forKey: "COLOR")
                        fetchFields.setObject(resultSet.stringForColumn("ItemName"), forKey: "NAME")
                        fetchFields.setObject(resultSet.stringForColumn("ItemCategory"), forKey: "CATEGORY")
                        fetchFields.setObject(resultSet.stringForColumn("ItemAssignTo"), forKey: "ITEMASSIGNTO")
                        
                        //                        itemNumberStr = resultSet.stringForColumn("ItemNumber")
                        arrayForQRCode.addObject(fetchFields)
                        
                        print("arrayForQRCode for assigning people is \(arrayForQRCode)")
                    }
                }
                resultSet.close()
                
                let ISSelectedItemz = self.storyboard!.instantiateViewControllerWithIdentifier("ISSelectedItem") as! ISSelectedItem
                self.navigationController?.pushViewController(ISSelectedItemz, animated: true)
        }

    }
 }

    func placeHolderMethodForSearchQR()
    {
        let paddingView = UIView(frame: CGRectMake(0, 0, 15, searchTextField.frame.height))
        searchTextField.leftView = paddingView
        searchTextField.leftViewMode = UITextFieldViewMode.Always
        
        searchTextField.attributedPlaceholder = NSAttributedString(string:"Search QR Code", attributes: [NSForegroundColorAttributeName: UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 16)!])
        
        searchView.backgroundColor = UIColor.clearColor()
        searchView.layer.cornerRadius = 6
        searchView.layer.borderWidth = 2
        searchView.layer.borderColor = UIColor(red: 170.0/255.0, green: 155.0/255.0, blue: 101.0/255.0, alpha: 1.0).CGColor
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        searchTextField.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        searchTextField.resignFirstResponder()
    }
}
