//
//  RecentItemCheckInOutViewController.swift
//  itemScan
//
//  Created by brst on 6/10/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

import UIKit

class RecentItemCheckInOutViewController: UIViewController {

    var SelectedItemName = String()
    
    var x: CGFloat = 0.0
    var navx: CGFloat = 0.0
    
    var recentListingArray:NSMutableArray = NSMutableArray()
    var recentItemName:NSString = NSString()
    
    @IBOutlet var MenuButton: UIButton!
    
    @IBOutlet var RecentTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        recentListingArray = []
        // Do any additional setup after loading the view.
        
        
        print("selected name = \(SelectedItemName)")
    }

    override func viewWillAppear(animated: Bool)
    {
        self.view.addSubview(closeMenuBtn)
        self.view.addSubview(myMenuView)
        myMenuView.frame = CGRectMake(-myMenuView.frame.size.width, myMenuView.frame.origin.y ,myMenuView.frame.size.width ,myMenuView.frame.size.height)
        
        x = -myMenuView.frame.size.width
        
        fetchInRecentDataBase()
    }

    
    @IBAction func MenuButtonAction(sender: AnyObject)
    {
        myMenuView.hidden = false
        
        if(x == -myMenuView.frame.size.width)
        {
            x = 0
            navx = myMenuView.frame.size.width
            closeMenuBtn.hidden = false
        }
        else
        {
            navx = 0
            closeMenuBtn.hidden = false
        }
        
        UIView.animateWithDuration(0.2, animations:
            {
                
                // self.navBar.frame.origin.x = self.navx
                myMenuView.frame.origin.x = self.x
                
        })

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //        // print("recentListingArray.count is \(recentListingArray.count)")
        return recentListingArray.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ISCustomCell", forIndexPath: indexPath) as! ISCustomCell
        
        cell.recentItemName.text = recentListingArray[indexPath.row].valueForKey("NAME") as? String
        
        let plzCheck = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
        //        // print("plzCheck is \(plzCheck)")
        if(plzCheck == "Checked Out")
        {
            cell.recentStatus.text = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
            cell.recentStatus.textColor = UIColor.redColor()
        }
        else if(plzCheck == "Checked In")
        {
            cell.recentStatus.text = recentListingArray[indexPath.row].valueForKey("STATUS") as? String
            cell.recentStatus.textColor = UIColor.greenColor()
        }
        else
        {
            
        }
        
        //        cell.transitItemAssignTo.text = transitListingArray[indexPath.row].valueForKey("ItemAssignTo") as? String
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        tableView.separatorColor = UIColor.clearColor()
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func fetchInRecentDataBase()
    {
        //        // print(recentListingArray.count)
        var str = "5451511"
        
        str = SelectedItemName as String
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Recent", selectColumns: ["*"], whereString: "RecentName == '\(str)'ORDER BY dateCreated DESC LIMIT 0,10", whereFields: [])
        
        
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let fetchFields: NSMutableDictionary! = NSMutableDictionary()
                
                fetchFields.setObject(resultSet.stringForColumn("RecentName"), forKey: "NAME")
                fetchFields.setObject(resultSet.stringForColumn("RecentStatus"), forKey: "STATUS")
                
                recentItemName = (resultSet.stringForColumn("RecentName"))
                
                recentListingArray.addObject(fetchFields)
            }
        }
        resultSet.close()
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
